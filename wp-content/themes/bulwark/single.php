<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php get_header(); ?>
<?php global $smof_data; ?>

<section id="page-header" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php if( $smof_data['tcsn_blog_title'] != "" ) { ?>
                <h3 class="page-title"><?php echo $smof_data['tcsn_blog_title']; ?></h3>
                <?php } else { ?>
                <h3 class="page-title"><?php echo the_title(); ?></h3>
                <?php } ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php get_template_part( 'includes/templates/headers/breadcrumb' ); ?>
            </div>
        </div>
    </div>
</section>
<!-- #page-header -->

<section id="content-main" class="clearfix">
    <div class="container">
        <div class="row">
            <?php if ( $smof_data['tcsn_single_post_layout'] == 'single-full-width' ) : ?>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( '/includes/templates/post-formats/content', get_post_format() ); ?>
                <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages :', 'tcsn_theme' ) . '</span>', 'after' => '</div>' ) ); ?>
                <?php tcsn_post_nav(); ?>
                <?php comments_template(); ?>
                <?php endwhile; ?>
            </div>
            <?php elseif ( $smof_data['tcsn_single_post_layout'] == 'single-with-sidebar' ) : ?>
            <?php if ( $smof_data['tcsn_single_post_sidebar'] == 'single-sidebar-left' ) { ?>
            <?php get_sidebar(); ?>
            <?php } ?>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( '/includes/templates/post-formats/content', get_post_format() ); ?>
                <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages :', 'tcsn_theme' ) . '</span>', 'after' => '</div>' ) ); ?>
                <?php tcsn_post_nav(); ?>
                <?php comments_template(); ?>
                <?php endwhile; ?>
            </div>
            <?php if ( $smof_data['tcsn_single_post_sidebar'] == 'single-sidebar-right' ) { ?>
            <?php get_sidebar(); ?>
            <?php } ?>
            <?php else : ?>
            <div class="col-md-8 col-sm-8 col-xs-12">
                <?php while ( have_posts() ) : the_post(); ?>
                <?php get_template_part( '/includes/templates/post-formats/content', get_post_format() ); ?>
                <?php wp_link_pages( array( 'before' => '<div class="page-links"><span class="page-links-title">' . __( 'Pages :', 'tcsn_theme' ) . '</span>', 'after' => '</div>' ) ); ?>
                <?php tcsn_post_nav(); ?>
                <?php comments_template(); ?>
                <?php endwhile; ?>
            </div>
            <?php get_sidebar(); ?>
            <?php endif ?>
        </div>
    </div>
</section>
<!-- #content -->

<?php get_footer(); ?>
