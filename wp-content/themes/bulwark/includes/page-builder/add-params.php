<?php

/**
 * Add new params to VC
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */

// vc_add_param parameter doesn't exist
if ( !function_exists('vc_add_param') ) {
	return;
}
 
/**
 * Row
 *
 */
vc_add_param("vc_row", array(
	"type"		  	=> "checkbox",
	"class"		  	=> "",
	"heading"	  	=> __( "Make Row Fullwidth", "tcsn_theme" ),
	"param_name"  	=> "fullwidth_row",
	"value"		  	=> array(
		__( "Yes, please", "tcsn_theme" )	=> "yes"
		),
	"description"	=> __( "This will work only with <strong>home page and fullwidth page template</strong>. <br>  Do not give '<strong>left and right margin and padding</strong>' to row in design option tab to make row fulwidth. Even do not write 0. <br> If need to give border give 0, for left and right border. <br> Check help document for more info.", "tcsn_theme" )
));

/**
 * Text Separator
 *
 */

vc_add_param( "vc_text_separator", array(
	"type"			=> "textfield",
	"class"			=> "",
	"heading"		=> __( "Font Size","tcsn_theme" ),
	"param_name"	=> "font_size",
	"value"			=> "16",
	"description"	=> __( "No need of unit, it will be in px.", "tcsn_theme" ),
) );

vc_add_param( "vc_text_separator", array(
	"type"			=> "textfield",
	"class"			=> "",
	"heading"		=> __( "Font Weight", "tcsn_theme" ),
	"param_name"	=> "font_weight",
	"value"			=> "normal",
	"description"	=> __( "normal / bold", "tcsn_theme" ),
) );
 
vc_add_param( "vc_text_separator", array(
	"type"			=> "colorpicker",
	"class"			=> "",
	"heading"		=> __( "Font Color", "tcsn_theme" ),
	"param_name"	=> "font_color",
	"value"			=> "#379f7a",
	"description"	=> __( "Default is : #379f7a", "tcsn_theme" ),
) );

vc_add_param( "vc_text_separator", array(
	"type"			=> "textfield",
	"class"			=> "",
	"heading"		=> __( "Margin Top to Separator", "tcsn_theme" ),
	"param_name"	=> "margin_top",
	"value"			=> "",
	"description"	=> __( "No need of unit, it will be in px.", "tcsn_theme" ),
) );

vc_add_param( "vc_text_separator", array(
	"type"			=> "textfield",
	"class"			=> "",
	"heading"		=> __( "Margin Bottom to Separator", "tcsn_theme" ),
	"param_name"	=> "margin_bottom",
	"value"			=> "",
	"description"	=> __( "No need of unit, it will be in px.", "tcsn_theme" ),
) );