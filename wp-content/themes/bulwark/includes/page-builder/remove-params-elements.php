<?php

/**
 * Remove params and elements from VC
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */

/**
 * Remove elements from VC
 *
 */
if ( function_exists('vc_remove_element') ) {
	add_action( 'init', 'tcsn_vc_remove_element' );
	if ( !function_exists( 'tcsn_vc_remove_element' )) {	
		function tcsn_vc_remove_element() {		
		
			vc_remove_element("vc_button");
			vc_remove_element("vc_cta_button");
			vc_remove_element("vc_button2");
			vc_remove_element("vc_cta_button2");
			vc_remove_element("vc_gallery");
			vc_remove_element("vc_images_carousel");
			vc_remove_element("vc_carousel");
			vc_remove_element("vc_posts_grid");
			vc_remove_element("vc_posts_slider");
		} 	
	} 
} 

/**
 * Remove params 
 *
 */
if ( function_exists('vc_remove_param') ) {
	
	// Add here
	
}