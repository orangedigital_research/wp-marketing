<?php
/**
 * The template for displaying posts in the Quote post format.
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php global $smof_data; ?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php if ( is_single() ) : ?>
    <h4 class="post-title">
        <?php the_title(); ?>
    </h4>
    <?php elseif ( is_page() ) : ?>
    <h4 class="post-title no-display"></h4>
    <?php else : ?>
    <h4 class="post-title"><a href="<?php the_permalink(); ?>" rel="bookmark">
        <?php the_title(); ?>
        </a></h4>
    <?php endif; ?>
    <?php if ( ! post_password_required() ) : ?>
    <?php if( !is_single() ) { ?>
    <a href="<?php the_permalink(); ?>" rel="bookmark">
    <?php } ?>
    <div class="pf-quote clearfix">
        <blockquote>
            <?php the_content(); ?>
        </blockquote>
        <span class="quote-source">
        <?php $pf_quote_source = rwmb_meta('rw_pf_quote_source', 'type=text'); ?>
        <?php echo  $pf_quote_source ?> </span> </div>
    <?php if( !is_single() ) { ?>
    </a>
    <?php } ?>
    <?php endif; ?>
    <?php if ( ! is_custom_post_type() && ! is_page() ) : ?>
    <div class="post-meta clearfix">
        <div class="post-format-icon"><a href="<?php echo get_post_format_link('quote'); ?>"><i class="icon-quote"></i></a></div>
        <?php tcsn_post_meta(); ?>
        <?php if ( comments_open() && ! is_single() && ! is_page() ) :?>
        <?php if ( ! post_password_required() ) { ?>
        <div class="comment-number">With <br>
            <div class="leave-comment-link">
                <?php comments_popup_link(  '' . __( '0 Replies', 'tcsn_theme' ), '' . __( '1 Replies', 'tcsn_theme' ), '' . __( '% Replies', 'tcsn_theme' ) ); ?>
            </div>
        </div>
        <?php } ?>
        <?php endif; ?>
    </div>
    <?php endif; ?>
    <div class="post-author">
        <?php if ( is_single() && get_the_author_meta( 'description' ) && is_multi_author() ) : ?>
        <?php get_template_part( '/includes/templates/post-formats/author-bio' ); ?>
        <?php endif; ?>
    </div>
    <?php if(!($wp_query->post_count == $wp_query->current_post+1)) : ?>
    <div class="post-footer"></div>
    <?php endif; ?>
</article>
<!-- #post -->