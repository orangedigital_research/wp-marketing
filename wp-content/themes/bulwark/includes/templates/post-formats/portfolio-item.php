<?php
/**
 * The template for displaying Portfolio item content
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php global $smof_data; ?>
<?php 
	$folio_img = get_post_thumbnail_id(); 
	$folio_img_url = wp_get_attachment_url( $folio_img,'full' ); 
	$folio_thumb_cropped = aq_resize( $folio_img_url, 300, 200, true ); 
	$folio_thumb = aq_resize( $folio_img_url, 300, 200, false ); 
	$lightbox_img_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large'); 
	$portfolio_type = rwmb_meta('rw_portfolio_type', 'type=select');
	$video_url = rwmb_meta('rw_video_url', 'type=text');
	$zoom_title = rwmb_meta('rw_zoom_title', 'type=text'); 
	$external_link= rwmb_meta('rw_external_link', 'type=checkbox'); 
	$link_url = rwmb_meta('rw_link_url', 'type=text'); 
?>
<?php if ( $smof_data['tcsn_portfolio_hover'] == 'zoom' ) { ?>
<div class='overlay'></div>
<?php the_post_thumbnail(); ?>
<a href="<?php echo $lightbox_img_url; ?>" data-rel="prettyPhoto" title="<?php echo $zoom_title; ?>" class="zoom-button"></a>
<?php  } ?>
<?php if ( $smof_data['tcsn_portfolio_hover'] == 'link' ) { ?>
<?php if ( $external_link == 1 ) { ?>
<div class='overlay'></div>
<?php the_post_thumbnail(); ?>
<a href="<?php echo $link_url; ?>" class="link-button"></a>
<?php } else { ?>
<div class='overlay'></div>
<?php the_post_thumbnail(); ?>
<a href="<?php the_permalink(); ?>" class="link-button"></a>
<?php } ?>
<?php } ?>
