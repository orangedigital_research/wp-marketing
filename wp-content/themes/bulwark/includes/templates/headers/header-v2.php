<?php
/**
 * The Header variation 2
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php global $smof_data; ?>

<div id="header-v2" class="clearfix">
    <div id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4 col-xs-12 logo">
                    <?php if( ( $smof_data['tcsn_logo_type'] == "tcsn_show_image_logo" ) ) : ?>
                    <?php if( $smof_data['tcsn_image_standard_logo'] != "" ) { ?>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('title'); ?>"><img src="<?php echo $smof_data['tcsn_image_standard_logo']; ?>" alt="<?php bloginfo('title'); ?>" class="logo-standard"></a>
                    <?php } ?>
                    <?php if( $smof_data['tcsn_image_retina_logo'] != "" ) { ?>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('title'); ?>"><img src="<?php echo $smof_data['tcsn_image_retina_logo']; ?>" width="<?php echo $smof_data['tcsn_logo_width']; ?>" height="<?php echo $smof_data['tcsn_logo_height']; ?>" alt="<?php bloginfo('title'); ?>" class="logo-retina"></a>
                    <?php } ?>
                    <?php elseif( ( $smof_data['tcsn_logo_type'] == "tcsn_show_text_logo" ) ) : ?>
                    <?php if( $smof_data['tcsn_text_logo'] != "" ) { ?>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('title'); ?>"><?php echo $smof_data['tcsn_text_logo']; ?></a>
                    <?php } ?>
                    <?php else : ?>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('title'); ?>"><img src="<?php echo get_template_directory_uri() . "/img/logo.png" ?>" alt="<?php bloginfo('title'); ?>"></a>
                    <?php endif; ?>
                </div>
                <!-- .logo -->
                
                <div class="col-md-8 col-sm-8 col-xs-12">
                    <nav class="menu-wrapper clearfix">
                        <?php get_template_part( 'includes/templates/headers/main-menu' ); ?>
                    </nav>
                    <!-- #menu --> 
                </div>
            </div>
        </div>
    </div>
    <!-- #header --> 
</div>
<!-- #header variation --> 
