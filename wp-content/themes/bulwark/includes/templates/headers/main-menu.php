<?php
/**
 * The Main Menu
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php wp_nav_menu( array( 
	'theme_location'  => 'primary_menu',
	'container'       => '',
	'container_class' => '',
	'container_id'    => '',
	'menu_class'      => 'sf-menu',
	'menu_id'         => 'nav',
	'depth'           => 0,
	'walker' => new TCSN_Dropdown_Walker_Nav_Menu,
	) 
); 
