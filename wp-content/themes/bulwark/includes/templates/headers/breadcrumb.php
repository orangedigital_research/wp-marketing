<?php
/**
 * The Breadcrumb
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php global $smof_data; ?>
<?php if( $smof_data['tcsn_show_breadcrumb'] == 1 ) { ?>
<ul class="breadcrumbs">
    <?php if(function_exists('bcn_display_list'))
    {
        bcn_display_list();
    }?>
</ul>
<?php } ?>