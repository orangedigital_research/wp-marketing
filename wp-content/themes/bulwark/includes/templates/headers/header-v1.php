<?php
/**
 * The Header variation 1
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php global $smof_data; ?>
<?php if( $smof_data['tcsn_show_mobile_search'] == 1 ) { ?>
<div id="mobile-search" class="visible-xs visible-sm clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="search-header">
                    <?php get_search_form(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
<!--small devices search-->
<?php } ?>

<div id="header-v1" class="clearfix">
    <div id="header">
        <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-2 col-xs-12 logo">
                    <?php if( ( $smof_data['tcsn_logo_type'] == "tcsn_show_image_logo" ) ) : ?>
                    <?php if( $smof_data['tcsn_image_standard_logo'] != "" ) { ?>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('title'); ?>"><img src="<?php echo $smof_data['tcsn_image_standard_logo']; ?>" alt="<?php bloginfo('title'); ?>" class="logo-standard"></a>
                    <?php } ?>
                    <?php if( $smof_data['tcsn_image_retina_logo'] != "" ) { ?>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('title'); ?>"><img src="<?php echo $smof_data['tcsn_image_retina_logo']; ?>" width="<?php echo $smof_data['tcsn_logo_width']; ?>" height="<?php echo $smof_data['tcsn_logo_height']; ?>" alt="<?php bloginfo('title'); ?>" class="logo-retina"></a>
                    <?php } ?>
                    <?php elseif( ( $smof_data['tcsn_logo_type'] == "tcsn_show_text_logo" ) ) : ?>
                    <?php if( $smof_data['tcsn_text_logo'] != "" ) { ?>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('title'); ?>"><?php echo $smof_data['tcsn_text_logo']; ?></a>
                    <?php } ?>
                    <?php else : ?>
                    <a href="<?php echo home_url(); ?>" title="<?php bloginfo('title'); ?>"><img src="<?php echo get_template_directory_uri() . "/img/logo.png" ?>" alt="<?php bloginfo('title'); ?>"></a>
                    <?php endif; ?>
                </div>
                <!-- .logo -->
                
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <nav class="menu-wrapper clearfix">
                        <?php get_template_part( 'includes/templates/headers/main-menu' ); ?>
                        <?php if( $smof_data['tcsn_show_search'] == 1 ) { ?>
                        <div class="search-header hidden-xs hidden-sm">
                            <?php get_search_form(); ?>
                        </div>
                        <?php } ?>
                    </nav>
                    <!-- #menu --> 
                </div>
            </div>
        </div>
    </div>
    <!-- #header --> 
</div>
<!-- #header variation --> 
