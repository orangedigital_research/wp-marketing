<?php
/**
 * The Template for displaying slider on portfolio item
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php global $smof_data; ?>
<?php $pf_select_gallery_rev_slider = rwmb_meta('rw_pf_select_gallery_rev_slider', 'type=select'); ?>
<?php if ( function_exists('putRevSlider') ) { ?>
<?php putRevSlider($pf_select_gallery_rev_slider); ?>
<?php } ?>
