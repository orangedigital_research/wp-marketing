<?php 
/**
 * Custom styles through options panel
 *
 */
function tcsn_custom_styles() {
	global $smof_data;

	wp_enqueue_style(
		'tcsn-custom-style',
		get_template_directory_uri() . '/css/custom_script.css'
	);
	
	// Variables
	// Body
	$tcsn_body_face  = $smof_data['tcsn_font_body']['face'];
	$tcsn_body_size  = $smof_data['tcsn_font_body']['size'];
	$tcsn_body_style = $smof_data['tcsn_font_body']['style'];
	$tcsn_body_color = $smof_data['tcsn_font_body']['color'];
	
	// Headings
	$tcsn_h1_face  = $smof_data['tcsn_font_h1']['face'];
	$tcsn_h1_size  = $smof_data['tcsn_font_h1']['size'];
	$tcsn_h1_style = $smof_data['tcsn_font_h1']['style'];
	$tcsn_h1_color = $smof_data['tcsn_font_h1']['color'];
	
	$tcsn_h2_face  = $smof_data['tcsn_font_h2']['face'];
	$tcsn_h2_size  = $smof_data['tcsn_font_h2']['size'];
	$tcsn_h2_style = $smof_data['tcsn_font_h2']['style'];
	$tcsn_h2_color = $smof_data['tcsn_font_h2']['color'];
	
	$tcsn_h3_face  = $smof_data['tcsn_font_h3']['face'];
	$tcsn_h3_size  = $smof_data['tcsn_font_h3']['size'];
	$tcsn_h3_style = $smof_data['tcsn_font_h3']['style'];
	$tcsn_h3_color = $smof_data['tcsn_font_h3']['color'];
	
	$tcsn_h4_face  = $smof_data['tcsn_font_h4']['face'];
	$tcsn_h4_size  = $smof_data['tcsn_font_h4']['size'];
	$tcsn_h4_style = $smof_data['tcsn_font_h4']['style'];
	$tcsn_h4_color = $smof_data['tcsn_font_h4']['color'];
	
	$tcsn_h5_face  = $smof_data['tcsn_font_h5']['face'];
	$tcsn_h5_size  = $smof_data['tcsn_font_h5']['size'];
	$tcsn_h5_style = $smof_data['tcsn_font_h5']['style'];
	$tcsn_h5_color = $smof_data['tcsn_font_h5']['color'];
	
	$tcsn_h6_face  = $smof_data['tcsn_font_h6']['face'];
	$tcsn_h6_size  = $smof_data['tcsn_font_h6']['size'];
	$tcsn_h6_style = $smof_data['tcsn_font_h6']['style'];
	$tcsn_h6_color = $smof_data['tcsn_font_h6']['color'];

	// Logo
	$tcsn_logo_face  = $smof_data['tcsn_font_logo']['face'];
	$tcsn_logo_size  = $smof_data['tcsn_font_logo']['size'];
	$tcsn_logo_style = $smof_data['tcsn_font_logo']['style'];
	$tcsn_logo_color = $smof_data['tcsn_font_logo']['color'];
	$tcsn_hover_logo  = $smof_data['tcsn_hover_logo'];
	$tcsn_logo_margin_top = $smof_data['tcsn_logo_margin_top'];

	// search
	$tcsn_search_margin_top = $smof_data['tcsn_search_margin_top'];

	// Menu
	$tcsn_menu_face  = $smof_data['tcsn_font_menu']['face'];
	$tcsn_menu_size  = $smof_data['tcsn_font_menu']['size'];
	$tcsn_menu_style = $smof_data['tcsn_font_menu']['style'];
	$tcsn_menu_color = $smof_data['tcsn_font_menu']['color'];
	$tcsn_menu_link_hover_bg_color = $smof_data['tcsn_menu_link_hover_bg_color'];
	$tcsn_menu_link_hover_border_color = $smof_data['tcsn_menu_link_hover_border_color'];
    $tcsn_menu_link_hover_color = $smof_data['tcsn_menu_link_hover_color'];
	
	// Page Header
	$tcsn_color_page_title = $smof_data['tcsn_color_page_title'];
	$tcsn_bcum_size  = $smof_data['tcsn_font_bcum']['size'];
	$tcsn_bcum_color = $smof_data['tcsn_font_bcum']['color'];
	
	// Footer
	$tcsn_footer_face  = $smof_data['tcsn_font_footer']['face'];
	$tcsn_footer_size  = $smof_data['tcsn_font_footer']['size'];
	$tcsn_footer_style = $smof_data['tcsn_font_footer']['style'];
	$tcsn_footer_color = $smof_data['tcsn_font_footer']['color'];
	$tcsn_color_footer_headings = $smof_data['tcsn_color_footer_headings'];
	
	// Widgets
	$tcsn_color_widget_link = $smof_data['tcsn_color_widget_link'];
	$tcsn_color_widget_link_hover = $smof_data['tcsn_color_widget_link_hover'];
	$tcsn_color_widget_border = $smof_data['tcsn_color_widget_border'];
	$tcsn_color_widget_social = $smof_data['tcsn_color_widget_social'];
		
	//General
	$tcsn_link_color = $smof_data['tcsn_link_color'];
	$tcsn_link_hover_color = $smof_data['tcsn_link_hover_color'];
	$tcsn_text_color = $smof_data['tcsn_text_color'];
	
	// Styles
	$custom_css = "
body { font-family: {$tcsn_body_face}, Arial, Helvetica, sans-serif; font-size: {$tcsn_body_size}; font-weight: {$tcsn_body_style}; color: {$tcsn_body_color}; }
h1, h1 a, h1 a:link, h1 a:visited, h1 a:active { font-family: {$tcsn_h1_face}, Arial, Helvetica, sans-serif; font-size: {$tcsn_h1_size}; font-weight: {$tcsn_h1_style}; color: {$tcsn_h1_color}; }
h2, h2 a, h2 a:link, h2 a:visited, h2 a:active { font-family: {$tcsn_h2_face}, Arial, Helvetica, sans-serif; font-size: {$tcsn_h2_size}; font-weight: {$tcsn_h2_style}; color: {$tcsn_h2_color}; }
h3, h3 a, h3 a:link, h3 a:visited, h3 a:active { font-family: {$tcsn_h3_face}, Arial, Helvetica, sans-serif; font-size: {$tcsn_h3_size}; font-weight: {$tcsn_h3_style}; color: {$tcsn_h3_color}; }
h4, h4 a, h4 a:link, h4 a:visited, h4 a:active { font-family: {$tcsn_h4_face}, Arial, Helvetica, sans-serif; font-size: {$tcsn_h4_size}; font-weight: {$tcsn_h4_style}; color: {$tcsn_h4_color}; }
h5, h5 a, h5 a:link, h5 a:visited, h5 a:active, .widgettitle, .widget-title, .comment-reply-title { font-family: {$tcsn_h5_face}, Arial, Helvetica, sans-serif; font-size: {$tcsn_h5_size}; font-weight: {$tcsn_h5_style}; color: {$tcsn_h5_color}; }
h6, h6 a, h6 a:link, h6 a:visited, h6 a:active { font-family: {$tcsn_h6_face}, Arial, Helvetica, sans-serif; font-size: {$tcsn_h6_size}; font-weight: {$tcsn_h6_style}; color: {$tcsn_h6_color}; }
a, a:link { color: {$tcsn_link_color}; }
a:visited { color: {$tcsn_link_color}; }
a:hover { color: {$tcsn_link_hover_color}; }
a:active { color: {$tcsn_link_color}; }
.color, blockquote, blockquote p { color: {$tcsn_text_color}; }
blockquote { border: 1px solid {$tcsn_text_color}; border-width: 0 0 0 1px; }
blockquote.pull-right { border: 1px solid {$tcsn_text_color}; border-width: 0 1px 0 0; }
.logo a { font-family: {$tcsn_logo_face}, Arial, Helvetica, sans-serif; font-size: {$tcsn_logo_size}; font-weight: {$tcsn_logo_style}; color: {$tcsn_logo_color}; margin-top: {$tcsn_logo_margin_top}px; }
.logo a:hover { color: {$tcsn_hover_logo}; }
.search-header { margin-top: {$tcsn_search_margin_top}px; }
.sf-menu a { font-family: {$tcsn_menu_face}, Arial, Helvetica, sans-serif; font-size: {$tcsn_menu_size}; font-weight: {$tcsn_menu_style}; }
.sf-menu a:hover, .sf-menu li.current-menu-item a, .sf-menu li:hover > a, .sf-menu li.current-menu-ancestor > a, .sf-menu ul { background-color: {$tcsn_menu_link_hover_bg_color}; 
border: 1px solid {$tcsn_menu_link_hover_border_color}; }
.sf-menu a, .sf-menu a:hover, .sf-menu li.current-menu-item a, .sf-menu li:hover > a, .sf-menu ul li a, .sf-menu li.current-menu-item li a { color: {$tcsn_menu_color}; }
.sf-menu a:hover, .sf-menu li.current-menu-item a, .sf-menu li:hover > a, .sf-menu li.current-menu-ancestor > a, .sf-menu ul li a:hover, .sf-menu ul li.current-menu-item a, .sf-menu ul li:hover > a, .sf-menu .sub-menu li.current-menu-item a { color: {$tcsn_menu_link_hover_color}; }
#page-header .page-title { color: {$tcsn_color_page_title}; }
.breadcrumbs { font-size: {$tcsn_bcum_size}; color: {$tcsn_bcum_color}; }
#footer { font-family: {$tcsn_footer_face}, Arial, Helvetica, sans-serif; font-size: {$tcsn_footer_size}; font-weight: {$tcsn_footer_style}; color: {$tcsn_footer_color}; }
#footer h1, #footer h2, #footer h3, #footer h4, #footer h5, #footer h6 { color: {$tcsn_color_footer_headings}; }
.recentpost-date, .recentpost-comment, .post-date a, .vcard a, .leave-comment-link {  font-family: {$tcsn_h6_face}, Arial, Helvetica, sans-serif;}
.recentpost-date, .post-date a, .vcard a, .leave-comment-link a { color: {$tcsn_h6_color};}
.widget_pages a, .widget_categories a, .widget_archive a, .custom-tagcloud a, .custom-tagcloud a:link, .tcsn_widget_conatct_info a, .widget_nav_menu a, #wp-calendar a:hover, #recentcomments a:hover { color: {$tcsn_color_widget_link}; }
.widget_pages a:hover, .widget_categories a:hover, .widget_archive a:hover, .widget_nav_menu a:hover, .widget_archive .post-count, .widget_categories .post-count, .tcsn_widget_conatct_info a:hover, #wp-calendar a, #recentcomments a { color: {$tcsn_color_widget_link_hover}; }
.widget_archive ul li, .widget_categories ul li, .custom-recent-entries li { border-bottom: 1px solid {$tcsn_color_widget_border}; }
.widget_nav_menu ul li a, .custom-tagcloud a, .custom-tagcloud a:link { border: 1px solid {$tcsn_color_widget_border}; }
.custom-tagcloud a:hover { background-color: {$tcsn_color_widget_link_hover}; border: 1px solid {$tcsn_color_widget_link_hover}; }
.tooltip.top .tooltip-arrow, .tooltip.top-left .tooltip-arrow, .tooltip.top-right .tooltip-arrow { border-top-color: {$tcsn_text_color}; }
.tooltip.right .tooltip-arrow { border-right-color: {$tcsn_text_color}; }
.tooltip.left .tooltip-arrow { border-left-color: {$tcsn_text_color}; }
.tooltip.bottom .tooltip-arrow, .tooltip.bottom-left .tooltip-arrow, .tooltip.bottom-right .tooltip-arrow { border-bottom-color: {$tcsn_text_color}; }
.dropcap, .highlight, .tooltip-inner, .mytooltip, .ribbon, .feature-icon .icon-wrappper, .pager li > a, .pager li > span { background-color: {$tcsn_text_color}; }
.mytooltip-arrow { border-right: 6px solid {$tcsn_text_color}; border-top: 7px solid {$tcsn_text_color}; }
.social li i { color: {$tcsn_color_widget_social}; }
@media only screen and (max-width: 767px) {
.logo a { margin-top: 0px !important}
}
	";
	wp_add_inline_style( 'tcsn-custom-style', $custom_css );
}
add_action( 'wp_enqueue_scripts', 'tcsn_custom_styles', 80 );

function tcsn_css_custom() {
global $smof_data; 
?>
<style>
body {<?php if ( $smof_data['tcsn_show_pattern_body'] == 1 ) { ?>background-color: <?php echo $smof_data['tcsn_bg_color_body']; ?>; background-image: url( <?php echo $smof_data['tcsn_pattern_body'] ?> ); background-repeat: <?php echo $smof_data['tcsn_pattern_repeat_body']; ?>; background-attachment: <?php echo $smof_data['tcsn_attachment_body']; ?>; <?php } else { ?> background-color: <?php echo $smof_data['tcsn_bg_color_body']; ?>; <?php } ?>}
#header { <?php if( $smof_data['tcsn_show_pattern_header'] == 1 ) { ?>background-color: <?php echo $smof_data['tcsn_bg_color_header']; ?>; background-image: url( <?php echo $smof_data['tcsn_pattern_header'] ?> ); background-repeat: <?php echo $smof_data['tcsn_pattern_repeat_header']; ?>; <?php } else { ?> background-color: <?php echo $smof_data['tcsn_bg_color_header']; ?>; <?php } ?> }
#page-header {<?php if ( $smof_data['tcsn_show_pattern_page_header'] == 1 ) { ?>background-color: <?php echo $smof_data['tcsn_bg_color_page_header']; ?>; background-image: url( <?php echo $smof_data['tcsn_pattern_page_header'] ?> ); background-repeat: <?php echo $smof_data['tcsn_pattern_repeat_page_header']; ?>; <?php } else { ?> background-color: <?php echo $smof_data['tcsn_bg_color_page_header']; ?>; <?php } ?>}
#footer { <?php if( $smof_data['tcsn_show_pattern_footer'] == 1 ) { ?>background-color: <?php echo $smof_data['tcsn_bg_color_footer']; ?>; background-image: url( <?php echo $smof_data['tcsn_pattern_footer'] ?> ); background-repeat: <?php echo $smof_data['tcsn_pattern_repeat_footer']; ?>; <?php } else { ?> background-color: <?php echo $smof_data['tcsn_bg_color_footer']; ?>; <?php } ?> border-top: 1px solid <?php echo $smof_data['tcsn_border_top_footer']; ?>;}
#submit:hover, input[type="submit"]:hover { background-color: <?php echo $smof_data['tcsn_text_color']; ?>; }
<?php if( $smof_data['tcsn_letter_spacing_body'] == 1 ) { ?>
body { letter-spacing: normal; }
<?php } ?>
<?php if( $smof_data['tcsn_letter_spacing_heading'] == 1 ) { ?>
h1, h2, h3, h4, h5, h6 { letter-spacing: normal; }
<?php } ?>
<?php if( $smof_data['tcsn_padding_header'] != ""  ) { ?>
@media (min-width: 992px), @media (min-width: 980px) and (max-width: 1199px), @media (min-width: 1200px) { 
#header { padding: <?php echo $smof_data['tcsn_padding_header']; ?>px 0;} 
.menu-wrapper { margin-top: <?php echo $smof_data['tcsn_menu_margin_top']; ?>px; }
}
<?php } ?>
<?php if( $smof_data['tcsn_custom_css'] != '' ) { echo $smof_data['tcsn_custom_css']; } ?>
</style>
<?php }
add_action( 'wp_head', 'tcsn_css_custom', 90 );