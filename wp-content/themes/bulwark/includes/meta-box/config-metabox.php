<?php
//Register meta boxes
add_action('admin_init', 'rw_register_meta_boxes');

function rw_register_meta_boxes()
{
    if (!class_exists('RW_Meta_Box'))
        return;
    $prefix       = 'rw_';
    $meta_boxes   = array();
	
	// Display metaboxes according to post format
	add_action( 'admin_print_scripts', 'display_metaboxes', 1000 );
    function display_metaboxes() {
    	if ( get_post_type() == "post" ) :
        ?>
<script type="text/javascript">
	jQuery( document ).ready( function($) {

	var	$pfaudio = $('#format-post-audio').hide(),
		$pfgallery = $('#format-post-gallery').hide(),
		$pflink = $('#format-post-link').hide(),
		$pfquote = $('#format-post-quote').hide(),
		$pfvideo = $('#format-post-video').hide(),
		$post_format = $('input[name="post_format"]');

	$post_format.each(function() {
		var $this = $(this);
		if( $this.is(':checked') )
			change_post_format( $this.val() );
	});

	$post_format.change(function() {
		change_post_format( $(this).val() );
	});

	function change_post_format( val ) {
		$pfaudio.hide();
		$pfgallery.hide();
		$pflink.hide();
		$pfquote.hide();
		$pfvideo.hide();
		
		if( val === 'audio' ) {
			$pfaudio.show();
		} else if( val === 'gallery' ) {
			$pfgallery.show();
		} else if( val === 'link' ) {
			$pflink.show();
		} else if( val === 'quote' ) {
			$pfquote.show();
		} else if( val === 'video' ) {
			$pfvideo.show();
		}
	}
});
     </script>
<?php
    endif;
	 }

	// All Revolution sliders in array
	$revolutionslider = array();
	if( class_exists('RevSlider') ) {
		$theslider = new RevSlider();
		$arrSliders = $theslider->getArrSliders();
		$revolutionslider[0] = 'Select a Slider';
		foreach($arrSliders as $slider) { 
			$revolutionslider[$slider->getAlias()] = $slider->getTitle();
		}
	}
	else {
		$revolutionslider[0] = 'Install Revolution Slider Plugin';
	}
	
	// Meta boxes for post formats
	
	// Audio Post Format
	$meta_boxes[] = array(
		'id'    => 'format-post-audio',
        'title' => 'Audio Embed Code',
        'pages' => array(
        	'post'
        ),
		'context'  => 'normal',
        'priority' => 'high',
        'fields'   => array(
			 array(
                'name' => '',
                'id'   => $prefix . 'pf_audio_embed',
                'type' => 'textarea',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
        )
    );
	
	// Gallery Post Format
    $meta_boxes[] = array(
		'id'       => 'format-post-gallery',
        'title'    => 'Select Revolution Slider',
        'context'  => 'normal',
        'priority' => 'high',
        'pages'    => array(
            'post'
        ),
        'fields'   => array(
            array(
                'name'       => __('', 'rwmb'),
                'id'         => $prefix . 'select_gallery_rev_slider',
				'desc'  => __( 'Select Revolution slider for gallery post.', 'rwmb' ),
                'type'       => 'select',
                'options'    => $revolutionslider,
                'multiple'    => false,
                'placeholder' => __('', 'rwmb')
            )
        )
    );
	
	// Link Post format
	$meta_boxes[] = array(
		'id'    => 'format-post-link',
        'title' => 'Link Text and URL',
        'pages' => array(
        	'post'
        ),
		'context'  => 'normal',
        'priority' => 'high',
        'fields'   => array(
			 array(
                'name' => 'Link text',
                'id'   => $prefix . 'pf_link_text',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
            array(
                'name' => 'Link URL',
                'id'   => $prefix . 'pf_link_url',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
        )
    );
	
	// Quote Post format
	$meta_boxes[] = array(
		'id'    => 'format-post-quote',
        'title' => 'Quote Source',
        'pages' => array(
        	'post'
        ),
		'context'  => 'normal',
        'priority' => 'high',
        'fields'   => array(
			 array(
                'name' => '',
                'id'   => $prefix . 'pf_quote_source',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
        )
    );
		
	// Video Post Format
	$meta_boxes[] = array(
		'id'    => 'format-post-video',
        'title' => 'Video Embed Code',
        'pages' => array(
        	'post'
        ),
		'context'  => 'normal',
        'priority' => 'high',
        'fields'   => array(
			 array(
                'name' => '',
                'id'   => $prefix . 'pf_video_embed',
                'type' => 'textarea',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
        )
    );
	
	// Meta box for client info in testimonial
	$meta_boxes[] = array(
        'title' => 'Client info',
        'pages' => array(
            'tcsn_testimonial'
        ),
        'context'  => 'side',
        'priority' => 'high',
        'fields'   => array(
            array(
                'name' => '',
                'id'   => $prefix . 'client_info',
                'type' => 'text',
                'size' => 27,
                'desc' => __('', 'rwmb')
            ),
        )
    );
	
	// Meta box for Member job title in team
	$meta_boxes[] = array(
        'title' => 'Member Job Title',
        'pages' => array(
            'tcsn_team'
        ),
        'context'  => 'side',
        'priority' => 'high',
        'fields'   => array(
            array(
                'name' => '',
                'id'   => $prefix . 'member_job',
                'type' => 'text',
                'size' => 27,
                'desc' => __('', 'rwmb')
            ),
        )
    );
	
	// Meta box for member social in team
	$meta_boxes[] = array(
        'title' => 'Provide URL',
        'pages' => array(
            'tcsn_team'
        ),
        'context'  => 'normal',
        'priority' => 'high',
        'fields'   => array(
			array(
                'name' => 'Behance',
                'id'   => $prefix . 'member_behance',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Delicious',
                'id'   => $prefix . 'member_delicious',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Dribbble',
                'id'   => $prefix . 'member_dribbble',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Dropbox',
                'id'   => $prefix . 'member_dropbox',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
            array(
                'name' => 'Facebook',
                'id'   => $prefix . 'member_facebook',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Flickr',
                'id'   => $prefix . 'member_flickr',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Googleplus',
                'id'   => $prefix . 'member_googleplus',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Instagram',
                'id'   => $prefix . 'member_instagram',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Linkedin',
                'id'   => $prefix . 'member_linkedin',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Paypal',
                'id'   => $prefix . 'member_paypal',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Pinterest',
                'id'   => $prefix . 'member_pinterest',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Skype',
                'id'   => $prefix . 'member_skype',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Soundcloud',
                'id'   => $prefix . 'member_soundcloud',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Stumbleupon',
                'id'   => $prefix . 'member_stumbleupon',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Tumblr',
                'id'   => $prefix . 'member_tumblr',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Twitter',
                'id'   => $prefix . 'member_twitter',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Viadeo',
                'id'   => $prefix . 'member_viadeo',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Vimeo',
                'id'   => $prefix . 'member_vimeo',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Youtube',
                'id'   => $prefix . 'member_youtube',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
			array(
                'name' => 'Mail',
                'id'   => $prefix . 'member_mail',
                'type' => 'text',
                'size' => 40,
                'desc' => __('', 'rwmb')
            ),
        )
    );
	
	// Meta box to select portfolio post type
    $meta_boxes[] = array(
        'title'    => 'Post type',
        'context'  => 'side',
        'priority' => 'high',
        'pages'    => array(
            'tcsn_portfolio'
        ),
        'fields'   => array(
            array(
                'name'       => __('Select Post Type', 'rwmb'),
                'id'         => $prefix . 'portfolio_type',
                'type'       => 'select',
                'options'    => array(
                    'Image'   => __('Image', 'rwmb'),
                    'Video'   => __('Video', 'rwmb'),
					'Audio'   => __('Audio', 'rwmb'),
					'Gallery' => __('Gallery', 'rwmb'),
                ),
                'multiple'    => false,
                'std'         => 'Image',
                'placeholder' => __('Select Post Type', 'rwmb'),
				'desc' => __('<br/>Preferred image size : <strong>530px x 370px</strong>', 'rwmb'),
            )
        )
    );
	
	// Meta box for client
	$meta_boxes[] = array(
        'title' => 'Client Name',
        'pages' => array(
            'tcsn_portfolio'
        ),
        'context'  => 'side',
        'priority' => 'high',
        'fields'   => array(
            array(
                'name' => '',
                'id'   => $prefix . 'portfolio_client',
                'type' => 'text',
                'size' => 27,
                'desc' => __('Any text can be given.', 'rwmb')
            ),
        )
    );
	
	// Meta box for title to lightbox image / video
	$meta_boxes[] = array(
        'title' => 'If zoom on hover : <br>Title to lightbox Image / video',
        'pages' => array(
            'tcsn_portfolio'
        ),
        'context'  => 'side',
        'priority' => 'high',
        'fields'   => array(
            array(
                'name' => '',
                'id'   => $prefix . 'zoom_title',
                'type' => 'text',
                'size' => 27,
                'desc' => __('', 'rwmb')
            ),
        )
    );
	
	// Meta box for portfolio url input
    $meta_boxes[] = array(
        'title' => 'If link on hover : <br>External Link URL',
        'pages' => array(
            'tcsn_portfolio'
        ),
        'context'  => 'side',
        'priority' => 'high',
        'fields'   => array(
			array(
				'name' => __( '<br>Checkbox to enable external link <br><br> If unchecked, link will display portfolio details page. <br><br>', 'rwmb' ),
				 'id'  => $prefix . 'external_link',
				'type' => 'checkbox',
				'std'  => 0,
			),
            array(
                'name' => '',
                'id'   => $prefix . 'link_url',
                'type' => 'text',
                'size' => 27,
                'desc' => __('Tick the checkbox and give link here', 'rwmb')
            ),
		
        )
    );
	
    // Meta box for video url input
    $meta_boxes[] = array(
        'title'   => 'If - Video Post',
        'pages'   => array(
            'tcsn_portfolio'
        ),
        'context'  => 'side',
        'priority' => 'high',
        'fields'   => array(
            array(
                'name' => 'Video URL :',
                'id'   => $prefix . 'video_url',
                'type' => 'text',
                'size' => 27,
                'desc' => __('<br/>Video will be displayed on zoom.<br/><br/><strong>URL examples</strong><br/> Youtube - http://youtu.be/XSGBVzeBUbk <br/><br/>  Vimeo - http://vimeo.com/69228454', 'rwmb')
            ),
        )
    );

	// Meta box for portfolio details
    $meta_boxes[] = array(
        'title'   => 'For Item Details Page : Portfolio / Project Info :',
        'pages'   => array(
            'tcsn_portfolio'
        ),
        'context'  => 'normal',
        'priority' => 'high',
        'fields'   => array(
            array(
                'name' => '',
                'id'   => $prefix . 'pf_details',
                'type' => 'textarea',
                'size' => 40,
                'desc' => '<br/>Copy following to above textarea. You can place other content (accepts HTML). <br> Will get displayed on portfolio item details page if predefined content is enabled through options panel.  <br><br> 
&lt;ul class="list-border"&gt;<br/>
	&lt;li&gt;&lt;strong&gt;Started on :&lt;/strong&gt; Sept 2, 2012&lt;/li&gt;<br>
	&lt;li&gt;&lt;strong&gt;Challenge :&lt;/strong&gt; Logo / Web / Corporate Identity&lt;/li&gt;<br>
	&lt;li&gt;&lt;strong&gt;Completed on :&lt;/strong&gt; Oct 4, 2012&lt;/li&gt;<br>
	&lt;li&gt;online /&lt;del&gt;offline&lt;/del&gt;&lt;/li&gt;<br>
&lt;/ul&gt;<br>
&lt;a href="#" class="mybtn"&gt;Launch Website&lt;/a&gt;',
            ),
        )
    );
	
	// Meta box for video embed
    $meta_boxes[] = array(
        'title'   => 'If Video/ Audio Post : Embed Code :',
        'pages'   => array(
            'tcsn_portfolio'
        ),
        'context'  => 'normal',
        'priority' => 'high',
        'fields'   => array(
            array(
                'name' => '',
                'id'   => $prefix . 'pf_video_audio_embed',
                'type' => 'textarea',
                'size' => 40,
                'desc' => 'This video / audio will get displayed on portfolio item details page if predefined content is enabled through options panel.',
            ),
        )
    );
	
	// Meta box for gallery portfolio
    $meta_boxes[] = array(
		
        'title'    => 'If Gallery Post : Revolution Slider',
        'context'  => 'normal',
        'priority' => 'high',
        'pages'    => array(
            'tcsn_portfolio'
        ),
        'fields'   => array(
            array(
                'name'       => __('', 'rwmb'),
                'id'         => $prefix . 'pf_select_gallery_rev_slider',
				'desc'  => __( 'Select Revolution slider if gallery portfolio item (do not select fullwidth slider if any). <br> Will get displayed on portfolio item details page if predefined content is enabled through options panel.', 'rwmb' ),
                'type'       => 'select',
                'options'    => $revolutionslider,
                'multiple'    => false,
                'placeholder' => __('', 'rwmb')
            )
        )
    );

	// Meta box to select revolution slider for home page
    $meta_boxes[] = array(
        'title'    => 'Select Revolution Slider (For Home Page)',
        'context'  => 'normal',
        'priority' => 'default',
        'pages'    => array(
            'page'
        ),
        'fields'   => array(
            array(
                'name'        => __('', 'rwmb'),
                'id'          => $prefix . 'select_rev_slider',
				'desc'        => __( 'Select Revolution Slider, only if home page template selected.', 'rwmb' ),
                'type'        => 'select',
                'options'     => $revolutionslider,
                'multiple'    => false,
                'placeholder' => __('', 'rwmb')
            )
        )
    );
	
    foreach ($meta_boxes as $meta_box) {
        new RW_Meta_Box($meta_box);
    }
}