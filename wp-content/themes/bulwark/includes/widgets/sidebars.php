<?php
/**
 * Registers widget areas.
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 *
 */
if ( ! function_exists('tcsn_widgets_init') ) {

// Register Sidebar
function tcsn_widgets_init()  {
	
	// Blog Widgets
	register_sidebar( array(
		'name'          => __( 'Blog Widgets', 'tcsn_theme' ),
		'id'            => 'widgets-blog',
		'description'   => __( 'This area will be shown as a post sidebar. Widgets will be stacked in this column.', 'tcsn_theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );

	// Page Widgets
	register_sidebar( array(
		'name'          => __( 'Page Widgets', 'tcsn_theme' ),
		'id'            => 'widgets-page',
		'description'   => __( 'This area will be shown as a page sidebar. Widgets will be stacked in this column.', 'tcsn_theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6 class="widget-title">',
		'after_title'   => '</h6>',
	) );

	//Footer Widgets (columns) - Dynamic
	global $smof_data;
	$footer_columns = $smof_data['tcsn_columns_footer'];
	for ($i=1; $i<=$footer_columns; $i++)
	{
		register_sidebar(array(
		'name' => 'Footer - column - '.$i,
		'description'   => __( 'This area is a dynamically generated footer widget column. Widgets will be stacked in here.', 'tcsn_theme' ),
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h5 class="widget-title">',
		'after_title'   => '</h5>',
		));
	}
}
// Hook into the 'widgets_init' action
add_action( 'widgets_init', 'tcsn_widgets_init' );
}