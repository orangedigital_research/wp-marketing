<?php
/**
 * Template Name: Home Page
 *
 * The Template for displaying home page. This is with slider and without page header.
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php get_header(); ?>
<?php global $smof_data; ?>

<?php get_template_part( 'includes/templates/sliders/slider' ); ?>
<section id="content-main" class="pad-bottom-none">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>
                </article>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- #content -->

<?php get_footer(); ?>
