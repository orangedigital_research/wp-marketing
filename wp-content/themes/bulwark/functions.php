<?php
/**
 * Bulwark functions and definitions.
 *
 * By using a child theme (http://codex.wordpress.org/Theme_Development
 * and http://codex.wordpress.org/Child_Themes), you can override certain
 * functions (those wrapped in a function_exists() call) by defining them first
 * in your child theme's functions.php file. The child theme's functions.php
 * file is included before the parent theme's file, so the child theme
 * functions would be used.
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
// Helper functions
require_once (get_template_directory() . '/includes/helpers.php');

// Theme widgets / Sidebars
require_once (get_template_directory() . '/includes/widgets/sidebars.php');
require_once (get_template_directory() . '/includes/widgets/custom-widgets.php');

// Custom styles / Google fonts
require_once (get_template_directory() . '/includes/custom-styles.php');
require_once (get_template_directory() . '/includes/googlefonts.php');

// Meta boxes
require_once (get_template_directory() . '/includes/meta-box/meta-box.php');

// Icons Fonts in array
require_once (get_template_directory() . '/includes/icon-font-array.php');

// Image resizer
require_once (get_template_directory() . '/includes/aq_resizer.php');

// Custom Post Types
require_once (get_template_directory() . '/includes/plugins/portfolio-post-types.php');
require_once (get_template_directory() . '/includes/plugins/testimonial-post-types.php');
require_once (get_template_directory() . '/includes/plugins/team-post-types.php');

// Recommend some useful plugins for this theme via TGMA script
require_once( get_template_directory() .'/includes/include-plugins.php' );

/**
 * Sets up the content width value based on the theme's design.
 *
 */
if ( ! isset( $content_width ) )
	$content_width = 1170;

/**
 * Theme only works in WordPress 3.6 or later.
 *
 */
if ( version_compare( $GLOBALS['wp_version'], '3.6-alpha', '<' ) )
	require get_template_directory() . '/includes/bk-compatiblity.php';

/**
 * Sets up theme defaults and registers the various WordPress features that theme supports.
 *
 */
function tcsn_theme_setup() {
	
	// Makes theme available for translation.
	// Translations can be added to the /languages/ directory.
	load_theme_textdomain( 'tcsn_theme', get_template_directory() . '/languages' );
	
    //  Styles the visual editor to resemble the theme style,
	add_editor_style( 'css/editor-style.css' );

    //  Adds RSS feed links to <head> for posts and comments.
	add_theme_support( 'automatic-feed-links' );

    // Switches default core markup for search form, comment form, and comments 
	// to output valid HTML5.
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list' ) );

	// This theme supports all available post formats by default.
	add_theme_support( 'post-formats', array(
		'audio', 'gallery', 'image', 'link', 'quote', 'video'
	) );

	// Add Menu Support
    register_nav_menus( array(
        'primary_menu'   => 'Primary menu',
    ) );

    // Thumbnail support
	add_theme_support( 'post-thumbnails' );
	
}
add_action( 'after_setup_theme', 'tcsn_theme_setup' );

/**
 * Enqueue Scripts and Styles
 *
 */
function tcsn_theme_scripts_styles() {
	
		// enqueue scripts
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '3.0.2', true );
		wp_enqueue_script( 'theme-scripts', get_template_directory_uri() . '/js/theme-scripts.js', array('jquery'), '1.0.0', true );
		wp_enqueue_script( 'theme-custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), '1.0.0', true );
		
		// enqueue styles
		wp_enqueue_style( 'bootstrap-style', get_template_directory_uri() . '/css/bootstrap.min.css' );
		wp_enqueue_style( 'font', get_template_directory_uri() . '/css/fontello.css' );
	    wp_enqueue_style( 'style-main', get_stylesheet_directory_uri() . '/style.css' );
		wp_enqueue_style( 'vc-style', get_template_directory_uri() . '/css/vc-override.css' );
		wp_enqueue_style( 'isotope-style', get_template_directory_uri() . '/css/isotope.css' );
		wp_enqueue_style( 'prettyPhoto-style', get_template_directory_uri() . '/css/prettyPhoto.css' );
		wp_enqueue_style( 'owlcarousel-style', get_template_directory_uri() . '/css/owl.carousel.css' );
		wp_enqueue_style( 'animate-style', get_template_directory_uri() . '/css/animate.css' );
	    wp_enqueue_style( 'responsive-style', get_template_directory_uri() . '/css/responsive.css' );

		// register style
		wp_register_style( 'bootstrap-nonrs-style', get_template_directory_uri() . '/css/non-responsive.css' );
	
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	
	global $smof_data;
	// To disable responsiveness
	if( $smof_data['tcsn_layout_responsive'] == 0 ) {
		wp_enqueue_style( 'bootstrap-nonrs-style' );
	}
}
add_action( 'wp_enqueue_scripts', 'tcsn_theme_scripts_styles' );

/**
 * Include SMOF - Slightly Modified Options Framework
 *
 */
require_once('admin/index.php'); 

/**
 * Allow shortcodes in sidebar widgets	
 *
 */
add_filter( 'widget_text', 'do_shortcode' );

/**
 * Excerpt more string 
 */
if (!function_exists('tcsn_excerpt_more')) {
function tcsn_excerpt_more( $more ) {
    return '..';
}
}
add_filter( 'excerpt_more', 'tcsn_excerpt_more' );

/**
 * Remove WP Generator Meta Tag from head
 */
remove_action( 'wp_head', 'wp_generator' );  


/**
 * Custom callback function for comment display
 *
 */
function tcsn_comment( $comment, $args, $depth ) {
$GLOBALS['comment'] = $comment; ?>

<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
    <div id="div-comment-<?php comment_ID(); ?>" class="comment-body">
        <div class="comment-author vcard">
            <?php if ( 0 != $args['avatar_size'] ) echo get_avatar( $comment, $args['avatar_size'] ); ?>
        </div>
        <?php printf( __( '<cite class="fn custom-fn">%s</cite> <span class="says">says:</span>' ), get_comment_author_link() ); ?>
        <div class="comment-meta comment-metadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
            <?php
		/* translators: 1: date, 2: time */
		printf( __( '%1$s at %2$s', 'tcsn_theme' ), get_comment_date(),  get_comment_time() ); ?>
            </a>
            <?php edit_comment_link( __( '(Edit)', 'tcsn_theme' ), '&nbsp;&nbsp;', '' );
	?>
            <div class="reply">
                <?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
            </div>
        </div>
        <div class="comment-text">
            <?php comment_text() ?>
        </div>
        <?php if ( '0' == $comment->comment_approved ) : ?>
        <p class="comment-awaiting-moderation">
            <?php _e( 'Your comment is awaiting moderation.', 'tcsn_theme' ) ?>
        </p>
        <?php endif; ?>
    </div>
    <?php
} // end comment callback function


if ( ! function_exists( 'tcsn_post_meta' ) ) :
/**
 *
 * Prints HTML with meta information for current post: author, date.
 *
 */
function tcsn_post_meta() {
	
	// Post date
	if ( ! has_post_format( 'link' ) && 'post' == get_post_type() )
		tcsn_post_date();
		
	// Post author
	if ( 'post' == get_post_type() ) {
		printf( '<span class="author vcard margin-less">By <br><a class="url fn n" href="%1$s" title="%2$s" rel="author">%3$s</a></span>',
			esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ),
			esc_attr( sprintf( __( 'View all posts by %s', 'tcsn_theme' ), get_the_author() ) ),
			get_the_author()
		);
	}
}
endif;

if ( ! function_exists( 'tcsn_post_meta_second' ) ) :
/**
 *
 * Prints HTML with meta information for current post: categories, tags
 *
 */
function tcsn_post_meta_second() {
	
	// Categories
	$categories_list = get_the_category_list( __( ', ', 'tcsn_theme' ) );
	if ( $categories_list ) {
		echo 'In : <span class="categories-links">' . $categories_list . '</span>';
	}
	
	// Tags
	$tag_list = get_the_tag_list( '', __( ', ', 'tcsn_theme' ) );
	if ( $tag_list ) {
		echo '<span class="meta-separator">.</span>Tagged : <span class="tags-links">' . $tag_list . '</span>';
	}
}
endif;

if ( ! function_exists( 'tcsn_post_date' ) ) :
/**
 *
 * Prints HTML with date information for current post.
 *
 */
function tcsn_post_date( $echo = true ) {
	if ( has_post_format( array( 'chat', 'status' ) ) )
		$format_prefix = _x( '%1$s on %2$s', '1: post format name. 2: date', 'tcsn_theme' );
	else
		$format_prefix = '%2$s';
	$date = sprintf( '<span class="post-date">On<br><a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a></span>',
		esc_url( get_permalink() ),
		esc_attr( sprintf( __( 'Permalink to %s', 'tcsn_theme' ), the_title_attribute( 'echo=0' ) ) ),
		esc_attr( get_the_date( 'c' ) ),
		esc_html( sprintf( $format_prefix, get_post_format_string( get_post_format() ), get_the_date() ) )
	);
	if ( $echo )
		echo $date;
	return $date;
}
endif;

/**
 *
 * Visual Composer
 *
 */
if( function_exists('vc_set_as_theme') ) {
	
	//  Initialize Visual Composer as a part of theme
	vc_set_as_theme(true);

	// Add, Remove params
	require_once( get_template_directory() . '/includes/page-builder/remove-params-elements.php' );
	require_once( get_template_directory() . '/includes/page-builder/add-params.php' );

	// Remove VC Custom Teaser metabox
	if ( is_admin() ) {
		if ( ! function_exists('tcsn_remove_vc_custom_teaser') ) {
			function tcsn_remove_vc_custom_teaser(){
				$post_types = get_post_types( '', 'names' ); 
				foreach ( $post_types as $post_type ) {
					remove_meta_box('vc_teaser',  $post_type, 'side');
				}
			} 
		} 
	add_action('do_meta_boxes', 'tcsn_remove_vc_custom_teaser');
	}
	
/**
 * Custom Shortcodes in Visual Composer
 */


// Recent Posts Carousel
function tcsn_bw_recentpost_sc( $atts, $content = null ) {
    extract ( shortcode_atts( array(
		'title'     => '',
		'thumbnail'	=> '',
		'excerpt'   => '',
		'limit'     => -1,
		'order'     => '',
		'orderby'   => '',
		'cat'	    => '',
	), $atts ) );

	$cat = str_replace(' ','-',$cat);
	 
	global $post;
	$args = array(
		'post_type'      => '',
		'posts_per_page' => esc_attr( $limit ),
		'order'          => esc_attr( $order ), 
		'orderby'        => $orderby,
		'post_status'    => 'publish',
		'category_name'  => $cat, 
	);

	query_posts( $args );
	$output = '';
	if( have_posts() ) : 
		$output .= '<div class="recentpost-carousel wpb_custom_element">';
		while ( have_posts() ) : the_post();
			$output .= '<div class="item clearfix">';
			
			// title
			if( $title !== 'yes' ):
				$permalink = get_permalink();
				$output .= '<h5 class="recentpost-heading"><a href="' . $permalink . '" rel="bookmark">' . get_the_title() . '</a></h5>';
			endif;	
			
			// thumbnail
			if( $thumbnail !== 'yes' ):
				if( has_post_thumbnail() ) { 
					$output .=  get_the_post_thumbnail(); 
				} 
			endif;	
			
			// excerpt
			if($excerpt!=='yes'):	
				$output .= '<div class="recentpost-excerpt">';
				$content = get_the_excerpt();
				$content = wp_trim_words( $content , '35' );
				$content = str_replace( ']]>', ']]&gt;', $content );
				$output .= $content;
				$output .= '</div>';
			endif;	

			$output .= '<span class="recentpost-date">' . get_the_date() . '</span>';
			$output .= '<span class="recentpost-comment"><i class="icon-chat"></i><a href="' . get_permalink(get_the_ID()) . '">' . get_comments_number(get_the_ID()) . ' ' . __('Replies', 'tcsn_theme') . '</a></span>';
			$output .= '</div>';
		endwhile;
		$output .= '</div>';
		wp_reset_query();
	endif;
	return $output;
}
add_shortcode('recent_post', 'tcsn_bw_recentpost_sc');

vc_map( array(
   "name"     => __( "Recent Post", "tcsn_theme" ),
   "base"     => "recent_post",
   "class"    => '',
   "icon"	  => "icon-wpb-bartag",
   "category" => __( 'Content', 'tcsn_theme' ),
   "params"   => array(
   	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Number of Posts to Show in Carousel", "tcsn_theme" ),
		"param_name"  => "limit",
		"value"       => __( "2", "tcsn_theme" ),
		"description" => '',
		),
	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Filter by Category", "tcsn_theme" ),
		"param_name"  => "cat",
		"value"       => '',
		"description" => "Filter output by posts categories, enter category names here. Separate with commas.",
		),
	array(
		"type"        => "dropdown",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Sort Posts By", "tcsn_theme" ),
		"param_name"  => "orderby",
		"value"       => array ( 
			"Date"   => "date", 
			"Random" => "rand", 
			"Author" => "author", 
			"Title"  => "title", 
			),
		"description" => '',
		),
	array(
		"type"        => "dropdown",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Arrange Sorted Posts", "tcsn_theme" ),
		"param_name"  => "order",
		"value"       => array ( "Descending" => "DESC", "Ascending" => "ASC" ),
		"description" => '',
		),
	array(
		"type"        => "checkbox",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Hide Title", "tcsn_theme" ),
		"param_name"  => "title",
		"value"       => array ( "Yes, please" => "yes" ),
		"description" => '',
		),
	array(
		"type"        => "checkbox",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Hide Thumbnail", "tcsn_theme" ),
		"param_name"  => "thumbnail",
		"value"       => array ( "Yes, please" => "yes" ),
		"description" => '',
		),
	array(
		"type"        => "checkbox",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Hide Post Excerpt", "tcsn_theme" ),
		"param_name"  => "excerpt",
		"value"       => array ( "Yes, please" => "yes" ),
		"description" => '',
		),
	)
) );

// Mini Divider
function tcsn_bw_minidivider_sc( $atts, $content = null ) { 
   extract( shortcode_atts( array(
     	'border_color'	=> '',
		'mr_top'        => '',
		'mr_bottom'   	=> '',
   ), $atts ) );
   
   if( $border_color != ''  ) {
		$return_border_color = 'border-top: 1px solid ' . $border_color . ';';
  	} else {
		$return_border_color = '';
	}
	
   if( $mr_top != ''  ) {
		$return_mr_top = 'margin-top:' . $mr_top . ';';
  	} else {
		$return_mr_top = '';
	}

	if( $mr_bottom != ''  ) {
		$return_mr_bottom = 'margin-bottom:' . $mr_bottom . ';';
  	} else {
		$return_mr_bottom = '';
	}
	
	$content = wpb_js_remove_wpautop($content); 

	return "<div class='mini-divider' style='{$return_mr_top}{$return_mr_bottom}{$return_border_color}'></div>";
}
add_shortcode( 'mini_divider', 'tcsn_bw_minidivider_sc' );

vc_map( array(
   "name"     => __( "Mini Divider", "tcsn_theme" ),
   "base"     => "mini_divider",
   "class"    => '',
   "icon"     => "icon-wpb-bartag",
   "category" => __( 'Content', 'tcsn_theme' ),
   "params"   => array(
   	array(
		"type"        => "colorpicker",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Divider Color", "tcsn_theme" ),
		"param_name"  => "border_color",
		"value"       => '',
		"description" => __( "Leave blank / clear for theme default", "tcsn_theme" )
		),
	   
	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Margin top", "tcsn_theme" ),
		"param_name"  => "mr_top",
		"value"       => '',
		"description" => __( "Not necessary always. Provide it in px, like : 20px", "tcsn_theme" )
		),
		
	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Margin Bottom", "tcsn_theme" ),
		"param_name"  => "mr_bottom",
		"value"       => "20px",
		"description" => __( "Default is 20px. Provide it in px, like : 20px", "tcsn_theme" )
		),
	)
) );

// Fancy heading With Big Text Below
function tcsn_bw_tooltip_heading_sc( $atts, $content = null ) { 
   extract( shortcode_atts( array(
     	'title'  	=> '', 
		'color'  	=> '',
		'bg_color'	=> '',
		), $atts ) );

	$add_style = array();
		if ( $bg_color ) {
			$add_style[] = 'background-color: '. $bg_color .';';
		} 
		if ( $color ) {
			$add_style[] = 'color: '. $color .';';
		} 
		$add_style = implode('', $add_style);

		if ( $add_style ) {
			$add_style = wp_kses( $add_style, array() );
			$add_style = ' style="' . esc_attr($add_style) . '"';
		}
		
		if( $bg_color != ''  ) {
		$return_bg_color = ' style="border-right: 6px solid ' . $bg_color . '; border-top: 7px solid ' . $bg_color . ';"';
  		} else {
		$return_bg_color = '';
		}

   $content = wpb_js_remove_wpautop($content); 
   return "<div class='wpb_custom_element'><div class='mytooltip'{$add_style}><h5>{$title}</h5><div class='mytooltip-arrow'{$return_bg_color}></div></div><div class='text-big'>{$content}</div></div>";
}
add_shortcode( 'tooltip_heading', 'tcsn_bw_tooltip_heading_sc' );

vc_map( array(
   "name"     => __( "Fancy Heading with Big Text Below", "tcsn_theme" ),
   "base"     => "tooltip_heading",
   "class"    => '',
   "icon"     => "icon-wpb-bartag",
   "category" => __( 'Content', 'tcsn_theme' ),
   "params"   =>   array(
	array(
		"type"       => "textfield",
		"holder"     => "div",
		"class"      => '',
		"heading"    => __( "Title", "tcsn_theme" ),
		"param_name" => "title",
		"value"      => __( "Title", "tcsn_theme" ),
	  ),
	  array(
		"type"        => "colorpicker",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Heading background Color", "tcsn_theme" ),
		"param_name"  => "bg_color",
		"value" 	   => '', 
		"description" => __( "Leave blank / clear for theme default", "tcsn_theme" )
	  ),
	  array(
		 "type"        => "colorpicker",
		 "holder"      => "div",
		 "class"       => '',
		 "heading"     => __( "Heading Text Color", "tcsn_theme" ),
		 "param_name"  => "color",
		 "value" 	   => '', 
		 "description" => __( "Leave blank / clear for theme default", "tcsn_theme" )
	  ),
	  array(
		 "type"        => "textarea_html",
		 "holder"      => "div",
		 "class"       => '',
		 "heading"     => __( "Content", "tcsn_theme" ),
		 "param_name"  => "content",
		 "value"       => __( "<p>I am a feature text block. Click edit button to change this text.</p>" , "tcsn_theme"),
		 "description" => __( "Enter your content.", "tcsn_theme" )
	  )
   )
) );

// Portfolio Carousel
function tcsn_bw_portfolio_sc( $atts, $content = null ) {
    extract ( shortcode_atts( array(
		'limit'       => -1,
		'order'	      => '',
		'orderby'     => '',
		'tax'         => '',
		'remove_link' => '',
	), $atts ) );
	
	if( $remove_link !== 'yes' ){
		$return_remove_link = '';
  	} else {
		$return_remove_link = ' remove-link-button';
	}
	
	global $post;
	$args = array(
		'post_type'          => 'tcsn_portfolio',
		'tcsn_portfoliotags' => $tax,
		'posts_per_page'     => esc_attr( $limit ),
		'order'              => esc_attr( $order ), 
		'orderby'            => $orderby,
		'post_status'        => 'publish',
	);
	
	query_posts( $args );
	$output = '';
	if( have_posts() ) :
		$output .= '<div class="portfolio-carousel wpb_custom_element">';	
		while ( have_posts() ) : the_post();
			$output .= '<div class="item clearfix">';
		 		$thumb       = get_post_thumbnail_id(); 
				$img_url     = wp_get_attachment_url( $thumb, 'full' ); 
				$image       = aq_resize( $img_url, 300, 200, true );
				$thumb_title = get_the_title();
				$img_link    =  get_permalink($post->ID);
				$output .= '<div class="folio-thumb ' . $return_remove_link . '"> <div class="overlay"></div><img src="' . $image . '" alt="' . $thumb_title . '"/><a href="' . $img_url . '" data-rel="prettyPhoto[gallery]" title="' . $thumb_title . '" class="zoom-button"></a><a href="' . $img_link . '" class="link-button" target="_blank" ' . $return_remove_link . '></a></div>';
			$output .= '</div>';
		endwhile;
		$output .= '</div>';
		wp_reset_query();
	endif;
	return $output;
}
add_shortcode('portfolio_carousel', 'tcsn_bw_portfolio_sc');

vc_map( array(
   "name"     => __( "Portfolio Carousel", "tcsn_theme" ),
   "base"     => "portfolio_carousel",
   "class"    => '',
   "icon"	  => "icon-wpb-bartag",
   "category" => __( 'Content', 'tcsn_theme' ),
   "params"   => array(
	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Number of posts to show in carousel", "tcsn_theme" ),
		"param_name"  => "limit",
		"value"       => __( "6", "tcsn_theme" ),
		"description" => '',
		),
	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Filter by Category", "tcsn_theme" ),
		"param_name"  => "tax",
		"value"       => '',
		"description" => "Enter --- <strong>CATEGORY SLUG</strong> --- here. Separate with commas.<br>Find category slug here : Portfolio Items > Portfolio Categories<br>This will help to group portfolio items from selected categories in one carousel.",
		),
	array(
		"type"        => "dropdown",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Sort Posts By", "tcsn_theme" ),
		"param_name"  => "orderby",
		"value"       => array (
			"Date"   => "date", 
			"Random" => "rand", 
			"Title"  => "title", 
			),
		"description" => '',
		),
	array(
		"type"        => "dropdown",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Arrange Sorted Posts", "tcsn_theme" ),
		"param_name"  => "order",
		"value"       => array ( "Descending" => "DESC", "Ascending" => "ASC"),
		"description" => '',
		),
	 array(
		"type"        => "checkbox",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Hide link", "tcsn_theme" ),
		"param_name"  => "remove_link",
		"value"       => array ( "Yes, please" => "yes" ),
		"description" => "Keep only zoom",
		),
	)
) );

// Box
function tcsn_bw_box_sc( $atts, $content = null ) { 
	extract( shortcode_atts( array(
		'color'        => '',
		'bg_color'     => '',
		'border_color' => '',
		'border_width' => '',
		), $atts ) );

	$add_style = array();
		if ( $bg_color ) {
			$add_style[] = 'background-color: '. $bg_color .';';
		} 
		if ( $border_color ) {
			$add_style[] = 'border: 1px solid ' . $border_color . ';';
		} 
		if ( $border_width ) {
			$add_style[] = 'border-width: ' . $border_width . ';';
		} 
		$add_style = implode('', $add_style);

		if ( $add_style ) {
			$add_style = wp_kses( $add_style, array() );
			$add_style = ' style="' . esc_attr($add_style) . '"';
		}

	$content = wpb_js_remove_wpautop($content); 
    return "<div class='box{$color} wpb_custom_element'{$add_style}>{$content}</div>";
	
}
add_shortcode( 'box', 'tcsn_bw_box_sc' );

vc_map( array(
   "name"     => __( "Box", "tcsn_theme" ),
   "base"	  => "box",
   "class"    => '',
   "icon"     => "icon-wpb-bartag",
   "category" => __( 'Content', 'tcsn_theme' ),
   "params"   => array(
   	array(
		 "type"        => "colorpicker",
		 "holder"      => "div",
		 "class"       => '',
		 "heading"     => __( "Background Color", "tcsn_theme" ),
		 "param_name"  => "bg_color",
		 "value" 	   => '', 
		 "description" => __( "Leave blank / clear for theme default", "tcsn_theme" )
		 ),
	array(
		"type"         => "colorpicker",
		 "holder"      => "div",
		 "class"       => '',
		 "heading"     => __( "Border Color", "tcsn_theme" ),
		 "param_name"  => "border_color",
		 "value" 	   => '', 
		 "description" => __( "Leave blank / clear for no border.", "tcsn_theme" )
		 ),
	array(
		 "type"        => "textfield",
		 "holder"      => "div",
		 "class"       => '',
		 "heading"     => __( "Border Width", "tcsn_theme" ),
		 "param_name"  => "border_width",
		 "value" 	   => '', 
		 "description" => __( "Border width. Ex: 1px 0 1px 0 (top, right, bottom, left)", "tcsn_theme" ),
		 ),
   array(
		"type"        => "dropdown",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Typography", "tcsn_theme" ),
		"param_name"  => "color",
		"value"       => array ( "Dark text - For light backgrounds" => '', "Light Text- For dark backgrounds" => " dark-bg-text", ),
		"description" => "Derk text is theme defult",
	    ),
   array(
		"type"        => "textarea_html",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Content", "tcsn_theme" ),
		"param_name"  => "content",
		"value"       => __( "<p>I am box text block. Click edit button to change this text.</p>", "tcsn_theme" ),
		"description" => __( "Enter your content.", "tcsn_theme" )
		),
   ),
) );

// Feature Item
function tcsn_bw_feature_item_sc( $atts, $content = null ) { 
	extract( shortcode_atts( array(
		'bg_image' => '',
		'bg_color' => '',
		'color'    => '',
		'title'    => '',
		'link'     => '', 
		'link_url' => '', 
		'target'   => '',
		), $atts ) );

	if( $bg_color != '' ) {
		$return_bg_color = ' style="background: ' . $bg_color . ';"';
  	} else {
		$return_bg_color = '';
	}
	
	if( $color != '' ) {
		$return_color = ' style="color: ' . $color . ';"';
  	} else {
		$return_color = '';
	}
	
	if( $link !== 'yes' ){
		$return_title = '<h5' . $return_color . '>' . $title . '</h5>';
  	} else {
		$return_title = '<h5><a href="' . $link_url . '" target="' . $target . '"' . $return_color . '>' . $title . '</a></h5>';
	}

	if ( $bg_image ) {
		$img_url  = wp_get_attachment_url($bg_image);
		$bg_image = $img_url;
	}
	return "<div class='feature-item wpb_custom_element'><img src='{$bg_image}'><div class='overlay'></div><div class='ribbon'{$return_bg_color}>{$return_title}</div></div>";
}
add_shortcode( 'feature_item', 'tcsn_bw_feature_item_sc' );

vc_map( array(
   "name"     => __( "Feature Item", "tcsn_theme" ),
   "base"	  => "feature_item",
   "class"    => '',
   "icon"     => "icon-wpb-bartag",
   "category" => __( 'Content', 'tcsn_theme' ),
   "params"   => array(
	array(
		"type"        => "attach_image",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Image", "tcsn_theme" ),
		"param_name"  => "bg_image",
		"value" 	  => '', 
		"description" => __( "Select background image from media library.", "tcsn_theme" )
		),
    array(
		"type"        => "colorpicker",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Ribbon Background Color", "tcsn_theme" ),
		"param_name"  => "bg_color",
		"value" 	  => '', 
		"description" => __( "Leave blank / clear for theme default", "tcsn_theme" )
		),
	array(
		"type"        => "colorpicker",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Ribbon Text Color", "tcsn_theme" ),
		"param_name"  => "color",
		"value" 	  => '', 
		"description" => __( "Leave blank / clear for theme default", "tcsn_theme" )
		),
	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Ribbon Title", "tcsn_theme" ),
		"param_name"  => "title",
		"value" 	  => "Featured", 
		"description" => '',
	  	),
	array(
		"type"        => "checkbox",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Make Ribbon Title a link", "tcsn_theme" ),
		"param_name"  => "link",
		"value"       => array ( "Yes, please" => "yes" ),
	  	),
	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "If Link provide URL here", "tcsn_theme" ),
		"param_name"  => "link_url",
		"value" 	  => '', 
		),
	array(
		"type"        => "dropdown",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Open Link in", "tcsn_theme" ),
		"param_name"  => "target",
		"value"       => array ( "New Window" => "_blank", "Same Window" => "_self" ),
		"description" => '',
		),
	),
) );

// List with icon
function tcsn_bw_icon_list_sc( $atts, $content = null ) {
	 extract( shortcode_atts( array(
     	'type'         => '', 
		'color'        => '',
		'size'         => '',
		'icon_color'   => '',
		'list_border'  => '', 
		'list_content' => '', 
   ), $atts ) );

   $add_style = array();
		if( $color != ''  ) {
			$add_style[] = 'color: '. $color .';';
		} 
		if( $size != ''  ) {
			$add_style[] = 'font-size: '. $size .';';
		} 
		$add_style = implode('', $add_style);

		if ( $add_style ) {
			$add_style = wp_kses( $add_style, array() );
			$add_style = ' style="' . esc_attr($add_style) . '"';
		}
		
	$add_style_icon = array();
		if( $icon_color != ''  ) {
			$add_style_icon[] = 'color: '. $icon_color .';';
		} 
		if( $size != ''  ) {
			$add_style_icon[] = 'font-size: '. $size .';';
		} 
		$add_style_icon = implode('', $add_style_icon);

		if ( $add_style_icon ) {
			$add_style_icon = wp_kses( $add_style_icon, array() );
			$add_style_icon = ' style="' . esc_attr($add_style_icon) . '"';
		}

	if($list_border == 'yes') {
		$return_list_border = ' list-icon-border';
	} else {
		$return_list_border = '';
	}
   
   return "<p class='list-icon{$return_list_border}'{$add_style}><i class='icon-{$type}'{$add_style_icon}></i>{$list_content}</p>";
}
add_shortcode( 'listicon', 'tcsn_bw_icon_list_sc' );

vc_map( array(
   "name"     => __( "List with icon", "tcsn_theme" ),
   "base"     => "listicon",
   "class"    => '',
   "icon"     => "icon-wpb-bartag",
   "category" => __( 'Content', 'tcsn_theme' ),
   "params"   => array(
	array(
		"type"        => "dropdown",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Select Icon", "tcsn_theme" ),
		"param_name"  => "type",
		"value"       => $font_icons_array, 
		),
	array(
         "type"        => "colorpicker",
         "holder"      => "div",
         "class"       => '',
         "heading"     => __( "Icon color", "tcsn_theme" ),
         "param_name"  => "icon_color",
         "value"       => '', 
         "description" => "Leave blank for same color as body font color.", 
		 ),
	array(
         "type"        => "colorpicker",
         "holder"      => "div",
         "class"       => '',
         "heading"     => __( "List text color", "tcsn_theme" ),
         "param_name"  => "color",
         "value"       => '', 
         "description" => "Leave blank for same color as body font color",
		 ),
	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Font Size", "tcsn_theme" ),
		"param_name"  => "size",
		 "value"      => '',
		"description" => __( "Give it as : 20px. Leave blank for same as body font size.", "tcsn_theme" )
	  	),
	array(
		"type"        => "checkbox",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Enable border bottom", "tcsn_theme" ),
		"param_name"  => "list_border",
		"value"       => array ( "Yes, please" => "yes" ),
		),
	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "List Content", "tcsn_theme" ),
		"param_name"  => "list_content",
		 "value"      => __( "I am list text block", "tcsn_theme" ),
		"description" => __( "Enter your content.", "tcsn_theme" )
	  	),
	)
) );

// Feature with Box
function tcsn_bw_feature_box_sc( $atts, $content = null ) { 
   extract( shortcode_atts( array(
   		'heading'    => '', 
		'subheading' => '', 
     	'icon_type'  => '', 
		'icon_color' => '', 
		'box'  => '', 
   ), $atts ) );
   
   $content = wpb_js_remove_wpautop($content);
   
   	
	if( $box !== 'yes' ){
		$box = '';
  	} else {
		$box = ' add-box';
	}
   
	if( $icon_color != ''  ) {
		$return_icon_color = ' style="color: ' . $icon_color . ';"';
	} else {
		$return_icon_color = '';
	}
	
	if( $content != ''  ) {
		$return_content = '<div class="feature-box-content">' . $content . '</div>';
	} else {
		$return_content = '';
	}
	return "<div class='feature-box{$box} wpb_custom_element'><div class='clearfix'><div class='feature-box-icon'><i{$return_icon_color} class='icon-{$icon_type}'></i></div><h4>{$heading}</h4><span class='subheading'>{$subheading}</span></div>{$return_content}</div>";
}
add_shortcode( 'featurebox', 'tcsn_bw_feature_box_sc' );

vc_map( array(
   "name"     => __( "Feature with Icon / Box", "tcsn_theme" ),
   "base"     => "featurebox",
   "class"    => '',
   "icon"     => "icon-wpb-bartag",
   "category" => __( 'Content', 'tcsn_theme' ),
   "params"   => array( 
	array(
		"type"        => "dropdown",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Select Icon", "tcsn_theme" ),
		"param_name"  => "icon_type",
		"value"       => $font_icons_array,
		),
	array(
		"type"        => "colorpicker",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Icon color", "tcsn_theme" ),
		"param_name"  => "icon_color",
		"value" 	  => '', 
		),
	array(
		 "type"        => "textfield",
		 "holder"      => "div",
		 "class"       => '',
		 "heading"     => __( "Heading", "tcsn_theme" ),
		 "param_name"  => "heading",
		 "value" 	   => '', 
	  	),
	array(
		 "type"        => "textfield",
		 "holder"      => "div",
		 "class"       => '',
		 "heading"     => __( "Subheading", "tcsn_theme" ),
		 "param_name"  => "subheading",
		 "value" 	   => '', 
		 ),
	array(
		"type"        => "checkbox",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Wrap this feature in a Box", "tcsn_theme" ),
		"param_name"  => "box",
		"value"       => array ( "Yes, please" => "yes" ),
		"description" => '',
		),
	array(
		"type"        => "textarea_html",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Content", "tcsn_theme" ),
		"param_name"  => "content",
		"value"       => '',
		"description" => __( "Enter your content.", "tcsn_theme" )
		),
	)
) );

// Person
function tcsn_bw_team_sc( $atts, $content = null ) {
	extract ( shortcode_atts( array(
		'id'      => '',
		'excerpt' => '',
	), $atts ) );
	      
	global $post;
	$args = array(
		'name'           => esc_attr( $id ),
		'post_type'      => 'tcsn_team',
		'posts_per_page' => '',
		'post_status'    => 'publish',
	);
	query_posts( $args );
	$output = '';
	
	if( have_posts() ) :
		$output .= '<div class="team wpb_custom_element clearfix">';	
		
		while ( have_posts() ) : the_post();
			$member_job = rwmb_meta('rw_member_job', 'type=text'); 
			$member_behance = rwmb_meta('rw_member_behance', 'type=text'); 
			$member_delicious = rwmb_meta('rw_member_delicious', 'type=text'); 
			$member_dribbble = rwmb_meta('rw_member_dribbble', 'type=text'); 
			$member_dropbox = rwmb_meta('rw_member_dropbox', 'type=text'); 
			$member_facebook = rwmb_meta('rw_member_facebook', 'type=text'); 
			$member_flickr = rwmb_meta('rw_member_flickr', 'type=text'); 
			$member_googleplus = rwmb_meta('rw_member_googleplus', 'type=text'); 
			$member_instagram = rwmb_meta('rw_member_instagram', 'type=text'); 
			$member_linkedin = rwmb_meta('rw_member_linkedin', 'type=text'); 
			$member_paypal = rwmb_meta('rw_member_paypal', 'type=text'); 
			$member_pinterest = rwmb_meta('rw_member_pinterest', 'type=text'); 
			$member_skype = rwmb_meta('rw_member_skype', 'type=text'); 
			$member_soundcloud = rwmb_meta('rw_member_soundcloud', 'type=text');
			$member_stumbleupon = rwmb_meta('rw_member_stumbleupon', 'type=text');
			$member_tumblr = rwmb_meta('rw_member_tumblr', 'type=text'); 
			$member_twitter = rwmb_meta('rw_member_twitter', 'type=text'); 	
			$member_viadeo = rwmb_meta('rw_member_viadeo', 'type=text'); 
			$member_vimeo = rwmb_meta('rw_member_vimeo', 'type=text'); 
			$member_youtube = rwmb_meta('rw_member_youtube', 'type=text');
			$member_mail = rwmb_meta('rw_member_mail', 'type=text');  

			$output .= '<div class="member-image">' . get_the_post_thumbnail($post->ID, 'full') . '</div>';
			
			if( $member_job != ''  ) {
			$output .= '<span class="member-job text-caps">' . $member_job . '</span>';	
			}
			
			$permalink = get_permalink();
			$output .= '<h4 class="member-name"><a href="' . $permalink . '" rel="bookmark">' . get_the_title() .'</a></h4>';
			$output .= '<div class="mini-divider"></div>';
			$output .= '<ul class="social clearfix">';
			
			if( $member_behance != ''  ) {
				$output .= '<li><a href="' . $member_behance . '" target="_blank" title="behance"><i class="icon-behance"></i></a></li>';
			}
			if( $member_delicious != ''  ) {
				$output .= '<li><a href="' . $member_delicious . '" target="_blank" title="delicious"><i class="icon-delicious"></i></a></li>';
			}
			if( $member_dribbble != ''  ) {
				$output .= '<li><a href="' . $member_dribbble . '" target="_blank" title="dribbble"><i class="icon-dribbble"></i></a></li>';
			}
			if( $member_dropbox != ''  ) {
				$output .= '<li><a href="' . $member_dropbox . '" target="_blank" title="dropbox"><i class="icon-dropbox"></i></a></li>';
			}
			if( $member_facebook != ''  ) {
				$output .= '<li><a href="' . $member_facebook . '" target="_blank" title="facebook"><i class="icon-facebook"></i></a></li>';
			}
			if( $member_flickr != ''  ) {
				$output .= '<li><a href="' . $member_flickr . '" target="_blank" title="flickr"><i class="icon-flickr"></i></a></li>';
			}
			if( $member_googleplus != ''  ) {
				$output .= '<li><a href="' . $member_googleplus . '" target="_blank" title="googleplus"><i class="icon-gplus"></i></a></li>';
			}
			if( $member_instagram != ''  ) {
				$output .= '<li><a href="' . $member_instagram . '" target="_blank" title="instagram"><i class="icon-instagram"></i></a></li>';
			}
			if( $member_linkedin != ''  ) {
				$output .= '<li><a href="' . $member_linkedin . '" target="_blank" title="linkedin"><i class="icon-linkedin"></i></a></li>';
			}
			if( $member_paypal != ''  ) {
				$output .= '<li><a href="' . $member_paypal . '" target="_blank" title="paypal"><i class="icon-paypal"></i></a></li>';
			}
			if( $member_pinterest != ''  ) {
				$output .= '<li><a href="' . $member_pinterest . '" target="_blank" title="pinterest"><i class="icon-pinterest"></i></a></li>';
			}
			if( $member_skype != ''  ) {
				$output .= '<li><a href="' . $member_skype . '" target="_blank" title="skype"><i class="icon-skype"></i></a></li>';
			}
			if( $member_soundcloud != ''  ) {
				$output .= '<li><a href="' . $member_soundcloud . '" target="_blank" title="soundcloud"><i class="icon-soundcloud"></i></a></li>';
			}
			if( $member_stumbleupon != ''  ) {
				$output .= '<li><a href="' . $member_stumbleupon . '" target="_blank" title="stumbleupon"><i class="icon-stumbleupon"></i></a></li>';
			}
			if( $member_tumblr != ''  ) {
				$output .= '<li><a href="' . $member_tumblr . '" target="_blank" title="tumblr"><i class="icon-tumblr"></i></a></li>';
			}
			if( $member_twitter != ''  ) {
				$output .= '<li><a href="' . $member_twitter . '" target="_blank" title="twitter"><i class="icon-twitter"></i></a></li>';
			}
			if( $member_viadeo != ''  ) {
				$output .= '<li><a href="' . $member_viadeo . '" target="_blank" title="viadeo"><i class="icon-viadeo"></i></a></li>';
			}
			if( $member_vimeo != ''  ) {
				$output .= '<li><a href="' . $member_vimeo . '" target="_blank" title="vimeo"><i class="icon-vimeo"></i></a></li>';
			}
			if( $member_youtube != ''  ) {
				$output .= '<li><a href="' . $member_youtube . '" target="_blank" title="youtube"><i class="icon-youtube"></i></a></li>';
			}
			if( $member_mail != ''  ) {
				$output .= '<li><a href="mailto:' . $member_mail . '" target="_blank" title="mail"><i class="icon-mail"></i></a></li>';
			}
			$output .= '</ul>';

			// excerpt
			if( $excerpt !== 'yes' ):	
			$output .= '<div class="team-excerpt">';
			$content = get_the_excerpt();
			$content = wp_trim_words( $content , '35' );
			$content = str_replace( ']]>', ']]&gt;', $content );
			$output .= $content;
			$output .= '</div>';
			endif;	
		endwhile;

		$output .= '</div>';

		wp_reset_query();
	endif;
	return $output;
}
add_shortcode('team', 'tcsn_bw_team_sc');

vc_map( array(
   "name"                 => __( "Team Member", "tcsn_theme" ),
   "base"                 => "team",
   "class"                => "",
   "icon"	              => "icon-wpb-bartag",
   "category"             => __( 'Content', 'tcsn_theme' ),
   "params"            => array(
   	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => "",
		"heading"     => __( "Person ID", "tcsn_theme" ),
		"param_name"  => "id",
		"value"       => __( "ID", "tcsn_theme" ),
		"description" => "",
		),
	array(
		"type"        => "checkbox",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Hide Excerpt", "tcsn_theme" ),
		"param_name"  => "excerpt",
		"value"       => array ( "Yes, please" => "yes" ),
		"description" => '',
		),
	)
) );

// Testimonial
function tcsn_bw_testimonial_sc( $atts, $content = null ) {
	extract ( shortcode_atts( array(
		'limit'     => -1,
		'order'     => '',
		'orderby'   => '',
	), $atts ) );
	
	global $post;
	$args = array(
		'post_type'      => 'tcsn_testimonial',
		'posts_per_page' => esc_attr( $limit ),
		'order'          => esc_attr( $order ), 
		'orderby'        => $orderby,
		'post_status'    => 'publish',
	);
	query_posts( $args );
	$output = '';
	if( have_posts() ) :
		$output .= '<div class="testimonial-carousel wpb_custom_element">';	
			while ( have_posts() ) : the_post();
				$output .= '<div class="item clearfix">';
				$output .= '<div class="testimonial-icon"><div class="clearfix">';
				$output .= '<i class="icon-quote"></i>';
				$output .= '<h6>' . get_the_title() .'</h6>';
				$client_info = rwmb_meta('rw_client_info', 'type=text'); 
				$output .= '<span class="testimonial-icon-subheading">' . $client_info . '</span>';	
				$output .= '</div>';
				$output .= '<div class="testimonial-icon-content">';
				$content = get_the_content();
				$content = apply_filters( 'the_content', $content );
				$content = str_replace( ']]>', ']]&gt;', $content );
				$output .= $content;
				$output .= '</div>';
				$output .= '</div>';
				$output .= '</div>';
			endwhile;
		$output .= '</div>';
		wp_reset_query();
	endif;
	return $output;
}
add_shortcode('testimonial', 'tcsn_bw_testimonial_sc');

vc_map( array(
   "name"                    => __( "Testimonial", "tcsn_theme" ),
   "base"                    => "testimonial",
   "class"                   => '',
   "icon"	                 => "icon-wpb-bartag",
   "category"                => __( 'Content', 'tcsn_theme' ),
    "params"     => array(
   	array(
		"type"        => "textfield",
		"holder"      => "div",
		"class"       => "",
		"heading"     => __( "Number of Testimonial to Show in Carousel", "tcsn_theme" ),
		"param_name"  => "limit",
		"value"       => __( "4", "tcsn_theme" ),
		"description" => "",
		),
	array(
		"type"        => "dropdown",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Sort Testimonials By", "tcsn_theme" ),
		"param_name"  => "orderby",
		"value"       => array ( 
			"Date"   => "date", 
			"Random" => "rand", 
			"Title"  => "title", 
			),
		"description" => '',
		),
	array(
		"type"        => "dropdown",
		"holder"      => "div",
		"class"       => '',
		"heading"     => __( "Arrange Sorted Testimonials", "tcsn_theme" ),
		"param_name"  => "order",
		"value"       => array ( "Descending" => "DESC", "Ascending" => "ASC" ),
		"description" => '',
		),
	)
) );

// Pricing
function tcsn_bw_pricing_sc( $atts, $content = null ) {
    extract ( shortcode_atts( array(
		'title'          => '',
		'price'          => '',
		'footnote'       => '',
		'class'          => '', 
	  	'color'          => '',
	  	'target'         => '', 
	  	'url'            => '', 
	  	'icon'           => '', 
	    'table'          => '', 
	    'button_content' => '',
	), $atts ) );
	
	if( $icon != ''  ) {
		$return_icon = '<i class="icon-' . $icon . '"></i>';
  	} else {
		$return_icon = '';
	}
	if( $url != ''  ) {
		$return_url = ' href="' . $url . '"';
	} else {
		$return_url = '';
	}
	if( $footnote != ''  ) {
		$return_footnote = '<p>' . $footnote . '</p>';
  	} else {
		$return_footnote = '';
	}

	$content = wpb_js_remove_wpautop($content); 
	
    return "<div class='pricing {$table} wpb_custom_element'><table><thead><tr><th><h3 class='pricing-title'>{$title}</h3></th></tr></thead><tfoot><tr><td class='footnote'>{$return_footnote}</td></tr></tfoot><tbody><tr><td>{$content}</td></tr><tr><td class='price'>{$price}</td></tr><tr><td class='focus-td'><a class='{$class} {$color}' target='{$target}'{$return_url}>{$button_content}{$return_icon}</a></td></tr></tbody></table></div>";
}
	
add_shortcode('pricing', 'tcsn_bw_pricing_sc');

vc_map( array(
   "name"                 => __( "Pricing", "tcsn_theme" ),
   "base"                 => "pricing",
   "class"                => "",
   "icon"                 => "icon-wpb-bartag",
   "category"             => __( 'Content', 'tcsn_theme' ),
   "params"               => array(
		array(
			"type"        => "textfield",
			"holder"      => "div",
			"class"       => "",
			"heading"     => __( "Title", "tcsn_theme" ),
			"param_name"  => "title",
			"value"       => "",
			"description" => "",
		),
		
		array(
			"type"        => "textfield",
			"holder"      => "div",
			"class"       => "",
			"heading"     => __( "Price", "tcsn_theme" ),
			"param_name"  => "price",
			"value"       => __( "$9.99 /mo", "tcsn_theme" ),
			"description" => "",
		),
		
				array(
			"type"        => "textfield",
			"holder"      => "div",
			"class"       => "",
			"heading"     => __( "Footnote", "tcsn_theme" ),
			"param_name"  => "footnote",
			"value"       => "",
			"description" => "",
		),
		
		array(
			"type"        => "dropdown",
		    "holder"      => "div",
		    "class"       => "",
		    "heading"     => __( "Button Style", "tcsn_theme" ),
		    "param_name"  => "class",
		    "value"       => array ( "Normal" => "mybtn", "Big" => "mybtn mybtn-big ", ),
		    "description" => "",
	    ),
		
		array(
		    "type"        => "dropdown",
		    "holder"      => "div",
		    "class"       => "",
		    "heading"     => __( "Button Color", "tcsn_theme" ),
		    "param_name"  => "color",
		    "value"       => array ( "Default" => "", "Green" => "mybtn-green", "Red" => "mybtn-red", "Blue" => "mybtn-blue", "Turquoise" => "mybtn-turquoise", "Grey" => "mybtn-grey", "Dark Goldenrod" => "mybtn-golden", "Violet Red" => "mybtn-violet", "Royal Blue" => "mybtn-royal", ),
		    "description" => "",
	    ),
	  
	    array(
		    "type"        => "dropdown",
		    "holder"      => "div",
		    "class"       => "",
		    "heading"     => __( "Select Icon for button", "tcsn_theme" ),
		    "param_name"  => "icon",
		    "value"       => $font_icons_array,
		    "description" => "",
	    ),
		
	   array(
	   		"type"        => "textarea",
		    "holder"      => "div",
		    "class"       => "",
		    "heading"     => __( "Button link text", "tcsn_theme" ),
		    "param_name"  => "button_content",
		    "value"       => __( "Link", "tcsn_theme" ),
		    "description" => "",
       ),
	  
	  array(
		   "type"         => "textfield",
		   "holder"       => "div",
		   "class"        => "",
		   "heading"      => __( "Button link URL", "tcsn_theme" ),
		   "param_name"   => "url",
		   "value"        => "",
		   "description"  => "",
	  ),
	  
	  array(
	  		"type"        => "dropdown",
		    "holder"      => "div",
		    "class"       => "",
		    "heading"     => __( "Open link in", "tcsn_theme" ),
		    "param_name"  => "target",
		    "value"       => array ( "New Window" => "_blank", "Same Window" => "_self", ),
		    "description" => "",
	   ),
	   
	   array(
	  		"type"        => "dropdown",
		    "holder"      => "div",
		    "class"       => "",
		    "heading"     => __( "Table type", "tcsn_theme" ),
		    "param_name"  => "table",
		    "value"       => array ( "Normal" => "default-table", "Featured" => "featured-table", ),
		    "description" => "",
	   ),
	  
	  array(
			"type"        => "textarea_html",
			"holder"      => "div",
			"class"       => "",
			"heading"     => __( "Content", "tcsn_theme" ),
			"param_name"  => "content",
			"value"       => __( "<p>I am CTA box text block. Click edit button to change this text.</p>", "tcsn_theme" ),
			"description" => __( "Enter your content.", "tcsn_theme" )
		)
	)
) );

// Add new shortcode
}