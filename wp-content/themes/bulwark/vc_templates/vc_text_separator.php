<?php
extract(shortcode_atts(array(
    'title' => '',
    'title_align' => '',
    'el_width' => '',
    'style' => '',
    'color' => '',
    'accent_color' => '',
    'el_class' => '',
	'font_size' => '',
	'font_weight' => '',
	'font_color' => '',
	'margin_top' => '',
	'margin_bottom' => '',
), $atts));
$class = "vc_separator wpb_content_element";

//$el_width = "90";
//$style = 'double';
//$title = '';
//$color = 'blue';

$class .= ($title_align!='') ? ' vc_'.$title_align : '';
$class .= ($el_width!='') ? ' vc_el_width_'.$el_width : ' vc_el_width_100';
$class .= ($style!='') ? ' vc_sep_'.$style : '';
$class .= ($color!='') ? ' vc_sep_color_'.$color : '';

$inline_css = ($accent_color!='') ? ' style="border-color: '.$accent_color.';"' : '';

$class .= $this->getExtraClass($el_class);

// Main Styles
$main_css = array();
	
	if ( $margin_top ) {
		$main_css[] = 'margin-top: '. $margin_top .'px;';
	}
	
	if ( $margin_bottom ) {
		$main_css[] = 'margin-bottom: '. $margin_bottom .'px;';
	}

$main_css = implode('', $main_css);

if ( $main_css ) {
	$main_css = wp_kses( $main_css, array() );
	$main_css = ' style="' . esc_attr($main_css) . '"';
}

// Text Styles
$text_style = array();

	if ( $font_size ) {
		$text_style[] = 'font-size: '. $font_size .'px;';
	}
	
	if ( $font_weight ) {
		$text_style[] = 'font-weight: '. $font_weight .' !important;';
	}
	
	if ( $font_color ) {
		$text_style[] = 'color: '. $font_color .';';
	}

$text_style = implode('', $text_style);

if ( $text_style ) {
	$text_style = wp_kses( $text_style, array() );
	$text_style = ' style="' . esc_attr($text_style) . '"';
}

$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, $class, $this->settings['base']);

?>
<div class="<?php echo esc_attr(trim($css_class)); ?>" <?php echo $main_css ?>>
	<span class="vc_sep_holder vc_sep_holder_l"><span<?php echo $inline_css; ?> class="vc_sep_line"></span></span>
	<?php if($title!=''): ?><h4 <?php echo $text_style ?>><?php echo $title; ?></h4><?php endif; ?>
	<span class="vc_sep_holder vc_sep_holder_r"><span<?php echo $inline_css; ?> class="vc_sep_line"></span></span>
</div>
<?php echo $this->endBlockComment('separator')."\n";