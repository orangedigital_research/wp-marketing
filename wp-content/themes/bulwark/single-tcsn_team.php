<?php
/**
 * The template for displaying team member details.
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php get_header(); ?>
<?php global $smof_data; ?>

<section id="page-header" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php if( $smof_data['tcsn_team_title'] != "" ) { ?>
                <h3 class="page-title"><?php echo $smof_data['tcsn_team_title']; ?></h3>
                <?php } ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php get_template_part( 'includes/templates/headers/breadcrumb' ); ?>
            </div>
        </div>
    </div>
</section>
<!-- #page-header -->

<section id="content-main" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 team-single">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="row team">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <?php if ( has_post_thumbnail() ) { ?>
                            <div class="member-image"> <?php echo '' . get_the_post_thumbnail( $post->ID, 'full', array( 'title' => '' ) ) . '';  ?></div>
                            <?php } ?>
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12"> 
                        
                         <?php 
									$member_job = rwmb_meta('rw_member_job', 'type=text'); 
									$member_behance = rwmb_meta('rw_member_behance', 'type=text'); 
									$member_delicious = rwmb_meta('rw_member_delicious', 'type=text'); 
									$member_dribbble = rwmb_meta('rw_member_dribbble', 'type=text'); 
									$member_dropbox = rwmb_meta('rw_member_dropbox', 'type=text'); 
									$member_facebook = rwmb_meta('rw_member_facebook', 'type=text'); 
									$member_flickr = rwmb_meta('rw_member_flickr', 'type=text'); 
									$member_googleplus = rwmb_meta('rw_member_googleplus', 'type=text'); 
									$member_instagram = rwmb_meta('rw_member_instagram', 'type=text'); 
									$member_linkedin = rwmb_meta('rw_member_linkedin', 'type=text'); 
									$member_paypal = rwmb_meta('rw_member_paypal', 'type=text'); 
									$member_pinterest = rwmb_meta('rw_member_pinterest', 'type=text'); 
									$member_skype = rwmb_meta('rw_member_skype', 'type=text'); 
									$member_soundcloud = rwmb_meta('rw_member_soundcloud', 'type=text');
									$member_stumbleupon = rwmb_meta('rw_member_stumbleupon', 'type=text');
									$member_tumblr = rwmb_meta('rw_member_tumblr', 'type=text'); 
									$member_twitter = rwmb_meta('rw_member_twitter', 'type=text'); 	
									$member_viadeo = rwmb_meta('rw_member_viadeo', 'type=text'); 
									$member_vimeo = rwmb_meta('rw_member_vimeo', 'type=text'); 
									$member_youtube = rwmb_meta('rw_member_youtube', 'type=text');
									$member_mail = rwmb_meta('rw_member_mail', 'type=text');  
								?>
                        
                        <span class="member-job text-caps"><?php echo $member_job; ?></span>
                        
                            <h4 class="member-name"><a href="<?php the_permalink (); ?>">
                                <?php the_title(); ?>
                                </a></h4>
                            <div class="mini-divider"></div>
                            
                            
                            
                                    <ul class="social clearfix">
                                        <?php if( $member_behance != ''  ) {  ?>
                                        <li><a href="<?php echo $member_behance; ?>" target="_blank" title="behance"><i class="icon-behance"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_delicious != ''  ) {  ?>
                                        <li><a href="<?php echo $member_delicious; ?>" target="_blank" title="delicious"><i class="icon-delicious"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_dribbble != ''  ) {  ?>
                                        <li><a href="<?php echo $member_behance; ?>" target="_blank" title="dribbble"><i class="icon-dribbble"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_dropbox != ''  ) {  ?>
                                        <li><a href="<?php echo $member_dropbox; ?>" target="_blank" title="dropbox"><i class="icon-dropbox"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_facebook != ''  ) {  ?>
                                        <li><a href="<?php echo $member_facebook; ?>" target="_blank" title="facebook"><i class="icon-facebook"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_flickr != ''  ) {  ?>
                                        <li><a href="<?php echo $member_flickr; ?>" target="_blank" title="flickr"><i class="icon-flickr"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_googleplus != ''  ) {  ?>
                                        <li><a href="<?php echo $member_googleplus; ?>" target="_blank" title="googleplus"><i class="icon-gplus"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_instagram != ''  ) {  ?>
                                        <li><a href="<?php echo $member_instagram; ?>" target="_blank" title="instagram"><i class="icon-instagram"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_linkedin != ''  ) {  ?>
                                        <li><a href="<?php echo $member_linkedin; ?>" target="_blank" title="linkedin"><i class="icon-linkedin"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_paypal != ''  ) {  ?>
                                        <li><a href="<?php echo $member_paypal; ?>" target="_blank" title="paypal"><i class="icon-paypal"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_pinterest != ''  ) {  ?>
                                        <li><a href="<?php echo $member_pinterest; ?>" target="_blank" title="pinterest"><i class="icon-pinterest"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_skype != ''  ) {  ?>
                                        <li><a href="<?php echo $member_skype; ?>" target="_blank" title="skype"><i class="icon-skype"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_soundcloud != ''  ) {  ?>
                                        <li><a href="<?php echo $member_soundcloud; ?>" target="_blank" title="soundcloud"><i class="icon-soundcloud"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_stumbleupon != ''  ) {  ?>
                                        <li><a href="<?php echo $member_stumbleupon; ?>" target="_blank" title="stumbleupon"><i class="icon-stumbleupon"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_tumblr != ''  ) {  ?>
                                        <li><a href="<?php echo $member_tumblr; ?>" target="_blank" title="tumblr"><i class="icon-tumblr"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_twitter != ''  ) {  ?>
                                        <li><a href="<?php echo $member_twitter; ?>" target="_blank" title="twitter"><i class="icon-twitter"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_viadeo != ''  ) {  ?>
                                        <li><a href="<?php echo $member_viadeo; ?>" target="_blank" title="viadeo"><i class="icon-viadeo"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_vimeo != ''  ) {  ?>
                                        <li><a href="<?php echo $member_vimeo; ?>" target="_blank" title="vimeo"><i class="icon-vimeo"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_youtube != ''  ) {  ?>
                                        <li><a href="<?php echo $member_youtube; ?>" target="_blank" title="youtube"><i class="icon-youtube"></i></a></li>
                                        <?php }  ?>
                                        <?php if( $member_mail != ''  ) {  ?>
                                        <li><a href="mailto:<?php echo $member_mail; ?>" target="_blank" title="mail"><i class="icon-mail"></i></a></li>
                                        <?php }  ?>
                                    </ul>
                                    
                                    
                               <div class="team-excerpt">     
                            <?php the_content(); ?>
                            </div>
                        </div>
                    </div>
                </article>
                <?php tcsn_post_nav(); ?>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- #content -->

<?php get_footer(); ?>
