// JavaScript Document for Bulwark
jQuery(document).ready(function($) {
	
	// Use strict 
	"use strict";
	
	$('[data-toggle="popover"]').popover({trigger: 'hover'});
	$('[data-toggle="tooltip"]').tooltip({trigger: 'hover'});
	
	// Owl carousel for recent posts
	$(".recentpost-carousel").owlCarousel({
		transitionStyle : false, // fade, backSlide, goDown
		navigation: false, // Show next and prev buttons
		pagination: true,
		slideSpeed: 300,
    	paginationSpeed: 400,
    	items: 3, // Increase this if need more than 2 items
 	 // itemsDesktop : false,
 	 // itemsDesktopSmall : false,
 	 // itemsTablet: false,
 	 // itemsMobile : false,
	 	theme: 'tcsn-theme',
	});
	
	// Owl carousel for portfolio
	$(".portfolio-carousel").owlCarousel({
		transitionStyle : false, // fade, backSlide, goDown
		navigation: true, // Show next and prev buttons
		navigationText : false,
		pagination: false,
	 	slideSpeed: 300,
		paginationSpeed: 400,
		items: 4, // Increase this if need more than 2 items
	 // itemsDesktop : false,
	 // itemsDesktopSmall : false,
     // itemsTablet: false,
     // itemsMobile : false,
	 	theme: 'tcsn-theme',
	});
	
	// Owl carousel for recent posts
	$(".testimonial-carousel").owlCarousel({
		transitionStyle : "fade", // fade, backSlide, goDown
		navigation: false, // Show next and prev buttons
		pagination: false,
		slideSpeed: 300,
		paginationSpeed: 400,
		singleItem:true,
		// items: 1, // Increase this if need more than 2 items
		autoPlay : 4000,
		stopOnHover: true,
		theme: 'tcsn-theme',
		autoHeight: true, // Use it only for one item per page setting.
	});

	// Feature item hover
	$('.feature-item .overlay').addClass('animated fadeIn');
	$('.feature-item').hover(function () {
		$(this).find('.overlay').addClass('animated slideInRight');
	}, function () {
		$(this).find('.overlay').removeClass('slideInRight');
	});
	
	
	
	$('.folio-thumb').hover(function () {
		$(this).find(".zoom-button, .link-button").addClass('animated bounceInDown button-delay');
	}, function () {
		$(this).find(".zoom-button, .link-button").removeClass('bounceInDown button-delay');
	});
	
	// Feature item hover
	$('.folio-thumb .overlay').addClass('animated fadeIn');
	$('.folio-thumb').hover(function () {
		$(this).find('.overlay').addClass('animated slideInDown');
	}, function () {
		$(this).find('.overlay').removeClass('slideInDown');
	});

	// Fitvids
	$(".video-wrapper").fitVids();
	
	// Isotope - Portfolio
	var $container = $('.filter-content');
	$container.imagesLoaded(function () {
		$container.isotope({
			itemSelector: '.item',
			layoutMode : 'fitRows',
		    //  masonry: {}
		});
	});
	$('.filter_nav li a').click(function () {
		$('.filter_nav li a').removeClass('active');
		$(this).addClass('active');
		var selector = $(this).attr('data-filter');
		$container.isotope({
			filter: selector
		});
		return false;
	});
	
	// Isotope - Search
	var $container_search = $('.mssearch-content');
	$container_search.imagesLoaded(function () {
		$container_search.isotope({
			itemSelector: '.mssearch-item',
			// layoutMode : 'fitRows',
		     masonry: {}
		});
	});
	
	// Isotope - CPT Archive
	var $container_archive = $('.msarchive-content');
	$container_archive.imagesLoaded(function () {
		$container_archive.isotope({
			itemSelector: '.msarchive-item',
			// layoutMode : 'fitRows',
		     masonry: {}
		});
	});
	
	// Dropdown nav for responsive
	selectnav('nav', {
		label: '',
		nested: true,
		indent: '-'
	});
			
	//video z-index
	$("iframe").each(function(){
			var ifr_source = $(this).attr('src');
			var wmode = "wmode=transparent";
			if(ifr_source.indexOf('?') != -1) {
				var getQString = ifr_source.split('?');
				var oldString = getQString[1];
				var newString = getQString[0];
				$(this).attr('src',newString+'?'+wmode+'&'+oldString);
			}
			else $(this).attr('src',ifr_source+'?'+wmode);
	});

	//prettyPhoto
	$('a[data-rel]').each(function () {
		$(this).attr('rel', $(this).data('rel'));
	});
	$("a[rel^='prettyPhoto'],a[rel^='prettyPhoto[gallery]']").prettyPhoto({
		animation_speed: 'fast',
		slideshow: 5000,
		autoplay_slideshow: false,
		opacity: 0.80,
		show_title: true,
		theme: 'pp_default',
		/* light_rounded / dark_rounded / light_square / dark_square / facebook */
		overlay_gallery: false,
		social_tools: false,
		changepicturecallback: function () {
			var $pp = $('.pp_default');
			if (parseInt($pp.css('left')) < 0) {
				$pp.css('left', 0);
			}
		}
	});
	
}); // Close document ready