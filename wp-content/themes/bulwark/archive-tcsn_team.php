<?php
/**
 * The template for displaying team archive pages.
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php get_header(); ?>
<?php global $smof_data; ?>

<section id="page-header" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php if( $smof_data['tcsn_team_archive_title'] != "" ) { ?>
                <h3 class="page-title"> <?php echo $smof_data['tcsn_team_archive_title']; ?> </h3>
                <?php } ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php get_template_part( 'includes/templates/headers/breadcrumb' ); ?>
            </div>
        </div>
    </div>
</section>
<!-- #page-header -->

<section id="content-main">
    <div class="container">
        <div class="row">
            <?php if ( have_posts() ) : ?>
            <div class="msarchive-content">
                <?php while ( have_posts() ) : the_post(); ?>
                <div class="col-md-4 col-sm-4 col-xs-12 msarchive-item">
                    <div class="archive-inner">
                        <?php if ( has_post_thumbnail() ) { ?>
                        <div class="archive-thumb"> <?php echo '<a href="' . get_permalink() . '">' . get_the_post_thumbnail( $post->ID, 'full', array( 'title' => '' ) ) . '</a>';  ?> </div>
                        <?php } ?>
                        <h6 class="archive-entry-title"><a href="<?php the_permalink (); ?>">
                            <?php the_title(); ?>
                            </a></h6>
                        <?php if ( has_excerpt() ) { ?>
                        <div class="archive-excerpt">
                            <?php the_excerpt(); ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php endwhile; ?>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php tcsn_paging_nav(); ?>
                <?php else : ?>
                <?php get_template_part( '/includes/templates/post-formats/content', 'none' ); ?>
            </div>
            <?php endif; ?>
        </div>
    </div>
</section>
<!-- #content -->

<?php get_footer(); ?>
