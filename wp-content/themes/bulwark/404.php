<?php
/**
 * The template for displaying 404 error page
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php get_header(); ?>
<?php global $smof_data; ?>

<section id="content-main" class="clearfix">
  <div class="container">
    <div class="row">
      <div class="error-404 clearfix">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <h1 class="heading-404">
            <?php _e( '404', 'tcsn_theme' ); ?>
          </h1>
        </div>
        <div class="col-md-6 col-md-offset-3">
          <p>
            <?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Try with search.', 'tcsn_theme' ); ?>
          </p>
          <?php get_search_form(); ?>
        </div>
      </div>
    </div>
  </div>
</section>
<!-- #content -->

<?php get_footer(); ?>
