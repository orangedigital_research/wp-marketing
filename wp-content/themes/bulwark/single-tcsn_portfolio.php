<?php
/**
 * The template for displaying portfolio details.
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php get_header(); ?>
<?php global $smof_data; ?>

<section id="page-header" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <h3 class="page-title"><?php echo the_title(); ?></h3>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php get_template_part( 'includes/templates/headers/breadcrumb' ); ?>
            </div>
        </div>
    </div>
</section>
<!-- #page-header -->

<section id="content-main" class="pad-bottom-none clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php if( $smof_data['tcsn_portfolio_predefined_content'] == 1 ) { ?>
                    <div class="row">
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="post-thumb">
                                <?php $portfolio_type = rwmb_meta('rw_portfolio_type', 'type=select');
									  $pf_video_audio_embed = rwmb_meta('rw_pf_video_audio_embed', 'type=textarea');
					 			?>
                                <?php 
					 	switch ($portfolio_type) {
							case 'Image': ?>
                                <div class="folio-thumb">
                                    <?php the_post_thumbnail(); ?>
                                </div>
                                <?php break; // end image ?>
                                <?php case 'Video': ?>
                                <div class="video-wrapper"> <?php echo $pf_video_audio_embed; ?> </div>
                                <?php break; // end video ?>
                                <?php case 'Audio': ?>
                                <div class="audio-wrapper"> <?php echo $pf_video_audio_embed; ?> </div>
                                <?php break; // end audio ?>
                                <?php case 'Gallery': ?>
                                <div class="gallery-wrapper">
                                    <?php get_template_part( 'includes/templates/sliders/slider-portfolio' ); ?>
                                </div>
                                <?php break; // end gallery ?>
                                <?php default: ?>
                                <div class="folio-thumb">
                                    <?php the_post_thumbnail(); ?>
                                </div>
                                <?php } //end switch ?>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <div class="portfolio-details">
                                <?php $pf_details = rwmb_meta('rw_pf_details', 'type=textarea'); ?>
                                <?php echo $pf_details; ?> </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="entry-content">
                        <?php the_content(); ?>
                    </div>
                </article>
                <?php tcsn_post_nav(); ?>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- #content -->

<?php get_footer(); ?>
