<?php
/**
 * Template Name: Portfolio - 3 Column
 *
 * The Template for displaying 3 column portfolio
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
 ?>
<?php get_header(); ?>
<?php global $smof_data; ?>

<section id="page-header" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <h3 class="page-title"><?php echo the_title(); ?></h3>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php get_template_part( 'includes/templates/headers/breadcrumb' ); ?>
            </div>
        </div>
    </div>
</section>
<!-- #page-header -->

<section id="content-main" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php if( $smof_data['tcsn_portfolio_filter'] == 1 ) { ?>
                <?php
						$terms = get_terms("tcsn_portfoliotags");
						$count = count($terms);
						echo '<ul class="filter_nav clearfix">';
						echo '<li><a class="active" href="#" data-filter="*" title="All" data-toggle="tooltip" data-placement="top"><i class="icon-menu"></i></a></li>';
							if ($count > 0) {
    							foreach ($terms as $term) {
        							$termname = strtolower($term->name);
        							$termname = str_replace(' ', '-', $termname);
        							echo '<li><a href="#" title="" data-filter=".' . $termname . '">' . $term->name . '</a></li>';
    							}
							}
						echo "</ul>"; ?>
                <?php } ?>
            </div>
            <!-- portfolio filter ends-->
            <div class="clearfix"></div>
            <?php 
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			$loop = new WP_Query( array(
				'post_type'      => 'tcsn_portfolio',
				'posts_per_page' => ( $smof_data['tcsn_portfolio_items_per_page'] ?  $smof_data['tcsn_portfolio_items_per_page'] : 9),
				'paged'          => $paged,
				'order'          => ( $smof_data['tcsn_portfolio_arrange'] ?  $smof_data['tcsn_portfolio_arrange'] : 'DESC'),   // DESC or ASC 
				'orderby'        => ( $smof_data['tcsn_portfolio_sort'] ?  $smof_data['tcsn_portfolio_sort'] : 'date'),   // date, rand or title
			
				) );
			?>
            <div id="items" class="filter-content">
                <?php if ($loop): while ($loop->have_posts()): $loop->the_post(); ?>
                <?php
					$terms = get_the_terms($post->ID, 'tcsn_portfoliotags');
					if ($terms && !is_wp_error($terms)):
						$links = array();
					foreach ($terms as $term) {
						$links[] = $term->name;
					}
					$links = str_replace(' ', '-', $links);
					$tax   = join(" ", $links);
					else:
					$tax = ''; ?>
                <?php endif; ?>
                <div class="col-md-4 col-sm-4 col-xs-12 item <?php echo strtolower($tax); ?> all">
                    <?php 
	 					$folio_img = get_post_thumbnail_id(); 
						$folio_img_url = wp_get_attachment_url( $folio_img,'full' ); 
						$folio_thumb_cropped = aq_resize( $folio_img_url, 360, 240, true ); 
						$folio_thumb = aq_resize( $folio_img_url, 360, 240, false ); 
						$lightbox_img_url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'large'); 
						$portfolio_type = rwmb_meta('rw_portfolio_type', 'type=select');
						$video_url = rwmb_meta('rw_video_url', 'type=text');
						$zoom_title = rwmb_meta('rw_zoom_title', 'type=text'); 
						$portfolio_client = rwmb_meta('rw_portfolio_client', 'type=text');
						$external_link= rwmb_meta('rw_external_link', 'type=checkbox'); 
						$link_url = rwmb_meta('rw_link_url', 'type=text');
						?>
                    <?php 
					 	switch ($portfolio_type) {
						case 'Image': ?>
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="folio-thumb">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php get_template_part( 'includes/templates/post-formats/portfolio-item' ); ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php break; // end image ?>
                    <?php case 'Video': ?>
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="folio-thumb">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php if ( $smof_data['tcsn_portfolio_hover'] == 'zoom' ) { ?>
                            <div class='overlay'></div>
                            <?php the_post_thumbnail(); ?>
                            <a href="<?php echo $video_url; ?>" data-rel="prettyPhoto" title="<?php echo $zoom_title; ?>" class="zoom-button"></a>
                            <?php  } ?>
                            <?php if ( $smof_data['tcsn_portfolio_hover'] == 'link' ) { ?>
                            <?php if ( $external_link == 1 ) { ?>
                            <div class='overlay'></div>
                            <?php the_post_thumbnail(); ?>
                            <a href="<?php echo $link_url; ?>" class="link-button"></a>
                            <?php } else { ?>
                            <div class='overlay'></div>
                            <?php the_post_thumbnail(); ?>
                            <a href="<?php the_permalink(); ?>" class="link-button"></a>
                            <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php break; // end video ?>
                    <?php case 'Audio': ?>
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="folio-thumb">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php get_template_part( 'includes/templates/post-formats/portfolio-item' ); ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php break; // end audio ?>
                    <?php case 'Gallery': ?>
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="folio-thumb">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php get_template_part( 'includes/templates/post-formats/portfolio-item' ); ?>
                        </div>
                    </div>
                    <?php endif; ?>
                    <?php break; // end gallery ?>
                    <?php default: ?>
                    <?php if ( has_post_thumbnail() ) : ?>
                    <div class="folio-thumb">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <?php get_template_part( 'includes/templates/post-formats/portfolio-item' ); ?>
                        </div>
                    </div>
                    <?php  endif; ?>
                    <?php } //end switch ?>
                    <div class="clearfix"></div>
                    <?php if( $smof_data['tcsn_portfolio_heading'] == 1 ) { ?>
                    <h5 class="folio-title"><a href="<?php the_permalink(); ?>" target="_blank">
                        <?php the_title(); ?>
                        </a></h5>
                    <?php } ?>
                    <?php if( $smof_data['tcsn_portfolio_client'] == 1 ) { ?>
                    <div class="mini-divider"></div>
                    <span class="folio-client text-caps"><?php echo $portfolio_client; ?></span>
                    <?php } ?>
                    <?php if( $smof_data['tcsn_portfolio_excerpt'] == 1 ) { ?>
                    <div class="folio-excerpt clearfix">
                        <?php the_excerpt(); ?>
                    </div>
                    <?php } ?>
                    <div class="clearfix"></div>
                </div>
                <?php endwhile;?>
                <?php endif; ?>
                <?php wp_reset_postdata(); ?>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php tcsn_pagination($loop->max_num_pages, $range = 2); ?>
            </div>
        </div>
    </div>
</section>
<!-- #content -->

<?php get_footer(); ?>
