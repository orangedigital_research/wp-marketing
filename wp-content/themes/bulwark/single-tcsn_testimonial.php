<?php
/**
 * The template for displaying tesimonial details.
 *
 * @package WordPress
 * @subpackage Bulwark
 * @since Bulwark 1.0
 */
?>
<?php get_header(); ?>
<?php global $smof_data; ?>

<section id="page-header" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php if( $smof_data['tcsn_testimonial_title'] != "" ) { ?>
                <h1 class="page-title"><?php echo $smof_data['tcsn_testimonial_title']; ?></h1>
                <?php } ?>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <?php get_template_part( 'includes/templates/headers/breadcrumb' ); ?>
            </div>
        </div>
    </div>
</section>
<!-- #page-header -->

<section id="content-main" class="clearfix">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 testimonial-single">
                            <?php  $client_info = rwmb_meta('rw_client_info', 'type=text');  ?>
                            <span class="testimonial-icon-subheading"><?php echo $client_info; ?></span>
                            <h4><a href="<?php the_permalink (); ?>">
                                <?php the_title(); ?>
                                </a></h4>
                            <div class="mini-divider"></div>
                            <?php the_content(); ?>
                        </div>
                    </div>
                </article>
                <?php tcsn_post_nav(); ?>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
<!-- #content -->

<?php get_footer(); ?>
