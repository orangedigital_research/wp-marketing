<?php

add_action('init','of_options');

if (!function_exists('of_options'))
{
	function of_options()
	{
		//Access the WordPress Categories via an Array
		$of_categories 		= array();  
		$of_categories_obj 	= get_categories('hide_empty=0');
		foreach ($of_categories_obj as $of_cat) {
		    $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;}
		$categories_tmp 	= array_unshift($of_categories, "Select a category:");    
	       
		//Access the WordPress Pages via an Array
		$of_pages 			= array();
		$of_pages_obj 		= get_pages('sort_column=post_parent,menu_order');    
		foreach ($of_pages_obj as $of_page) {
		    $of_pages[$of_page->ID] = $of_page->post_name; }
		$of_pages_tmp 		= array_unshift($of_pages, "Select a page:");       
	
		//Testing 
		$of_options_select 	= array("one","two","three","four","five"); 
		$of_options_radio 	= array("one" => "One","two" => "Two","three" => "Three","four" => "Four","five" => "Five");
		
		//Sample Homepage blocks for the layout manager (sorter)
		$of_options_homepage_blocks = array
		( 
			"disabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_one"		=> "Block One",
				"block_two"		=> "Block Two",
				"block_three"	=> "Block Three",
			), 
			"enabled" => array (
				"placebo" 		=> "placebo", //REQUIRED!
				"block_four"	=> "Block Four",
			),
		);


		//Stylesheets Reader
		$alt_stylesheet_path = LAYOUT_PATH;
		$alt_stylesheets = array();
		
		if ( is_dir($alt_stylesheet_path) ) 
		{
		    if ($alt_stylesheet_dir = opendir($alt_stylesheet_path) ) 
		    { 
		        while ( ($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false ) 
		        {
		            if(stristr($alt_stylesheet_file, ".css") !== false)
		            {
		                $alt_stylesheets[] = $alt_stylesheet_file;
		            }
		        }    
		    }
		}
		
		//Background Images Reader for header and footer
		$bg_images_path = get_template_directory() . '/img/patterns/'; // change this to where you store your bg images
		$bg_images_url = get_template_directory_uri() .'/img/patterns/'; // change this to where you store your bg images
		$bg_images = array();
		
		if ( is_dir($bg_images_path) ) {
		    if ($bg_images_dir = opendir($bg_images_path) ) { 
		        while ( ($bg_images_file = readdir($bg_images_dir)) !== false ) {
		            if(stristr($bg_images_file, ".png") !== false || stristr($bg_images_file, ".jpg") !== false) {
		            	natsort($bg_images); //Sorts the array into a natural order
		                $bg_images[] = $bg_images_url . $bg_images_file;
		            }
		        }    
		    }
		}
		
		/*-----------------------------------------------------------------------------------*/
		/* TO DO: Add options/functions that use these */
		/*-----------------------------------------------------------------------------------*/
		
		//More Options
		$uploads_arr 		= wp_upload_dir();
		$all_uploads_path 	= $uploads_arr['path'];
		$all_uploads 		= get_option('of_uploads');
		$other_entries 		= array("Select a number:","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19");
		$body_repeat 		= array("no-repeat","repeat-x","repeat-y","repeat");
		$body_pos 			= array("top left","top center","top right","center left","center center","center right","bottom left","bottom center","bottom right");
		
		// Image Alignment radio box
		$of_options_thumb_align = array("alignleft" => "Left","alignright" => "Right","aligncenter" => "Center"); 
		
		// Image Links to Options
		$of_options_image_link_to = array("image" => "The Image","post" => "The Post"); 


/*-----------------------------------------------------------------------------------*/
/* The Options Array */
/*-----------------------------------------------------------------------------------*/

// Prefix
$prefix = "tcsn_";

// Set the Options Array
global $of_options;
$of_options = array();

/**
 * General Options
 *
 */
$of_options[] = array( 
	"name" => "General",
	"type" => "heading",
	);
				
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Favicon / Touch Icons",
	"type" => "info",
	);

$of_options[] = array( 	
	"name" => "Favicon",
	"desc" => __( "Upload Favicon ( 16px x 16px ico/png/jpg ).", "tcsn_theme" ),
	"id"   => $prefix . "favicon",
	"std"  => "",
	"type" => "upload",
	);

$of_options[] = array( 
	"name" => "Standard iPhone Touch Icon",
	"desc" => __( "Upload Icon ( 57px x 57px png ).", "tcsn_theme" ),
	"id"   => $prefix . "favicon_iphone",
	"std"  => "",
	"type" => "upload",
	);

$of_options[] = array( 
	"name" => "Retina iPhone Touch Icon",
	"desc" => __( "Upload Icon ( 114px x 114px png ).", "tcsn_theme" ),
	"id"   => $prefix . "favicon_iphone_retina",
	"std"  => "",
	"type" => "upload",
	);
	
$of_options[] = array( 
	"name" => "Standard iPad Touch Icon",
	"desc" => __( "Upload Icon ( 72px x 72px png ).", "tcsn_theme" ),
	"id"   => $prefix . "favicon_ipad",
	"std"  => "",
	"type" => "upload",
	);

$of_options[] = array( 
	"name" => "Retina iPad Touch Icon",
	"desc" => __( "Upload Icon ( 144px x 144px png ).", "tcsn_theme" ),
	"id"   => $prefix . "favicon_ipad_retina",
	"std"  => "",
	"type" => "upload",
	);

$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Responsive",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "",
	"desc" => __( "Check to enable responsiveness.", "tcsn_theme" ),
	"id"   => $prefix . "layout_responsive",
	"std"  => 1,
	"type" => "checkbox",
	); 

$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Tracking Code",
	"type" => "info",
	);

$of_options[] = array( 
	"name" => "",
	"desc" => __( "Paste Google Analytics (or other) tracking code here. This will be added into the <strong>header</strong> of theme.", "tcsn_theme" ),
	"id" => $prefix . "header_tracking",
	"std" => "",
	"type" => "textarea"
	); 
	
$of_options[] = array( 
	"name" => "",
	"desc" => __( "Paste Google Analytics (or other) tracking code here. This will be added into the <strong>footer</strong> of theme.", "tcsn_theme" ),
	"id" => $prefix . "footer_tracking",
	"std" => "",
	"type" => "textarea"
	); 

$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Comments on Pages (other than posts)",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "",
	"desc" => __( "Check to enable comments on pages.", "tcsn_theme" ),
	"id"   => $prefix . "page_comments",
	"std"  => 0,
	"type" => "checkbox",
	); 

/**
 * Typography and Styling options
 *
 */
$of_options[] = array( 
	"name" => "Typography and Styling",
	"type" => "heading",
	);

// Color scheme
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Predefined Color Schemes",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name"    => "",
	"desc" => __( "Select color skin.", "tcsn_theme" ),
	"id"      => $prefix . "color_scheme",
	"std"     => "orange",
	"type"    => "select",
	"options" => array( 
		'turquoise' => 'Turquoise', 
		'green'     => 'Avocado Green', 
		'red'       => 'Auburn Red', 
		'blue'      => 'Bondi Blue', 
		'grey'      => 'Grey ', 	
		'golden'    => 'Dark goldenrod',
		'violet'    => 'Medium Violet Red',  
		'royal'     => 'Royal Blue',  
		),
	);

// Body typography
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Body Typography",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "",
	"desc" => __( "Set body text. <br/>  ( Defaults: 14px, Open Sans, Normal, #999999 )", "tcsn_theme" ),
	"id"   => $prefix . "font_body",
	"std"  => array( 
		'size'  => '14px', 
		'face'  => 'Open Sans', 
		'style' => 'Normal', 
		'color' => '#999999',
		),
	"type" => "typography",
	);

$of_options[] = array( 
	"name" => __( "Letter Spacing", "tcsn_theme" ),
	"desc" => __( "Remove letter spacing", "tcsn_theme" ),
	"id"   => $prefix . "letter_spacing_body",
	"std"  => 0,
	"type" => "checkbox",
	); 
	
// Headings typography
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Headings",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "H1",
	"desc" => __( "Set H1. <br/>  ( Defaults: 48px, Open Sans, Normal, #383838 )", "tcsn_theme" ),
	"id"   => $prefix . "font_h1",
	"std"  => array( 
		'size'  => '48px', 
		'face'  => 'Open Sans', 
		'style' => 'Normal', 
		'color' => '#383838',
		),
	"type" => "typography",
	);

$of_options[] = array( 
	"name" => "H2",
	"desc" => __( "Set H2. <br/>  ( Defaults: 36px, Open Sans, Normal, #383838 )", "tcsn_theme" ),
	"id"   => $prefix . "font_h2",
	"std"  => array( 
		'size'  => '36px', 
		'face'  => 'Open Sans', 
		'style' => 'Normal', 
		'color' => '#383838',
		),
	"type" => "typography",
	);
	
$of_options[] = array( 
	"name" => "H3",
	"desc" => __( "Set H3. <br/>  ( Defaults: 30px, Open Sans, Normal, #383838 )", "tcsn_theme" ),
	"id"   => $prefix . "font_h3",
	"std"  => array( 
	'size'  => '30px', 
		'face'  => 'Open Sans', 
		'style' => 'Normal', 
		'color' => '#383838',
		),
	"type" => "typography",
);
	
$of_options[] = array( 
	"name" => "H4",
	"desc" => __( "Set H4. <br/>  ( Defaults: 24px, Open Sans, Normal, #383838 )", "tcsn_theme" ),
	"id"   => $prefix . "font_h4",
	"std"  => array( 
		'size'  => '24px', 
		'face'  => 'Open Sans', 
		'style' => 'Normal', 
		'color' => '#383838',
		),
	"type" => "typography",
);
	
$of_options[] = array( 
	"name" => "H5",
	"desc" => __( "Set H5. <br/>  ( Defaults: 18px, Open Sans, Normal, #383838 )", "tcsn_theme" ),
	"id"   => $prefix . "font_h5",
	"std"  => array( 
		'size'  => '18px', 
		'face'  => 'Open Sans', 
		'style' => 'Normal', 
		'color' => '#383838',
		),
	"type" => "typography",
);
	
$of_options[] = array( 
	"name" => "H6",
	"desc" => __( "Set H6. <br/>  ( Defaults: 14px, Open Sans, Normal, #383838 )", "tcsn_theme" ),
	"id"   => $prefix . "font_h6",
	"std"  => array( 
		'size'  => '14px', 
		'face'  => 'Open Sans', 
		'style' => 'Normal', 
		'color' => '#383838',
		),
	"type" => "typography",
);

$of_options[] = array( 
	"name" => __( "Letter Spacing", "tcsn_theme" ),
	"desc" => __( "Remove heading letter spacing", "tcsn_theme" ),
	"id"   => $prefix . "letter_spacing_heading",
	"std"  => 0,
	"type" => "checkbox",
	); 

// Menu typography
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Menu",
	"type" => "info",
	);

$of_options[] = array( 
	"name" => "",
	"desc" => __( "(Defaults: 14px, Open Sans, Normal, #383838 )", "tcsn_theme" ),
	"id"   => $prefix . "font_menu",
	"std"  => array( 
		'size'  => '14px', 
		'face'  => 'Open Sans', 
		'style' => 'Normal', 
		'color' => '#383838',
		),
	"type" => "typography",
	);

$of_options[] = array( 
	"name" => "Hover Link Color",
	"desc" => "",
	"id"   => $prefix . "menu_link_hover_color",
	"std"  => "#999999",
	"type" => "color",
	);

$of_options[] = array( 
	"name" => "Hover / Active Link Background Color",
	"desc" => "",
	"id"   => $prefix . "menu_link_hover_bg_color",
	"std"  => "#fff",
	"type" => "color",
	);

$of_options[] = array( 
	"name" => "Hover / Active Link Border Color",
	"desc" => "",
	"id"   => $prefix . "menu_link_hover_border_color",
	"std"  => "#e0e0e0",
	"type" => "color",
	);

// Page Header typography
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Page Header Typography",
	"type" => "info",
	);

$of_options[] = array( 
	"name" => "Page Title Color",
	"desc" => __( "Set page title color.", "tcsn_theme" ),
	"id"   =>  $prefix . "color_page_title",
	"std"  => "#383838",
	"type" => "color",
	);

$of_options[] = array( 
	"name" => "Breadcrumb",
	"desc" => __( "Set breadcrumb text. <br/> ( Defaults:12px,#999999 )", "tcsn_theme" ),
	"id"   =>  $prefix . "font_bcum",
	"std"  => array(
		'size'  => '12px',
		'color' => '#999999',
		),
	"type" => "typography",
	);

// Footer typography
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Footer Typography",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "",
	"desc" => __( "Set footer text. <br/> ( Defaults : 14px, Open Sans, Normal, #999999 )", "tcsn_theme" ),
	"id"   => $prefix . "font_footer",
	"std"  => array( 
		'size'  => '14px', 
		'face'  => 'Open Sans', 
		'style' => 'Normal', 
		'color' => '#999999', 
		),
	"type" => "typography",
	);

$of_options[] = array( 	
	"name" => "Headings Color",
	"desc" => __( "Color of headings in footer (h1 to h6).", "tcsn_theme" ),
	"id"   => $prefix . "color_footer_headings",
	"std"  => "#383838",
	"type" => "color",
	);

// Widgets typography
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Widgets Typography",
	"type" => "info",
	);

$of_options[] = array( 	
	"name" => "Link Color",
	"desc" => __( "Color of links in widgets", "tcsn_theme" ),
	"id"   => $prefix . "color_widget_link",
	"std"  => "#999",
	"type" => "color",
	);

$of_options[] = array( 	
	"name" => "Link Hover Color",
	"desc" => __( "Color of hover to links in widgets", "tcsn_theme" ),
	"id"   => $prefix . "color_widget_link_hover",
	"std"  => "#379f7a",
	"type" => "color",
	);

$of_options[] = array( 	
	"name" => "Border Color",
	"desc" => __( "Color of borders in widgets", "tcsn_theme" ),
	"id"   => $prefix . "color_widget_border",
	"std"  => "#e0e0e0",
	"type" => "color",
	);

$of_options[] = array( 	
	"name" => "Social Icon Color",
	"desc" => __( "Color of social icons of social network widget", "tcsn_theme" ),
	"id"   => $prefix . "color_widget_social",
	"std"  => "#999999",
	"type" => "color",
	);
	
// General typography
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Others : Theme Defaults",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "Link Color",
	"desc" => __( "Set link color.", "tcsn_theme" ),
	"id"   =>  $prefix . "link_color",
	"std"  => "#379f7a",
	"type" => "color",
	);

$of_options[] = array( 
	"name" => "Link Hover Color",
	"desc" => __( "Set link hover color. Generally same as color of body text.", "tcsn_theme" ),
	"id"   =>  $prefix . "link_hover_color",
	"std"  => "#999999",
	"type" => "color",
	);

$of_options[] = array( 
	"name" => "Theme Base Color",
	"desc" => "Theme base color, will work for colored text, blockquote, tooltip etc.",
	"id"   => $prefix . "text_color",
	"std"  => "#379f7a",
	"type" => "color",
	);

/**
 * Body Options
 *

 */
$of_options[] = array( 
	"name" => "Body",
	"type" => "heading",
	);

$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Body Background",
	"type" => "info",
	);
					
$of_options[] = array( 
	"name" => "Background Color",
	"desc" => "",
	"id"   => $prefix . "bg_color_body",
	"std"  => "#fff",
	"type" => "color",
	);

$of_options[] = array( 
	"name" => "Background Image",
	"desc" => __( "Check to enable background pattern.", "tcsn_theme" ),
	"id"   => $prefix . "show_pattern_body",
	"std"  => 0,
	"type" => "checkbox",
	); 
		
$of_options[] = array( 	
	"name"    => "",
	"desc"    => __( "Select a background pattern. <br><br> To add more patterns, just upload patterns in : <br>  Theme folder > img > patterns", "tcsn_theme" ),
	"id"      => $prefix . "pattern_body",
	"std" 	  => "",
	"type" 	  => "tiles",
	"options" => $bg_images,
	);
						
$of_options[] = array( 
	"name"    => "Background Repeat",
	"desc"    => "",
	"id"      => $prefix . "pattern_repeat_body",
	"std"     => "repeat",
	"type"    => "select",
	"options" => array(
		'repeat'    => 'repeat', 
		'repeat-x'  => 'repeat-x', 
		'repeat-y'  => 'repeat-y', 
		'no-repeat' => 'no-repeat',
		),
	);  
					
$of_options[] = array( 
	"name"    => "Background Attachment",			
	"desc"    => "",
	"id"      => $prefix . "attachment_body",
	"std"     => "scroll",
	"type"    => "select",
	"options" => array( 
		'scroll' => 'scroll', 
		'fixed'  => 'fixed', 
		),
	);  

/**
 * Header Options
 *
 */
$of_options[] = array( 
	"name" => "Header",
	"type" => "heading",
	);

// Header
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Select a Header Layout",
	"type" => "info",
	);

$of_options[] = array( 
	"name"    => "",
	"desc"    => "",
	"id"      => $prefix. "layout_header",
	"std"     => "",
	"type"    => "images",
	"options" => array(
		"v1" => get_template_directory_uri() . "/includes/img/header1.jpg",
		"v2" => get_template_directory_uri() . "/includes/img/header2.jpg",
		),
	);

// Header
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Header Background",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "Background Color",
	"desc" => "",
	"id"   => $prefix . "bg_color_header",
	"std"  => "#fff",
	"type" => "color",
	);

$of_options[] = array( 
	"name" => "Background Image",
	"desc" => __( "Check to enable background pattern.", "tcsn_theme" ),
	"id"   => $prefix . "show_pattern_header",
	"std"  => 0,
	"type" => "checkbox",
	); 
		
$of_options[] = array( 	
	"name"    => "",
	"desc"    => __( "Select a background pattern. <br><br> To add more patterns, just upload patterns in : <br>  Theme folder > img > patterns", "tcsn_theme" ),
	"id"      => $prefix . "pattern_header",
	"std" 	  => "",
	"type" 	  => "tiles",
	"options" => $bg_images,
	);
	
$of_options[] = array( 
	"name"    => "Background Repeat",
	"desc"    => "",
	"id"      => $prefix . "pattern_repeat_header",
	"std"     => "repeat",
	"type"    => "select",
	"options" => array( 
		'repeat'    => 'repeat', 
		'repeat-x'  => 'repeat-x', 
		'repeat-y'  => 'repeat-y', 
		'no-repeat' => 'no-repeat', 
		),
	); 	

// Header Padding
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Padding to header",
	"type" => "info",
	);

$of_options[] = array( 
	"name" => "",
	"desc" => __( "No need of unit. It will be in px. <br>Enter padding here like : 30<br>Default is 60.", "tcsn_theme" ),
	"id"   => $prefix . "padding_header",
	"std"  => "",
	"type" => "text",
	); 
	
// Logo
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Logo",
	"type" => "info",
	);
		
$of_options[] = array( 
	"name"    => "Select Logo Type",
	"desc"    => __( "If Image Logo : Upload logo image using field below. <br/><br/> If text logo : Provide text using field below.", "tcsn_theme" ),
	"id"      => $prefix . "logo_type",
	"std"     => $prefix . "show_text_logo",
	"type"    => "images",
	"options" => array(
		$prefix . "show_image_logo" => get_template_directory_uri() . "/includes/img/image-logo.png",
		$prefix . "show_text_logo"  => get_template_directory_uri() . "/includes/img/text-logo.png",
		),
	);
	
$of_options[] = array( 
	"name" => "For Standard Image Logo",
	"desc" => __( "Upload logo.", "tcsn_theme" ),
	"id"   => $prefix . "image_standard_logo",
	"std"  => "",
	"type" => "upload",
	);

$of_options[] = array( 
	"name" => "For Text Logo",
	"desc" => __( "Enter text for logo and set font.", "tcsn_theme" ),
	"id"   => $prefix . "text_logo",
	"std"  => "Mylogo",
	"type" => "text",
	);  

$of_options[] = array( 
	"name" => "",
	"desc" => __( "Set font. ( Defaults : 32px, Arial, normal, #999999 )", "tcsn_theme" ),
	"id"   => $prefix . "font_logo",
	"std"  => array( 
		'size'  => '32px', 
		'face'  => 'Arial', 
		'style' => 'normal', 
		'color' => '#999999',
		),
	"type" => "typography",
	);

$of_options[] = array( 
	"name" => "",
	"desc" => __( "<<----- Logo hover color. If text logo, it will change color on hover. <br/> Keep it same as logo text color or change as per liking.", "tcsn_theme" ),
	"id"   => $prefix . "hover_logo",
	"std"  => "#999999",
	"type" => "color",
	);

$of_options[] = array( 
	"name" => "Margin Top to logo",
	"desc" => __( "No need of unit. It will be in px.", "tcsn_theme" ),
	"id"   => $prefix . "logo_margin_top",
	"std"  => "10",
	"type" => "text",
	);  

// Retina Logo
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "subinfo_heading",
	"std"  => "Retina Logo (Optional)",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "For Retina Image Logo",
	"desc" => __( "Upload logo.", "tcsn_theme" ),
	"id"   => $prefix . "image_retina_logo",
	"std"  => "",
	"type" => "upload",
	);

$of_options[] = array( 
	"name" => "Logo Width",
	"desc" => __( "Enter width of logo for retina device. It should be the width of normal / standard logo, not the retina logo.<br> No need of unit. It will be in px.", "tcsn_theme" ),
	"id"   => $prefix . "logo_width",
	"std"  => "",
	"type" => "text",
	);  

$of_options[] = array( 
	"name" => "Logo Height",
	"desc" => __( "Enter height of logo for retina device. It should be the height of normal / standard logo, not the retina logo.<br> No need of unit. It will be in px.", "tcsn_theme" ),
	"id"   => $prefix . "logo_height",
	"std"  => "",
	"type" => "text",
	);  

// Search
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Search",
	"type" => "info",
	);
			
$of_options[] = array( 
	"name" => "",
	"desc" => __( "Check to enable search in header.", "tcsn_theme" ),
	"id"   => $prefix . "show_search",
	"std"  => 1,
	"type" => "checkbox",
	);

$of_options[] = array( 
	"name" => "Margin Top to search",
	"desc" => __( "No need of unit. It will be in px.", "tcsn_theme" ),
	"id"   => $prefix . "search_margin_top",
	"std"  => "10",
	"type" => "text",
	);  

$of_options[] = array( 
	"name" => "Search for small devices",
	"desc" => __( "Check to enable search in header (for small devices).", "tcsn_theme" ),
	"id"   => $prefix . "show_mobile_search",
	"std"  => 0,
	"type" => "checkbox",
	);

// Menu
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Menu",
	"type" => "info",
	);

$of_options[] = array( 
	"name" => "Margin Top to Menu",
	"desc" => __( "No need of unit. It will be in px.", "tcsn_theme" ),
	"id"   => $prefix . "menu_margin_top",
	"std"  => "",
	"type" => "text",
	);  

/**
 * Page Header Options
 *
 */
$of_options[] = array( 
	"name" => "Page Header",
	"type" => "heading",
	);

$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Page Header Background",
	"type" => "info",
	);
					
$of_options[] = array( 
	"name" => "Background Color",
	"desc" => __( "Page header background color.", "tcsn_theme" ),
	"id"   => $prefix . "bg_color_page_header",
	"std"  => "#efefef",
	"type" => "color",
	);

$of_options[] = array( 
	"name" => "Background Image",
	"desc" => __( "Check to enable background pattern.", "tcsn_theme" ),
	"id"   => $prefix . "show_pattern_page_header",
	"std"  => 0,
	"type" => "checkbox",
	); 
		
$of_options[] = array( 	
	"name"    => "",
	"desc"    => __( "Select a background pattern. <br><br> To add more patterns, just upload patterns in : <br>  Theme folder > img > patterns", "tcsn_theme" ),
	"id"      => $prefix . "pattern_page_header",
	"std" 	  => "",
	"type" 	  => "tiles",
	"options" => $bg_images,
	);
						
$of_options[] = array( 
	"name"    => "Background Repeat",
	"desc"    => "",
	"id"      => $prefix . "pattern_repeat_page_header",
	"std"     => "repeat",
	"type"    => "select",
	"options" => array(
		'repeat'    => 'repeat', 
		'repeat-x'  => 'repeat-x', 
		'repeat-y'  => 'repeat-y', 
		'no-repeat' => 'no-repeat',
		),
	);  

// Breadcrumb
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Breadcrumb",
	"type" => "info",
	);
		
$of_options[] = array( 
	"name" => "",
	"desc" => __( "Check to enable breadcrumb.", "tcsn_theme" ),
	"id"   => $prefix . "show_breadcrumb",
	"std"  => 1,
	"type" => "checkbox",
	); 

/**
 * Footer Options
 *
 */		
$of_options[] = array( 
	"name" => "Footer",
	"type" => "heading",
	);

$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Number of Footer Columns",
	"type" => "info",
	);
	
$of_options[] = array(
	"name"    => "",
	"desc"    => __( "Select number of columns. <br> Then add widgets : Appearance > Widgets", "tcsn_theme" ),
	"id"      => $prefix . "columns_footer",
	"std"     => "4",
	"type"    => "images",
	"options" => array(
		"1" => get_template_directory_uri() . "/includes/img/col1.png",
		"2" => get_template_directory_uri() . "/includes/img/col2.png",
		"3" => get_template_directory_uri() . "/includes/img/col3.png",
		"4" => get_template_directory_uri() . "/includes/img/col4.png",
		),
	);

$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Footer Background",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" =>  "Background Color",
	"desc" => __( "Background color.", "tcsn_theme" ),
	"id"   => $prefix . "bg_color_footer",
	"std"  => "#efefef",
	"type" => "color",
	);

$of_options[] = array( 
	"name" => "Background Image",
	"desc" => __( "Check to enable background pattern.", "tcsn_theme" ),
	"id"   => $prefix . "show_pattern_footer",
	"std"  => 0,
	"type" => "checkbox",
	); 
	
$of_options[] = array( 	
	"name"    => "",
	"desc"    => __( "Select a background pattern. <br><br> To add more patterns, just upload patterns in : <br>  Theme folder > img > patterns", "tcsn_theme" ),
	"id"      => $prefix . "pattern_footer",
	"std" 	  => "",
	"type" 	  => "tiles",
	"options" => $bg_images,
	);

$of_options[] = array( 
	"name"    => "Background Repeat",
	"desc"    => "",
	"id"      => $prefix . "pattern_repeat_footer",
	"std" 	  => "repeat",
	"type"    => "select",
	"options" => array(
		'repeat'    => 'repeat', 
		'repeat-x'  => 'repeat-x', 
		'repeat-y'  => 'repeat-y', 
		'no-repeat' => 'no-repeat',
		),
	); 	

$of_options[] = array( 
	"name" => "Border Top Color",
	"desc" => __( "", "tcsn_theme" ),
	"id"   => $prefix . "border_top_footer",
	"std"  => "#e0e0e0",
	"type" => "color",
	);	

/**
 * Portfolio Options
 *
 */	
$of_options[] = array( 	
	"name" => "Portfolio",
	"type" => "heading",
);


// Portfolio grid page	
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Portfolio Grid Pages (2,3,4 columns)",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "Portfolio items per page",
	"desc" => __( "Specify the number of portfolio items to display per page.", "tcsn_theme" ),
	"id"   => $prefix . "portfolio_items_per_page",
	"std"  => "9",
	"type" => "text",
	);

$of_options[] = array( 
	"name" => "Sort Portfolio Items",
	"id"   => $prefix . "portfolio_sort",
	"type"    => "select",
	"options" => array(
		'date'  => 'By Date', 
		'rand' 	=> 'Random', 
		'title'	=> 'By Title', 
		),
	);

$of_options[] = array( 
	"name" => "Arrange Sorted Portfolio Items",
	"desc" => __( "For more flxible re-ordering, refer help document for recommended plugin", "tcsn_theme" ),
	"id"   => $prefix . "portfolio_arrange",
	"type"    => "select",
	"options" => array(
		'DESC'   => 'Descending', 
		'ASC'    => 'Ascending',
		),
	);
	
$of_options[] = array( 
	"name" => "Enable Portfolio Filter",
	"desc" => __( "Check to enable portfolio filter.", "tcsn_theme" ),
	"id"   => $prefix . "portfolio_filter",
	"std"  => 1,
	"type" => "checkbox",
); 
	
$of_options[] = array( 
	"name" => "Enable Heading to Portfolio Item",
	"desc" => __( "Check to enable heading of portfolio item.", "tcsn_theme" ),
	"id"   => $prefix . "portfolio_heading",
	"std"  => 1,
	"type" => "checkbox",
); 

$of_options[] = array( 
	"name" => "Enable Client Name",
	"desc" => __( "Check to enable client name below heading", "tcsn_theme" ),
	"id"   => $prefix . "portfolio_client",
	"std"  => 1,
	"type" => "checkbox",
); 

$of_options[] = array( 
	"name" => "Enable Excerpt to Portfolio Item",
	"desc" => __( "Check to enable excerpt of portfolio item.", "tcsn_theme" ),
	"id"   => $prefix . "portfolio_excerpt",
	"std"  => 1,
	"type" => "checkbox",
); 

$of_options[] = array( 
	"name" => "Select zoom or link on hover",
	"desc" => __( "", "tcsn_theme" ),
	"id"   => $prefix . "portfolio_hover",
	"type"    => "select",
	"options" => array(
		'zoom'    => 'Zoom',
		'link'   => 'Link', 
		),
	);

// Portfolio details page	
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Portfolio Details Pages",
	"type" => "info",
	);

$of_options[] = array( 
	"name" => "Enable predefined content",
	"desc" => __( "Check to enable predefined content on portfolio details page.", "tcsn_theme" ),
	"id"   => $prefix . "portfolio_predefined_content",
	"std"  => 1,
	"type" => "checkbox",
); 

// Portfolio archive page	
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Portfolio Archive Pages",
	"type" => "info",
	);


$of_options[] = array( 
	"name" => "Portfolio Archive page Title",
	"desc" => __( "", "tcsn_theme" ),
	"id"   => $prefix . "portfolio_archive_title",
	"std"  => "Our Portfolio",
	"type" => "text",
	); 

/**
 * Blog Options
 *
 */	
$of_options[] = array( 	
	"name" => "Blog",
	"type" => "heading",
);

$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Archives",
	"type" => "info",
	);

$of_options[] = array(
	"name"    => "Layout",
	"desc"    => __( "Select archives layout.", "tcsn_theme" ),
	"id"      => $prefix . "blog_layout",
	"std"     => "with-sidebar",
	"type"    => "select",
	"options" => array(
		'full-width'   => 'Full Width', 
		'with-sidebar' => 'With Sidebar',
		),
	);	

$of_options[] = array(
	"name"    => "Sidebar Position",
	"desc"    => __( "Select archives sidebar position.", "tcsn_theme" ),
	"id"      => $prefix . "blog_sidebar",
	"std"     => "sidebar-right",
	"type"    => "select",
	"options" => array(
		'sidebar-left'  => 'Sidebar Left', 
		'sidebar-right' => 'Sidebar Right',
		),
	);	

$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Single Post",
	"type" => "info",
	);

$of_options[] = array(
	"name"    => "Layout",
	"desc"    => __( "Select layout.", "tcsn_theme" ),
	"id"      => $prefix . "single_post_layout",
	"std"     => "with-sidebar",
	"type"    => "select",
	"options" => array(
		'single-full-width'   => 'Full Width', 
		'single-with-sidebar' => 'With Sidebar',
		),
	);	

$of_options[] = array(
	"name"    => "Sidebar Position",
	"desc"    => __( "Select sidebar position.", "tcsn_theme" ),
	"id"      => $prefix . "single_post_sidebar",
	"std"     => "sidebar-right",
	"type"    => "select",
	"options" => array(
		'single-sidebar-left'  => 'Sidebar Left', 
		'single-sidebar-right' => 'Sidebar Right',
		),
	);	

$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Others",
	"type" => "info",
	);

$of_options[] = array( 
	"name" => "Blog Title",
	"desc" => __( "Blog page title.", "tcsn_theme" ),
	"id"   => $prefix . "blog_title",
	"std"  => "Blogpost",
	"type" => "text",
	); 	

/**
 * Custom Post Types Options
 *
 */	
$of_options[] = array( 	
	"name" => "Team / Testimonial",
	"type" => "heading",
);

// Team details page	
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Team",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "Team Details Page Title",
	"desc" => __( "", "tcsn_theme" ),
	"id"   => $prefix . "team_title",
	"std"  => "Our Team",
	"type" => "text",
	); 

$of_options[] = array( 
	"name" => "Team Archive Page Title",
	"desc" => __( "", "tcsn_theme" ),
	"id"   => $prefix . "team_archive_title",
	"std"  => "Team Archive",
	"type" => "text",
	); 

// Testimonial details page	
$of_options[] = array( 
	"name" => "",
	"desc" => "",
	"id"   => "info_heading",
	"std"  => "Testimonial",
	"type" => "info",
	);
	
$of_options[] = array( 
	"name" => "Testimonial Details page Title",
	"desc" => __( "", "tcsn_theme" ),
	"id"   => $prefix . "testimonial_title",
	"std"  => "Reviews",
	"type" => "text",
	); 

$of_options[] = array( 
	"name" => "Testimonial Archive page Title",
	"desc" => __( "", "tcsn_theme" ),
	"id"   => $prefix . "testimonial_archive_title",
	"std"  => "Review Archive",
	"type" => "text",
	); 
			
/**
 * Custom CSS
 *
 */	 
$of_options[] = array( 
	"name" => "Custom CSS",
	"type" => "heading",
);
					
$of_options[] = array( 
	"name" => "Custom CSS",
	"desc" => __( "Paste your CSS Code. <br><br> Styles those are given through options panel (like fonts and backgrounds) will not get overridden from style.css of main theme or child theme. Those will need to be given here only.<br><br> Always keep backup, in case of accidental 'Reset'.", "tcsn_theme" ),
	"id"   => $prefix . "custom_css",
	"std"  => "",
	"type" => "textarea",
	); 				

/**
 * Backup Options
 *
 */	 							
$of_options[] = array( 	
	"name" => "Backup Options",
	"type" => "heading",
);
				
$of_options[] = array( 	
	"name" => "Backup and Restore Options",
	"desc" => __( "You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.", "tcsn_theme" ),
	"id"   => "of_backup",
	"std"  => "",
	"type" => "backup",
	);
				
$of_options[] = array( 	
	"name" => "Transfer Theme Options Data",
	"desc" => __( "You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click \"Import Options\".", "tcsn_theme" ),
	"id"   => "of_transfer",
	"std"  => "",
	"type" => "transfer",
	);
				
	}//End function: of_options()
}//End chack if function exists: of_options()
?>
