<?php
/*------------------------------------------------------------
 * Table of Contents
 *
 * 1.  Lists
 * 2.  Text color
 * 3.  Dropcap
 * 4.  Highlight
 * 5.  Spacer / Gap
 * 6.  Block quote
 * 7.  Tooltip
 * 8.  Table
 * 9.  Big Text
 * 10. Text Caps
 * 11. Button
 * 12. Icons
 * 13. Info Popover
 * 
 *------------------------------------------------------------*/

/*------------------------------------------------------------
 * Remove extra P tags
 *
 *------------------------------------------------------------*/
add_filter("the_content", "tcsn_bw_shortcode_format");
 
function tcsn_bw_shortcode_format($content) {
 
// array of custom shortcodes requiring the fix
$block = join("|",array( "list","list_item","list_checkmark","list_arrow","list_star","list_heart","list_circle","list_unstyled","list_inline","list_separator","list_pricing","text_color","dropcap","highlight","spacer","blockquote","tooltip","table","tbody","thead","tr","th","td", "text_big", "text_caps", "button", "icon", "popover") );
// opening tag
$rep = preg_replace("/(<p>)?\[($block)(\s[^\]]+)?\](<\/p>|<br \/>)?/","[$2$3]",$content);
// closing tag
$rep = preg_replace("/(<p>)?\[\/($block)](<\/p>|<br \/>)?/","[/$2]",$rep);
 
return $rep;
 
}

/*------------------------------------------------------------
 * add_shortcodes
 *
 * @since 1.0
 * 
 *------------------------------------------------------------*/
 function tcsn_bw_register_shortcodes() {
	add_shortcode( 'list', 'tcsn_bw_list_sc' );
	add_shortcode( 'list_item', 'tcsn_bw_list_item_sc' );
	add_shortcode( 'list_checkmark', 'tcsn_bw_checkmark_list_sc' );
	add_shortcode( 'list_arrow', 'tcsn_bw_arrow_list_sc' );
	add_shortcode( 'list_star', 'tcsn_bw_star_list_sc' );
	add_shortcode( 'list_heart', 'tcsn_bw_heart_list_sc' );
	add_shortcode( 'list_circle', 'tcsn_bw_circle_list_sc' );
	add_shortcode( 'list_unstyled', 'tcsn_bw_unstyled_list_sc' );
	add_shortcode( 'list_inline', 'tcsn_bw_inline_list_sc' );
	add_shortcode( 'list_separator', 'tcsn_bw_separator_list_sc' );
	add_shortcode( 'list_pricing', 'tcsn_bw_pricing_list_sc' );
	add_shortcode( 'text_color', 'tcsn_bw_text_color_sc' );
	add_shortcode( 'dropcap', 'tcsn_bw_dropcap_sc' );
	add_shortcode( 'highlight', 'tcsn_bw_text_highlight_sc' );
	add_shortcode( 'spacer', 'tcsn_bw_spacer_sc' );
	add_shortcode( 'blockquote', 'tcsn_bw_blockquote_sc' );
	add_shortcode( 'tooltip', 'tcsn_bw_tooltip_sc' );
	add_shortcode( 'table', 'tcsn_bw_table_sc' );
	add_shortcode( 'tbody', 'tcsn_bw_table_body_sc' );
	add_shortcode( 'thead', 'tcsn_bw_table_head_sc' );
	add_shortcode( 'tr', 'tcsn_bw_table_tr_sc' );
	add_shortcode( 'th', 'tcsn_bw_table_th_sc' );
	add_shortcode( 'td', 'tcsn_bw_table_td_sc' );
	add_shortcode( 'text_big', 'tcsn_bw_text_big_sc' );
	add_shortcode( 'text_caps', 'tcsn_bw_text_caps_sc' );
	add_shortcode( 'button', 'tcsn_bw_button_sc' );
	add_shortcode( 'icon', 'tcsn_bw_icon_sc' );
	add_shortcode( 'popover', 'tcsn_bw_popover_sc' );
}
add_action('init', 'tcsn_bw_register_shortcodes');

/*------------------------------------------------------------
 * 1. Lists
 *
 * @since 1.0
 *
 * Examples below:
 *
// [list][list_item]List item one[/list_item][list_item]List item two[/list_item][/list]
// [list_checkmark][list_item]List item one[/list_item][list_item]List item two[/list_item][/list_checkmark]
// [list_inline][list_item]List item one[/list_item][list_item]List item two[/list_item][/list_inline]
 *
 *------------------------------------------------------------*/
// ul
function tcsn_bw_list_sc( $atts, $content = null ) {
   return '<ul>' . do_shortcode( $content ) . '</ul>';
}
// li
function tcsn_bw_list_item_sc( $atts, $content = null ) {
   return '<li>' . do_shortcode( $content ) . '</li>';
}
// Checkmark list
function tcsn_bw_checkmark_list_sc( $atts, $content = null ) {
   return '<ul class="list-checkmark">' . do_shortcode( $content ) . '</ul>';
}
// Arrow list
function tcsn_bw_arrow_list_sc( $atts, $content = null ) {
   return '<ul class="list-arrow">' . do_shortcode( $content ) . '</ul>';
}
// Heart list
function tcsn_bw_heart_list_sc( $atts, $content = null ) {
   return '<ul class="list-heart">' . do_shortcode( $content ) . '</ul>';
}
// Star list
function tcsn_bw_star_list_sc( $atts, $content = null ) {
   return '<ul class="list-star">' . do_shortcode( $content ) . '</ul>';
}
// Circle list
function tcsn_bw_circle_list_sc( $atts, $content = null ) {
   return '<ul class="list-circle">' . do_shortcode( $content ) . '</ul>';
}
// Unstyled list
function tcsn_bw_unstyled_list_sc( $atts, $content = null ) {
   return '<ul class="list-unstyled">' . do_shortcode( $content ) . '</ul>';
}
// Inline list
function tcsn_bw_inline_list_sc( $atts, $content = null ) {
   return '<ul class="list-inline">' . do_shortcode( $content ) . '</ul>';
}
// Separator list
function tcsn_bw_separator_list_sc( $atts, $content = null ) {
   return '<ul class="list-separator">' . do_shortcode( $content ) . '</ul>';
}
// Inline list
function tcsn_bw_pricing_list_sc( $atts, $content = null ) {
   return '<ul class="list-pricing">' . do_shortcode( $content ) . '</ul>';
}

/*------------------------------------------------------------
 * 2. Text color
 *
 * @since 1.0
 *
 * Examples below: 
 *
// [text_color color="blue"]Content here[/text_color] 
 *
 *------------------------------------------------------------*/
function tcsn_bw_text_color_sc( $atts, $content = null ) {
	extract ( shortcode_atts( array(
		'color' => '', 
    ), $atts ) );
	
	if( $color != ''  ) {
		$return_color = ' style="color:' . $color . ';"';
	}
	else{
		$return_color = '';
	}

	return '<span class="color"' . $return_color . '>' . do_shortcode( $content ) . '</span>';
}

/*------------------------------------------------------------
 * 3. Dropcap
 *
 * @since 1.0
 *
 * Examples below: 
 *
// [dropcap size="" bg_color=""]T[/dropcap]
 *
 *------------------------------------------------------------*/
function tcsn_bw_dropcap_sc( $atts, $content = null ) {
	extract( shortcode_atts( array(
		'bg_color' => '', 
    ), $atts ) );
	
	if( $bg_color != ''  ) {
		$return_bg_color = ' style="background:' . $bg_color . ';"';
	}
	else{
		$return_bg_color = '';
	}

return '<span class="dropcap"' . $return_bg_color . '>' . do_shortcode( $content ) . '</span>';
}  
  
/*------------------------------------------------------------
 * 4. Highlight
 *
 * @since 1.0
 *
 * Examples below: 
 *
// [highlight bgcolor="green" color="#fff"]Content here[/highlight] 
 *
 *------------------------------------------------------------*/
function tcsn_bw_text_highlight_sc( $atts, $content = null ) {
	extract ( shortcode_atts( array(
		'bgcolor' => '', 
		'color'   => '', 
    ), $atts ) );
	
	if( $bgcolor != '' ||  $color != '' ) {

	$return_start = ' style="';
	$return_end = '"';

	if( $bgcolor != '' ) {
		$return_bgcolor = 'background-color:' . $bgcolor . ';';
	}
	else {
		$return_bgcolor = '';
	}
	if( $color != ''  ) {
		$return_color = 'color:' . $color . ';';
	}
	else{
		$return_color = '';
	}
}
else {
	$return_start = '';
	$return_end = '';
	$return_bgcolor = '';
	$return_color = '';
}
	return '<span class="highlight"' . $return_start . '' . $return_bgcolor . '' . $return_color . '' . $return_end . '>' . do_shortcode( $content ) . '</span>';
}

/*------------------------------------------------------------
 * 5. Vertical Spacer / Gap
 *
 * @since 1.0
 *
 * Example below: 
 * 
// [spacer height="in px"]
 * 
 *------------------------------------------------------------*/
function tcsn_bw_spacer_sc( $atts, $content ) {
	extract ( shortcode_atts( array(
		'height'       => '', // in px
    ), $atts ) );

	return '<div class="spacer" style="height: ' . $height . ';"></div>';
}

/*------------------------------------------------------------
 * 6. Blockquote
 *
 * @since 1.0
 *
 * Examples below:
 *
// [blockquote align=""]Content here[/blockquote]
// [blockquote align="pull-right"]Content here[/blockquote]
 *
 *------------------------------------------------------------*/
function tcsn_bw_blockquote_sc( $atts,  $content = null ) {
	extract( shortcode_atts( array(
    	'align' => '', 
    ), $atts ) );
	if( $align != ''  ) {
		$return_align = ' class="' . $align . '"';
	}
	else{
		$return_align = '';
	}
    return '<blockquote' . $return_align . '>' . do_shortcode( $content ) . '</blockquote>';
}

/*------------------------------------------------------------
 * 7. Tooltip
 *
 * @since 1.0
 *
// [tooltip url="" title="Content inside tooltip" placement="top"]Link text[/tooltip]
 *
 *------------------------------------------------------------*/
function tcsn_bw_tooltip_sc( $atts, $content = null ) {
	extract ( shortcode_atts( array(
		'url'       => '', 
		'title'     => '', 
        'placement' => 'top', // top, bottom, left, right
    ), $atts ) );
	
	if( $url != ''  ) {
		$return_url = 'href="' . $url . '" ';
	}
	else{
		$return_url = '';
	}
    return '<a ' . $return_url . 'title="' . $title . '" data-placement="' . $placement . '" data-toggle="tooltip" target="_blank">' . do_shortcode( $content ) . '</a>';
}

/*------------------------------------------------------------
 * 8. Table
 *
 * @since 1.0
 *
 * Example below:
 *
// [table strip="striped" border="bordered" compact="" hover="hover"]
//
// [thead]
// [tr]
// [th]Heading one[/th]
// [th]Heading two[/th]
// [/tr]
// [/thead]
//
// [tbody]
// [tr]
// [td]One[/td]
// [td]Two[/td]
// [/tr]
// [tr]
// [td]Three[/td]
// [td]Four[/td]
// [/tr]
// [/tbody]
//
// [/table]
 *
 *------------------------------------------------------------*/
function tcsn_bw_table_sc( $atts,  $content = null ) {
	extract( shortcode_atts( array(
    	'strip'   => '', // null, striped
        'border'  => '', // null, bordered
        'hover'   => '', // null, hover
        'compact' => '' // null, condensed
    ), $atts ) );
    if ( $strip ) {
        $strip = 'table-' . $strip;
    }
    if ( $border ) {
        $border = 'table-' . $border;
    }
    if ( $hover ) {
        $hover = 'table-' . $hover;
    }
    if ( $compact ) {
        $compact  = 'table-' . $compact;
    }
    return '<table class="table ' . $strip . ' ' . $border . ' ' . $hover . ' ' . $compact  . '">' . do_shortcode( $content ) . '</table>';
}

// Table Body
function tcsn_bw_table_body_sc( $atts,  $content = null ) {
	extract ( shortcode_atts( array(
		'class'       => '',  // null, success, error, warning, info
    ), $atts ) );
	if( $class != ''  ) {
		$return_url = ' class="' . $class . '"';
	}
	else{
		$return_url = '';
	}
    return '<tbody' . $return_url . '>' . do_shortcode( $content ) . '</tbody>';
}

// Table Head
function tcsn_bw_table_head_sc( $atts,  $content = null ) {
	extract ( shortcode_atts( array(
		'class'       => '', 
    ), $atts ) );
	if( $class != ''  ) {
		$return_url = ' class="' . $class . '"';
	}
	else{
		$return_url = '';
	}
    return '<thead' . $return_url . '>' . do_shortcode( $content ) . '</thead>';
}

// Table Row
function tcsn_bw_table_tr_sc( $atts,  $content = null ) {
	extract ( shortcode_atts( array(
		'class'       => '',  // null, success, error, warning, info
    ), $atts ) );
	if( $class != ''  ) {
		$return_url = ' class="' . $class . '"';
	}
	else{
		$return_url = '';
	}
    return '<tr' . $return_url . '>' . do_shortcode( $content ) . '</tr>';
}

// Table Head Cell
function tcsn_bw_table_th_sc( $atts,  $content = null ) {
	extract ( shortcode_atts( array(
		'class'       => '', 
    ), $atts ) );
	if( $class != ''  ) {
		$return_url = ' class="' . $class . '"';
	}
	else{
		$return_url = '';
	}
    return '<th' . $return_url . '>' . do_shortcode( $content ) . '</th>';
}

// Table Cell
function tcsn_bw_table_td_sc( $atts,  $content = null ) {
	extract ( shortcode_atts( array(
		'class'       => '', 
    ), $atts ) );
	if( $class != ''  ) {
		$return_url = ' class="' . $class . '"';
	}
	else{
		$return_url = '';
	}
    return '<td' . $return_url . '>' . do_shortcode( $content ) . '</td>';
}

/*------------------------------------------------------------
 * 9. Text big
 *
 * @since 1.0
 *
 * Example below:
 *
// [text_big font_size=""]Text here[/text_big]
 *
 *------------------------------------------------------------*/
function tcsn_bw_text_big_sc( $atts,  $content = null ) {
	extract( shortcode_atts( array(
    	'font_size' => '', 
    ), $atts ) );

    return '<span class="text-big" style="font-size: ' . $font_size . '" >' . do_shortcode( $content ) . '</span>';
}

/*------------------------------------------------------------
 * 10. Text Caps
 *
 * @since 1.0
 *
 * Example below:
 *
// [text_caps font_size="e.g. 11px / leave blank for theme default" color="e.g. #000 / leave blank for theme default"]Text here[/text_caps]
 *
 *------------------------------------------------------------*/
function tcsn_bw_text_caps_sc( $atts,  $content = null ) {
	extract( shortcode_atts( array(
    	'font_size' => '', 
		'color' => '', 
    ), $atts ) );

// Add styles for background, padding, margin
$add_style = array();

	if ( $font_size ) {
		$add_style[] = 'font-size: '. $font_size .';';
	} 
	
	if ( $color ) {
		$add_style[] = 'color: '. $color .';';
	} 

$add_style = implode('', $add_style);

if ( $add_style ) {
	$add_style = wp_kses( $add_style, array() );
	$add_style = ' style="' . esc_attr($add_style) . '"';
}

    return '<span class="text-caps" ' . $add_style . '>' . do_shortcode( $content ) . '</span>';
}

/*------------------------------------------------------------
 * 11. Button
 *
 * @since 1.0
 *
 * Example below:
 *
// [button class="" target="" url=""]Link text here[/button]
 * 
 *------------------------------------------------------------*/
function tcsn_bw_button_sc( $atts, $content=null ) {
	extract ( shortcode_atts( array(
		'class'  => '', 
		'color'  => '',
		'target' => '', 
		'url'    => '', 
		'icon'   => '', 
	), $atts ) );
	
	if( $icon != ''  ) {
		$return_icon = '<i class="icon-' . $icon . '"></i>';
  	} else {
		$return_icon = '';
	}
	if( $url != ''  ) {
		$return_url = ' href="' . $url . '"';
	} else {
		$return_url = '';
	}
	return '<a class="' . $class . $color . '" target="' . $target . '" ' . $return_url . '>' . do_shortcode( $content ) . $return_icon . '</a>';
}

/*------------------------------------------------------------
 * 12. Icons
 *
 * @since 1.0
 *
 * Examples below:
 *
// [icon type="star" color="" size=""]
// [icon type="star" color="red" size="3em"]
// [icon type="star" color="#69F" size="20px"]
 *
 *------------------------------------------------------------*/
function tcsn_bw_icon_sc( $atts, $content ) {
	extract( shortcode_atts( array(
		'type'  => '', 
        'color' => 'black', 
		'size' => '',
    ), $atts ) );
	
	if( $size != ''  ) {
		$return_size = 'font-size: ' . $size . ';';
	}
	else{
		$return_size = '';
	}
	
    return '<i class="icon-' . $type . '" style="color:' . $color . ';' . $return_size . '"></i>';
}

/*------------------------------------------------------------
 * 13. Popover
 *
 * @since 1.0
 *
 * Examples below:
 *
// [popover icon_color="" popover_placement="e.g. top, left, right, bottom" popover_content="Content Here"]
 *
 *------------------------------------------------------------*/
function tcsn_bw_popover_sc( $atts, $content ) {
	extract( shortcode_atts( array(
		'icon_color'	=> '',
		'popover_placement'	=> '', 
        'popover_content'   => '', 
    ), $atts ) );
	
	// Add styles for background, padding, margin
	$add_style = array();
		
		if ( $icon_color ) {
			$add_style[] = 'color: '. $icon_color .';';
		} 
	
	$add_style = implode('', $add_style);
	
	if ( $add_style ) {
		$add_style = wp_kses( $add_style, array() );
		$add_style = ' style="' . esc_attr($add_style) . '"';
	}
	
    return '<span class="popover-btn"><i class="icon-info-circled" ' . $add_style . ' data-toggle="popover" data-placement="' . $popover_placement . '" data-content="' . $popover_content . '"></i></span>';
}