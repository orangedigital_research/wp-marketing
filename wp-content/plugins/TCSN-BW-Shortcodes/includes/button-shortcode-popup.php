<?php
// look up for the path
require_once('tcsn_config.php');
// check for rights
if ( !is_user_logged_in() || !current_user_can('edit_posts') ) 
	wp_die(__( 'You are not allowed to be here', 'tcsn-shortcodes' ));
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Theme Styled Buttons</title>
<script type="text/javascript" src="<?php echo get_option( 'siteurl' ) ?>/wp-content/plugins/TCSN-BW-Shortcodes/js/jquery-1.9.1.min.js"></script>
<script language="javascript" type="text/javascript" src="<?php echo get_option( 'siteurl' ) ?>/wp-includes/js/tinymce/tiny_mce_popup.js"></script>
<link rel="stylesheet" href="<?php echo get_option( 'siteurl' ) ?>/wp-content/plugins/TCSN-BW-Shortcodes/css/tinymce.css" />
<script type="text/javascript">
var TCSNDialog = {
    local_ed: 'ed',
    init: function (ed, url) {
        TCSNDialog.local_ed = ed;
        tinyMCEPopup.resizeToInnerSize();
    },
    insert: function insertButton(ed) {

        // set up variables to contain input values
		var style  = jQuery('#dialogwindow select#field-style').val();
		var size   = jQuery('#dialogwindow select#field-size').val();
		var target = jQuery('#dialogwindow select#field-target').val();
        var url    = jQuery('#dialogwindow input#field-url').val();
        var text   = jQuery('#dialogwindow input#field-text').val();
		var icon   = jQuery('#dialogwindow select#field-icon').val();

        var output = '';

        // setup the output of shortcode
        output = ' [button ';

        output += 'class="' + size + ' ' + style + '" ';
		output += 'target="' + target + '" ';

        if (url) {
            output += 'url="' + url + '" ';
		}
		
	    if (icon) {
            output += 'icon="' + icon + '" ';
		}

        if (text) {
            output += ']' + text + '[/button]';
        }
		
        // if it is blank, use the selected text, if present
        else {
            output += ']' + TCSNDialog.local_ed.selection.getContent() + '[/button] ';
        }
        tinyMCEPopup.execCommand('mceReplaceContent', false, output);

        // Return
        tinyMCEPopup.close();
    }
};
tinyMCEPopup.onInit.add(TCSNDialog.init, TCSNDialog);
</script>
</head>
<body>
<div id="dialogwindow">
    <form action="/" method="get" accept-charset="utf-8">
        <p class="clearfix">
            <label for="field-text">Button Text</label>
            <input type="text" name="field-text" value="" id="field-text" />
        </p>
        <p class="clearfix">
            <label for="field-url">Button URL</label>
            <input type="text" name="field-url" value="" id="field-url" />
        </p>
        <p class="clearfix">
            <label for="field-target">Target</label>
            <select name="field-target" id="field-target" size="1">
                <option value="_blank" selected="selected">_blank</option>
                <option value="_self"=>_self</option>
            </select>
        </p>
        <p class="clearfix">
            <label for="field-style">Button Style</label>
            <select name="field-style" id="field-style" size="1">
                <option value="">Normal Button >>>>>>>>>>>>>>></option>
                <option value="">Dark</option>
                <option value="mybtn-green">Green</option>
                <option value="mybtn-red">Red</option>
                <option value="mybtn-blue">Blue</option>
                <option value="mybtn-turquoise" selected="selected">Turquoise </option>
                <option value="mybtn-grey">Grey</option>
                <option value="mybtn-golden">Dark goldenrod</option>
                <option value="mybtn-violet">Violet red</option>
                <option value="mybtn-royal">Royal Blue</option>
                <option value="">Transparent Button With Border >>>>>>>>>>>>>>></option>
                <option value="mybtn-flat">Dark With Border</option>
                <option value="mybtn-flat-green">Green With Border</option>
                <option value="mybtn-flat-red">Red With Border</option>
                <option value="mybtn-flat-blue">Blue With Border</option>
                <option value="mybtn-flat-turquoise">Turquoise With Border</option>
                <option value="mybtn-flat-grey">Grey With Border</option>
                <option value="mybtn-flat-golden">Dark goldenrod With Border</option>
                <option value="mybtn-flat-violet">Violet red With Border</option>
                <option value="mybtn-flat-royal">Royal Blue With Border</option>
            </select>
        </p>
        <p class="clearfix">
            <label for="field-size">Button Size</label>
            <select name="field-size" id="field-size" size="1">
                <option value="mybtn" selected="selected">Normal</option>
                <option value="mybtn mybtn-big ">Big</option>
            </select>
        </p>
        <p class="clearfix">
            <label for="field-icon">Icon</label>
            <select name="field-icon" id="field-icon" size="1">
                <option value="" selected="selected">None</option>
                <option value="accessibility">Accessibility</option>
                <option value="acrobat">Acrobat</option>
                <option value="acrobat">Address</option>
                <option value="address-book">Address-book</option>
                <option value="adjust">Adjust</option>
                <option value="adjust-1">Adjust-1</option>
                <option value="adult">Adult</option>
                <option value="aim">Aim</option>
                <option value="air">Air</option>
                <option value="airport">Airport</option>
                <option value="alert">Alert</option>
                <option value="align-center">Align-center</option>
                <option value="align-justify">Align-justify</option>
                <option value="align-left">Align-left</option>
                <option value="align-right">Align-right</option>
                <option value="Amazon">Amazon</option>
                <option value="android">Android</option>
                <option value="angellist">Angellist</option>
                <option value="appnet">Appnet</option>
                <option value="appstore">Appstore</option>
                <option value="archive">Archive</option>
                <option value="arrow-combo">Arrow-combo</option>
                <option value="arrows-ccw">Arrows-ccw</option>
                <option value="arrows-cw">Arrows-cw</option>
                <option value="art-gallery">Art-gallery</option>
                <option value="asterisk">Asterisk</option>
                <option value="attach">Attach</option>
                <option value="attach-1">Attach-1</option>
                <option value="attention">Attention</option>
                <option value="back">Back</option>
                <option value="back-in-time">Back-in-time</option>
                <option value="bag">Bag</option>
                <option value="barcode">Barcode</option>
                <option value="basket">Basket</option>
                <option value="basket-1">Basket-1</option>
                <option value="basket-2">Basket-2</option>
                <option value="basket-alt">Basket-alt</option>
                <option value="battery">Battery</option>
                <option value="behance">Behance</option>
                <option value="bell">Bell</option>
                <option value="bell-1">Bell-1</option>
                <option value="bicycle">Bicycle</option>
                <option value="bitbucket">Bitbucket</option>
                <option value="bitcoin">Bitcoin</option>
                <option value="block">Block</option>
                <option value="blogger">Blogger</option>
                <option value="bold">Bold</option>
                <option value="book">Book</option>
                <option value="book-1">Book-1</option>
                <option value="bookmark">Bookmark</option>
                <option value="bookmark-1">Bookmark-1</option>
                <option value="bookmarks">Bookmarks</option>
                <option value="book-open">Book-open</option>
                <option value="box">Box</option>
                <option value="braille">Braille</option>
                <option value="briefcase">Briefcase</option>
                <option value="briefcase-1">Briefcase-1</option>
                <option value="brush">Brush</option>
                <option value="bucket">Bucket</option>
                <option value="buffer">Buffer</option>
                <option value="calendar">Calendar</option>
                <option value="calendar-1">Calendar-1</option>
                <option value="calendar-2">Calendar-2</option>
                <option value="call">Call</option>
                <option value="camera">Camera</option>
                <option value="camera-1">Camera-1</option>
                <option value="camera-2">Camera-2</option>
                <option value="camera-3">Camera-3</option>
                <option value="cancel">Cancel</option>
                <option value="cancel-1">Cancel-1</option>
                <option value="cancel-2">Cancel-2</option>
                <option value="cancel-circled">Cancel-circled</option>
                <option value="cancel-squared">Cancel-squared</option>
                <option value="cart">Cart</option>
                <option value="cc">Cc</option>
                <option value="cc-1">cc-1</option>
                <option value="cc-by">Cc-by</option>
                <option value="cc-nc">Cc-nc</option>
                <option value="cc-nc-eu">Cc-nc-eu</option>
                <option value="cc-nc-jp">Cc-nc-jp</option>
                <option value="cc-nd">Cc-nd</option>
                <option value="cc-pd">Cc-pd</option>
                <option value="cc-remix">Cc-remix</option>
                <option value="cc-sa">Cc-sa</option>
                <option value="cc-share">Cc-share</option>
                <option value="ccw">Ccw</option>
                <option value="cc-zero">Cc-zero</option>
                <option value="cd">Cd</option>
                <option value="cd-1">Cd-1</option>
                <option value="chart">Chart</option>
                <option value="chart-1">Chart-1</option>
                <option value="chart-area">Chart-area</option>
                <option value="chart-bar">Chart-bar</option>
                <option value="chart-bar-1">Chart-bar-1</option>
                <option value="chart-line">Chart-line</option>
                <option value="chart-pie">Chart-pie</option>
                <option value="chart-pie-1">Chart-pie-1</option>
                <option value="chat">Chat</option>
                <option value="check">Check</option>
                <option value="check-1">Check-1</option>
                <option value="check-empty">Check-empty</option>
                <option value="chrome">Chrome</option>
                <option value="cinema">Cinema</option>
                <option value="clipboard">Clipboard</option>
                <option value="clipboard-1">Clipboard-1</option>
                <option value="clock">Clock</option>
                <option value="cloud">Cloud</option>
                <option value="cloud-1">Cloud-1</option>
                <option value="cloudapp">Cloudapp</option>
                <option value="cloud-thunder">Cloud-thunder</option>
                <option value="code">Code</option>
                <option value="cog">Cog</option>
                <option value="cog-1">Cog-1</option>
                <option value="cogs">Cogs</option>
                <option value="comment">Comment</option>
                <option value="comment-1">Comment-1</option>
                <option value="compass">Compass</option>
                <option value="credit-card">Credit-card</option>
                <option value="credit-card-1">Credit-card-1</option>
                <option value="credit-card-2">Credit-card-2</option>
                <option value="cup">Cup</option>
                <option value="cw">Cw</option>
                <option value="cw-1">Cw-1</option>
                <option value="database">Database</option>
                <option value="data-science-inv">Data-science-inv</option>
                <option value="db-shape">Db-shape</option>
                <option value="delicious">Delicious</option>
                <option value="desktop">Desktop</option>
                <option value="digg">Digg</option>
                <option value="direction">Direction</option>
                <option value="disqus">Disqus</option>
                <option value="doc">Doc</option>
                <option value="doc-1">Doc-1</option>
                <option value="doc-landscape">Doc-landscape</option>
                <option value="docs">Docs</option>
                <option value="doc-text">Doc-text</option>
                <option value="doc-text-inv">Doc-text-inv</option>
                <option value="dot">Dot</option>
                <option value="dot-2">Dot-2</option>
                <option value="dot-3">Dot-3</option>
                <option value="down">Down</option>
                <option value="down-1">Down-1</option>
                <option value="down-bold">Down-bold</option>
                <option value="down-circled">Down-circled</option>
                <option value="down-dir">Down-dir</option>
                <option value="download">Download</option>
                <option value="down-open">Down-open</option>
                <option value="down-open-1">Down-open-1</option>
                <option value="down-open-big">Down-open-big</option>
                <option value="down-open-mini">Down-open-mini</option>
                <option value="down-thin">Down-thin</option>
                <option value="dribbble">Dribbble</option>
                <option value="dribbble-1">Dribbble-1</option>
                <option value="dribbble-circled">Dribbble-circled</option>
                <option value="drive">Drive</option>
                <option value="dropbox">Dropbox</option>
                <option value="dropbox-1">Dropbox-1</option>
                <option value="droplet">Droplet</option>
                <option value="drupal">Drupal</option>
                <option value="duckduckgo">Duckduckgo</option>
                <option value="dwolla">Dwolla</option>
                <option value="easel">Easel</option>
                <option value="ebay">Ebay</option>
                <option value="email">Email</option>
                <option value="embassy">Embassy</option>
                <option value="erase">Erase</option>
                <option value="eventasaurus">Eventasaurus</option>
                <option value="eventbrite">Eventbrite</option>
                <option value="eventful">Eventful</option>
                <option value="evernote">Evernote</option>
                <option value="evernote-1">Evernote-1</option>
                <option value="exclamation">Exclamation</option>
                <option value="export">Export</option>
                <option value="export-1">Export-1</option>
                <option value="export-2">Export-2</option>
                <option value="eye">Eye</option>
                <option value="eye-1">Eye-1</option>
                <option value="eye-2">Eye-2</option>
                <option value="facebook">Facebook</option>
                <option value="facebook-1">Facebook-1</option>
                <option value="facebook-circled">Facebook-circled</option>
                <option value="facebook-squared">Facebook-squared</option>
                <option value="fast-backward">Fast-backward</option>
                <option value="fast-forward">Fast-forward</option>
                <option value="feather">Feather</option>
                <option value="filter">Filter</option>
                <option value="fire">Fire</option>
                <option value="fire-1">Fire-1</option>
                <option value="fire-station">Fire-station</option>
                <option value="fivehundredpx">Fivehundredpx</option>
                <option value="flag">Flag</option>
                <option value="flag-1">Flag-1</option>
                <option value="flash">Flash</option>
                <option value="flashlight">Flashlight</option>
                <option value="flattr">Flattr</option>
                <option value="flattr-1">Flattr-1</option>
                <option value="flickr">Flickr</option>
                <option value="flickr-1">Flickr-1</option>
                <option value="flickr-circled">Flickr-circled</option>
                <option value="flight">Flight</option>
                <option value="flight-1">Flight-1</option>
                <option value="floppy">Floppy</option>
                <option value="flow-branch">Flow-branch</option>
                <option value="flow-cascade">Flow-cascade</option>
                <option value="flow-line">Flow-line</option>
                <option value="flow-parallel">Flow-parallel</option>
                <option value="flow-tree">Flow-tree</option>
                <option value="folder">Folder</option>
                <option value="folder-1">Folder-1</option>
                <option value="folder-close">Folder-close</option>
                <option value="folder-open">Folder-open</option>
                <option value="font">Font</option>
                <option value=" fontsize"> Fontsize</option>
                <option value="forrst">Forrst</option>
                <option value="forward">Forward</option>
                <option value="foursquare">Foursquare</option>
                <option value="gauge">Gauge</option>
                <option value="gauge-1">Gauge-1</option>
                <option value="gift">Gift</option>
                <option value="github">Github</option>
                <option value="github-1">Github-1</option>
                <option value="github-circled">Github-circled</option>
                <option value="github-circled-1">Github-circled-1</option>
                <option value="glasses">Glasses</option>
                <option value="globe">Globe</option>
                <option value="gmail">gmail</option>
                <option value="google">google</option>
                <option value="google-circles">google-circles</option>
                <option value="googleplay">Googleplay</option>
                <option value="gowalla">Gowalla</option>
                <option value="gplus">Gplus</option>
                <option value="gplus-1">Gplus-1</option>
                <option value="gplus-circled">Gplus-circled</option>
                <option value="graduation-cap">Graduation-cap</option>
                <option value="grooveshark">Grooveshark</option>
                <option value="group">Group</option>
                <option value="guest">Guest</option>
                <option value="guidedog">Guidedog</option>
                <option value="hackernews">Hackernews</option>
                <option value="harbor">Harbor</option>
                <option value="hdd">Hdd</option>
                <option value="heart">Heart</option>
                <option value="heart-1">Heart-1</option>
                <option value="heart-2">Heart-2</option>
                <option value="heart-empty">Heart-empty</option>
                <option value="help">Help</option>
                <option value="help-1">Help-1</option>
                <option value="help-circled">Help-circled</option>
                <option value="home">Home</option>
                <option value="home-1">Home-1</option>
                <option value="hourglass">Hourglass</option>
                <option value="html5">Html5</option>
                <option value="ie">Ie</option>
                <option value="inbox">Inbox</option>
                <option value="inbox-1">Inbox-1</option>
                <option value="inbox-alt">Inbox-alt</option>
                <option value="indent-left">Indent-left</option>
                <option value="indent-right">Indent-right</option>
                <option value="industrial-building">Industrial-building</option>
                <option value="infinity">Infinity</option>
                <option value="info">Info</option>
                <option value="info-circled">Info-circled</option>
                <option value="instagram">Instagram</option>
                <option value="instagram-1">Instagram-1</option>
                <option value="install">Install</option>
                <option value="instapaper">Instapaper</option>
                <option value="intensedebate">Intensedebate</option>
                <option value="ipod">Ipod</option>
                <option value="italic">Italic</option>
                <option value="itunes">Itunes</option>
                <option value="key">Key</option>
                <option value="key-1">Key-1</option>
                <option value="keyboard">Keyboard</option>
                <option value="klout">Klout</option>
                <option value="lamp">Lamp</option>
                <option value="language">Language</option>
                <option value="lanyrd">Lanyrd</option>
                <option value="laptop">Laptop</option>
                <option value="lastfm">Lastfm</option>
                <option value="lastfm-1">Lastfm-1</option>
                <option value="lastfm-circled">Lastfm-circled</option>
                <option value="layout">Layout</option>
                <option value="leaf">Leaf</option>
                <option value="leaf-1">Leaf-1</option>
                <option value="left">Left</option>
                <option value="left-1">Left-1</option>
                <option value="left-2">Left-2</option>
                <option value="left-bold">Left-bold</option>
                <option value="left-circled">Left-circled</option>
                <option value="left-dir">Left-dir</option>
                <option value="left-open">Left-open</option>
                <option value="left-open-1">Left-open-1</option>
                <option value="left-open-big">Left-open-big</option>
                <option value="left-open-mini">Left-open-mini</option>
                <option value="left-thin">Left-thin</option>
                <option value="lego">Lego</option>
                <option value="level-down">Level-down</option>
                <option value="level-up">Level-up</option>
                <option value="library">Library</option>
                <option value="lifebuoy">Lifebuoy</option>
                <option value="lightbulb">Lightbulb</option>
                <option value="light-down">Light-down</option>
                <option value="light-up">Light-up</option>
                <option value="link">Link</option>
                <option value="linkedin">Linkedin</option>
                <option value="linkedin-1">Linkedin-1</option>
                <option value="linkedin-circled">Linkedin-circled</option>
                <option value="list">List</option>
                <option value="list-1">List-1</option>
                <option value="list-add">List-add</option>
                <option value="lkdto">Lkdto</option>
                <option value="location">Location</option>
                <option value="location-1">Location-1</option>
                <option value="lock">Lock</option>
                <option value="lock-1">Lock-1</option>
                <option value="lock-2">Lock-2</option>
                <option value="lock-open">Lock-open</option>
                <option value="lock-open-1">Lock-open-1</option>
                <option value="lock-open-2">Lock-open-2</option>
                <option value="login">Login</option>
                <option value="login-1">Login-1</option>
                <option value="logo-db">Logo-db</option>
                <option value="logout">Logout</option>
                <option value="loop">Loop</option>
                <option value="macstore">Macstore</option>
                <option value="magnet">Magnet</option>
                <option value="mail">Mail</option>
                <option value="mail-1">Mail-1</option>
                <option value="mail-2">Mail-2</option>
                <option value="mail-3">Mail-3</option>
                <option value="map">Map</option>
                <option value="meetup">Meetup</option>
                <option value="megaphone">Megaphone</option>
                <option value="megaphone-1">Megaphone-1</option>
                <option value="menu">Menu</option>
                <option value="mic">Mic</option>
                <option value="mic-1">Mic-1</option>
                <option value="minefield">Minefield</option>
                <option value="minus">Minus</option>
                <option value="minus-1">Minus-1</option>
                <option value="minus-circled">Minus-circled</option>
                <option value="minus-squared">Minus-squared</option>
                <option value="mixi">Mixi</option>
                <option value="mobile">Mobile</option>
                <option value="mobile-1">Mobile-1</option>
                <option value="mobile-2">Mobile-2</option>
                <option value="mobile-alt">Mobile-alt</option>
                <option value="monitor">Monitor</option>
                <option value="monitor-1">Monitor-1</option>
                <option value="moon">Moon</option>
                <option value="mouse">Mouse</option>
                <option value="move">Move</option>
                <option value="music">Music</option>
                <option value="mute">Mute</option>
                <option value="myspace">Myspace</option>
                <option value="network">Network</option>
                <option value="network-1">Network-1</option>
                <option value="newspaper">Newspaper</option>
                <option value="ninetyninedesigns">Ninetyninedesigns</option>
                <option value="note">Note</option>
                <option value="note-beamed">Note-beamed</option>
                <option value="off">Off</option>
                <option value="ok">Ok</option>
                <option value="ok-1">Ok-1</option>
                <option value="ok-2">Ok-2</option>
                <option value="ok-circled">Ok-circled</option>
                <option value="openid">Openid</option>
                <option value="opentable">Opentable</option>
                <option value="palette">Palette</option>
                <option value="paper-plane">Paper-plane</option>
                <option value="pause">Pause</option>
                <option value="paypal">Paypal</option>
                <option value="paypal-1">Paypal-1</option>
                <option value="pencil">Pencil</option>
                <option value="pencil-1">Pencil-1</option>
                <option value="person">Person</option>
                <option value="phone">Phone</option>
                <option value="phone-1">Phone-1</option>
                <option value="picasa">Picasa</option>
                <option value="picture">Picture</option>
                <option value="picture-1">Picture-1</option>
                <option value="pinboard">Pinboard</option>
                <option value="pinterest">Pinterest</option>
                <option value="pinterest-1">Pinterest-1</option>
                <option value="pinterest-circled">Pinterest-circled</option>
                <option value="plancast">Plancast</option>
                <option value="play">Play</option>
                <option value="play-1">Play-1</option>
                <option value="plurk">Plurk</option>
                <option value="plus">Plus</option>
                <option value="plus-1">Plus-1</option>
                <option value="plus-2">Plus-2</option>
                <option value="plus-circled">Plus-circled</option>
                <option value="plus-squared">Plus-squared</option>
                <option value="pocket">Pocket</option>
                <option value="podcast">Podcast</option>
                <option value="popup">Popup</option>
                <option value="popup-1">Popup-1</option>
                <option value="post">Post</option>
                <option value="posterous">Posterous</option>
                <option value="print">Print</option>
                <option value="print-1">Print-1</option>
                <option value="progress-0">Progress-0</option>
                <option value="progress-1">Progress-1</option>
                <option value="progress-2">Progress-2</option>
                <option value="progress-3">Progress-3</option>
                <option value="publish">Publish</option>
                <option value="qq">Qq</option>
                <option value="qrcode">Qrcode</option>
                <option value="quora">Quora</option>
                <option value="quote">Quote</option>
                <option value="rdio">Rdio</option>
                <option value="rdio-circled">Rdio-circled</option>
                <option value="record">Record</option>
                <option value="reddit">Reddit</option>
                <option value="renren">Renren</option>
                <option value="reply">Reply</option>
                <option value="reply-all">Reply-all</option>
                <option value="resize-full">Resize-full</option>
                <option value="resize-full-1">Resize-full-1</option>
                <option value="resize-full-alt">Resize-full-alt</option>
                <option value="resize-horizontal">Resize-horizontal</option>
                <option value="resize-small">Resize-small</option>
                <option value="resize-small-1">Resize-small-1</option>
                <option value="resize-vertical">Resize-vertical</option>
                <option value="retweet">Retweet</option>
                <option value="right">Right</option>
                <option value="right-1">Right-1</option>
                <option value="right-2">Right-2</option>
                <option value="right-bold">Right-bold</option>
                <option value="right-circled">Right-circled</option>
                <option value="right-dir">Right-dir</option>
                <option value="right-open">Right-open</option>
                <option value="right-open-1">Right-open-1</option>
                <option value="right-open-big">Right-open-big</option>
                <option value="right-open-mini">Right-open-mini</option>
                <option value="right-thin">Right-thin</option>
                <option value="road">road</option>
                <option value="rocket">Rocket</option>
                <option value="rss">Rss</option>
                <option value="rss-1">Rss-1</option>
                <option value="scribd">Scribd</option>
                <option value="search">Search</option>
                <option value="search-1">Search-1</option>
                <option value="share">Share</option>
                <option value="share-1">Share-1</option>
                <option value="shareable">Shareable</option>
                <option value="shuffle">Shuffle</option>
                <option value="shuffle-1">Shuffle-1</option>
                <option value="signal">Signal</option>
                <option value="signal-1">Signal-1</option>
                <option value="sina-weibo">Sina-weibo</option>
                <option value="skype">Skype</option>
                <option value="skype-1">Skype-1</option>
                <option value="skype-circled">Skype-circled</option>
                <option value="smashing">Smashing</option>
                <option value="smashmag">Smashmag</option>
                <option value="songkick">Songkick</option>
                <option value="sound">Sound</option>
                <option value="soundcloud">Soundcloud</option>
                <option value="soundcloud-1">Soundcloud-1</option>
                <option value="spotify">Spotify</option>
                <option value="spotify-1">Spotify-1</option>
                <option value="spotify-circled">Spotify-circled</option>
                <option value="squares">Squares</option>
                <option value="stackoverflow">Stackoverflow</option>
                <option value="star">Star</option>
                <option value="star-1">Star-1</option>
                <option value="star-2">Star-2</option>
                <option value="star-empty">Star-empty</option>
                <option value="star-empty-1">Star-empty-1</option>
                <option value="statusnet">Statusnet</option>
                <option value="steam">Steam</option>
                <option value="stop">Stop</option>
                <option value="stripe">Stripe</option>
                <option value="stumbleupon">Stumbleupon</option>
                <option value="stumbleupon-1">Stumbleupon-1</option>
                <option value="stumbleupon-circled">Stumbleupon-circled</option>
                <option value="suitcase">Suitcase</option>
                <option value="sweden">Sweden</option>
                <option value="switch">Switch</option>
                <option value="tablet">Tablet</option>
                <option value="tablet-1">Tablet-1</option>
                <option value="tag">Tag</option>
                <option value="tag-1">Tag-1</option>
                <option value="tag-2">Tag-2</option>
                <option value="tags">Tags</option>
                <option value="tape">Tape</option>
                <option value="target">Target</option>
                <option value="tasks">Tasks</option>
                <option value="text-height">Text-height</option>
                <option value="text-width">Text-width</option>
                <option value="th">Th</option>
                <option value="thermometer">Thermometer</option>
                <option value="th-large">Th-large</option>
                <option value="th-list">Th-list</option>
                <option value="thumbs-down">Thumbs-down</option>
                <option value="thumbs-down-1">Thumbs-down-1</option>
                <option value="thumbs-up">Thumbs-up</option>
                <option value="thumbs-up-1">Thumbs-up-1</option>
                <option value="ticket">Ticket</option>
                <option value="tint">Tint</option>
                <option value="to-end">To-end</option>
                <option value="tools">Tools</option>
                <option value="to-start">To-start</option>
                <option value="traffic-cone">Traffic-cone</option>
                <option value="trash">Trash</option>
                <option value="trash-1">Trash-1</option>
                <option value="tree-1">Tree-1</option>
                <option value="tree-2">Tree-2</option>
                <option value="trophy">Trophy</option>
                <option value="tumblr">Tumblr</option>
                <option value="tumblr-1">Tumblr-1</option>
                <option value="tumblr-circled">Tumblr-circled</option>
                <option value="twitter">Twitter</option>
                <option value="twitter-1">Twitter-1</option>
                <option value="twitter-circled">Twitter-circled</option>
                <option value="up">Up</option>
                <option value="up-1">Up-1</option>
                <option value="up-bold">Up-bold</option>
                <option value="up-circled">Up-circled</option>
                <option value="up-dir">Up-dir</option>
                <option value="upload">Upload</option>
                <option value="upload-cloud">Upload-cloud</option>
                <option value="up-open">Up-open</option>
                <option value="up-open-1">Up-open-1</option>
                <option value="up-open-big">Up-open-big</option>
                <option value="up-open-mini">Up-open-mini</option>
                <option value="up-thin">Up-thin</option>
                <option value="user">User</option>
                <option value="user-1">User-1</option>
                <option value="user-2">User-2</option>
                <option value="user-add">User-add</option>
                <option value="users">Users</option>
                <option value="users-1">Users-1</option>
                <option value="vcard">Vcard</option>
                <option value="viadeo">Viadeo</option>
                <option value="video">Video</option>
                <option value="video-1">Video-1</option>
                <option value="video-alt">Video-alt</option>
                <option value="videocam">Videocam</option>
                <option value="videocam-1">Videocam-1</option>
                <option value="vimeo">Vimeo</option>
                <option value="vimeo-1">Vimeo-1</option>
                <option value="vimeo-circled">Vimeo-circled</option>
                <option value="vk">Vk</option>
                <option value="vkontakte">Vkontakte</option>
                <option value="volume">Volume</option>
                <option value="volume-1">Volume-1</option>
                <option value="volume-down">Volume-down</option>
                <option value="volume-up">Volume-up</option>
                <option value="volume-up-1">Volume-up-1</option>
                <option value="w3c">W3c</option>
                <option value="warning">Warning</option>
                <option value="water">Water</option>
                <option value="weibo">Weibo</option>
                <option value="wikipedia">Wikipedia</option>
                <option value="window">Window</option>
                <option value="windows">Windows</option>
                <option value="wordpress">Wordpress</option>
                <option value="wrench">Wrench</option>
                <option value="wrench-1">Wrench-1</option>
                <option value="xing">Xing</option>
                <option value="yahoo">Yahoo</option>
                <option value="yelp">Yelp</option>
                <option value="youtube">Youtube</option>
                <option value="zoom-in">Zoom-in</option>
                <option value="zoom-in-1">Zoom-in-1</option>
                <option value="zoom-out">Zoom-out</option>
                <option value="zoom-out-1">Zoom-out-1</option>
            </select>
        </p>
    </form>
    <div class="clearfix"></div>
    <a class="mybtn" href="javascript:TCSNDialog.insert(TCSNDialog.local_ed)">Insert Button</a>
    <div class="clearfix"></div>
  
</div>
</body>
</html>